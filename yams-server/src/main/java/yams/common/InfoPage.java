/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */

package yams.common;

import java.io.Serializable;
import java.util.List;

/**
 * 分页对象
 * Created by Silence on 2016/4/15.
 */
public class InfoPage<T> implements Serializable {

    protected List<T> content;

    protected Long total;

    public InfoPage() {
    }

    public InfoPage(List<T> content, Long total) {
        this.content = content;
        this.total = total;
    }

    public List<T> getContent() {
        return content;
    }

    public void setContent(List<T> content) {
        this.content = content;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }
}
