package yams.common.constants;

/**
 * 账单支付状态
 * Created by Luffy on 2017/8/6.
 */
public enum BillStatusEnum implements ConstantEnum {

    UNPAID("待支付",0),
    PAID("已支付", 1);

    private final String text;

    private final Integer value;

    BillStatusEnum(String text, Integer value) {
        this.text = text;
        this.value = value;
    }

    public Integer getValue() {
        return this.value;
    }

    public String getText() {
        return this.text;
    }

    public String toString() {
        return this.getText();
    }
}
