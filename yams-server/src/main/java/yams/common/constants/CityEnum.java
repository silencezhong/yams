package yams.common.constants;

import java.io.Serializable;

/**
 * 城市枚举
 * Created by Luffy on 2018/05/15.
 */
public enum CityEnum implements Serializable {

    HANGZHOU("杭州", "001"),
    SHAOXING("绍兴", "002"),
    WUXI("无锡", "003");


    private final String text;

    private final String value;

    CityEnum(String text, String value) {
        this.text = text;
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public String getText() {
        return this.text;
    }

    public String toString() {
        return this.getText();
    }

    public static String getTextByVal(String value) {
        for (CityEnum e : CityEnum.values()) {
            if (e.getValue().equals(value)) {
                return e.getText();
            }
        }
        return "";
    }
}
