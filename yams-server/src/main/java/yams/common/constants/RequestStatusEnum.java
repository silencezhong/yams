/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */

package yams.common.constants;

/**
 * 实体状态的枚举值设定
 * 在做Jackson数据绑定的时候，请记住直接传递：DISABLE,ENABLE即可
 * Created by Silence on 2016/4/28.
 */
public enum RequestStatusEnum implements ConstantEnum {

    IGNORE("已忽略", 0),
    TODO("待处理", 1),
    ONGOING("处理中", 3),
    PROCESSED("已处理", 5),
    DONE("已评价", 9);

    private final String text;

    private final Integer value;

    RequestStatusEnum(String text, Integer value) {
        this.text = text;
        this.value = value;
    }

    public Integer getValue() {
        return this.value;
    }

    public String getText() {
        return this.text;
    }

    public String toString() {
        return this.getText();
    }

}
