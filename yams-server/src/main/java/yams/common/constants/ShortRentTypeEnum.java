/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */

package yams.common.constants;

import java.io.Serializable;

/**
 *  短租类型
 */
public enum ShortRentTypeEnum implements Serializable {

    SRT03("3小时", "SRT03"),
    SRT05("5小时", "SRT05"),
    SRT08("8小时", "SRT08"),
    SRT24("一天", "SRT00");
    private final String text;

    private final String value;

    ShortRentTypeEnum(String text, String value) {
        this.text = text;
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public String getText() {
        return this.text;
    }

    public String toString() {
        return this.getText();
    }

}
