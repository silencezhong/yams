package yams.common.constants;

import java.io.Serializable;

/**
 * 房型
 * Created by Luffy on 2017/8/9.
 */
public enum HouseGroupEnum implements Serializable {

    ROOM05("奢华一房", "ROOM05"),
    ROOM03("转角敞亮两房", "ROOM03"),
    ROOM01("豪华浴缸舒适房", "ROOM01"),
    ROOM07("舒适一室一厅", "ROOM07"),
    ROOM04("明亮卫浴房", "ROOM04"),
    ROOM06("舒适小套房", "ROOM06"),
    ROOM02("豪华浴缸舒适房双床房", "ROOM02");

    private final String text;

    private final String value;

    HouseGroupEnum(String text, String value) {
        this.text = text;
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public String getText() {
        return this.text;
    }

    public String toString() {
        return this.getText();
    }
}
