package yams.common.constants;

/**
 * 账单类型
 * Created by Luffy on 2017/8/18.
 */
public enum BillTypeEnum implements ConstantEnum {

    CONTRACT("合同",1),
    WATER_ELEC_GAS("水电气",2),
    REPAIR("报修",3),
    CLEAN("保洁",4),
    OTHER("其他",5);

    private final String text;

    private final Integer value;

    BillTypeEnum(String text, Integer value) {
        this.text = text;
        this.value = value;
    }

    public Integer getValue() {
        return this.value;
    }

    public String getText() {
        return this.text;
    }

    public String toString() {
        return this.getText();
    }

    public static String getTextByVal(Integer value) {
        for (BillTypeEnum e : BillTypeEnum.values()) {
            if (value.equals(e.getValue())) {
                return e.getText();
            }
        }
        return "";
    }
}
