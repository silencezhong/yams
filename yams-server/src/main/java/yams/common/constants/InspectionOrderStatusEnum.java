/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */

package yams.common.constants;

import java.io.Serializable;

/**
 * 预约看房订单状态
 * Created by Luffy on 2017/07/20.
 */
public enum InspectionOrderStatusEnum implements Serializable {

    TODO("预约中", "IOS00"),
    CONFIRM("已确认", "IOS01"),
    LOOKED("已看房", "IOS02"),
    C_CANCEL("客户失约", "IOS03"),
    W_CANCEL("后台取消", "IOS04");


    private final String text;
    private final String value;

    InspectionOrderStatusEnum(String text, String value) {
        this.text = text;
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public String getText() {
        return this.text;
    }

    public String toString() {
        return this.getText();
    }

}
