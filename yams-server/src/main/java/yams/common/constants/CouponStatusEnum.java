package yams.common.constants;

/**
 * 券状态
 * Created by Luffy on 2017/07/18.
 */
public enum CouponStatusEnum implements ConstantEnum {

    EXPIRE("已过期", 0),
    ENABLE("未领用", 1),
    OBTAIN("已领用", 2),
    USED("已使用", 3);

    private final String text;

    private final Integer value;

    CouponStatusEnum(String text, Integer value) {
        this.text = text;
        this.value = value;
    }

    public Integer getValue() {
        return this.value;
    }

    public String getText() {
        return this.text;
    }

    public String toString() {
        return this.getText();
    }
}
