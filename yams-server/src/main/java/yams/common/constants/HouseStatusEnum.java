package yams.common.constants;

/**
 * 房屋状态
 * Created by Silence on 2017/2/10.
 */
public enum HouseStatusEnum implements ConstantEnum {

    EMPTY("空房", 1),
    CHECKED("已租赁", 2),
    REPAIRING("维修中", 3),
    EXCEPTION("异常", 4),
    BOOKED("已预订",5),
    CLEAN("清扫中",6);

    private final String text;

    private final Integer value;

    HouseStatusEnum(String text, Integer value) {
        this.text = text;
        this.value = value;
    }

    public Integer getValue() {
        return this.value;
    }

    public String getText() {
        return this.text;
    }

    public String toString() {
        return this.getText();
    }
}
