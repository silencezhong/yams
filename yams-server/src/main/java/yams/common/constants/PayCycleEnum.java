package yams.common.constants;

/**
 * 合同付款周期
 * Created by Luffy on 2017/07/26.
 */
public enum PayCycleEnum implements ConstantEnum {

    ONE_MONTH("一月一付", 1),
    TWO_MONTH("两个月一付",2),
    THREE_MONTH("一季度一付", 3),
    HALF_YEAR("半年一付", 6),
    ONE_YEAR("一年一付", 12);

    private final String text;

    private final Integer value;

    PayCycleEnum(String text, Integer value) {
        this.text = text;
        this.value = value;
    }

    public Integer getValue() {
        return this.value;
    }

    public String getText() {
        return this.text;
    }

    public String toString() {
        return this.getText();
    }
    public static String getTextByVal(int value) {
        for (PayCycleEnum e : PayCycleEnum.values()) {
            if (e.getValue().equals(value)) {
                return e.getText();
            }
        }
        return "";
    }
}
