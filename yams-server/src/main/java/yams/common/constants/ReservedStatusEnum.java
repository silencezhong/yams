/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */

package yams.common.constants;

/**
 * 实体状态的枚举值设定
 * 在做Jackson数据绑定的时候，请记住直接传递：DISABLE,ENABLE即可
 * Created by Silence on 2016/4/28.
 */
public enum ReservedStatusEnum implements ConstantEnum {

    TODO("待确认", 1),
    CONFIRM("已确认", 2),
    C_CANCEL("自助取消", 0),
    W_CANCEL("后台取消", -1);
    private final String text;

    private final Integer value;

    ReservedStatusEnum(String text, Integer value) {
        this.text = text;
        this.value = value;
    }

    public Integer getValue() {
        return this.value;
    }

    public String getText() {
        return this.text;
    }

    public String toString() {
        return this.getText();
    }

}
