package yams.common.constants;

/**
 * 合同状态
 * Created by Silence on 2017/2/10.
 */
public enum ContractStatusEnum implements ConstantEnum {

    APPLICATION("未审核",0),
    EXECUTING("执行中", 1),
    FINISHED("已结束",2),
    STOP("已终止", 3),
    ARCHIVED("已归档", 4);

    private final String text;

    private final Integer value;

    ContractStatusEnum(String text, Integer value) {
        this.text = text;
        this.value = value;
    }

    public Integer getValue() {
        return this.value;
    }

    public String getText() {
        return this.text;
    }

    public String toString() {
        return this.getText();
    }
}
