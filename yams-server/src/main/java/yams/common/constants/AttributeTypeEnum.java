package yams.common.constants;

/**
 * 字段类型枚举
 * Created by Silence on 2017/2/10.
 */
public enum AttributeTypeEnum implements ConstantEnum {

    NUMBER("数值型", 0),
    ENUM("枚举型", 1),
    MEDIA("多媒体", 2),
    STRING("字符型", 3),
    INTEGER("整数型", 4),
    RICHTEXT("富文本", 5);

    private final String text;

    private final Integer value;

    AttributeTypeEnum(String text, Integer value) {
        this.text = text;
        this.value = value;
    }

    @Override
    public Integer getValue() {
        return this.value;
    }

    @Override
    public String getText() {
        return this.text;
    }
}
