/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */

package yams.common.constants;

import java.io.Serializable;

/**
 * 实体状态的枚举值设定
 * 在做Jackson数据绑定的时候，请记住直接传递：DISABLE,ENABLE即可
 * Created by Silence on 2016/4/28.
 */
public enum OrderStatusEnum implements Serializable {

    TODO("待确认待付款", "OS00"),
    CONFIRM("已确认待付款", "OS01"),
    PAID("已确认已付款", "OS02"),
    C_CANCEL("自助取消", "OS04"),
    W_CANCEL("后台取消", "OS03"),
    H_EXIT("已退房", "OS05");
    private final String text;

    private final String value;

    OrderStatusEnum(String text, String value) {
        this.text = text;
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public String getText() {
        return this.text;
    }

    public String toString() {
        return this.getText();
    }

}
