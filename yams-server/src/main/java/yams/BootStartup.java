package yams;

/**
 * 服务启动
 * Created by Silence on 2016/10/11.
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import yams.resource.IResourceLayer;
import yams.utils.ContextUtil;

@Configuration
@EnableAutoConfiguration
@ComponentScan("yams")
@ImportResource(value = {"classpath:/server-provider.xml"})
public class BootStartup {

    public static void main(String[] args) {
        ContextUtil.current = SpringApplication.run(BootStartup.class, args);

        ContextUtil.current.getBeansOfType(IResourceLayer.class).forEach((a, b) -> {
            b.initService();
        });
    }
}
