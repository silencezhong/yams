package yams.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import yams.service.*;
import javax.annotation.Resource;
import java.time.LocalDateTime;

/**
 * 通知管家和客户的定时任务
 * Created by Luffy on 2017/7/28.
 */
@Component
public class NoticeTask {

    private final static Logger logger = LoggerFactory.getLogger(NoticeTask.class);

    @Resource
    private IBillService billService;
    @Resource
    private IContractService contractService;

    /**
     * 每天中午做的事情
     * 1. 即将到期合同微信提醒
     * 2. 超期账单微信提醒
     */
    @Scheduled(cron = "0 0 12 * * ?")
    public void execute() {

        checkoutHouse();
        overdueBill();

    }

    /**
     * 即将到期合同微信提醒
     */
    private void checkoutHouse(){

        LocalDateTime now = LocalDateTime.now();
        logger.info("~~"+now.toString()+"即将到期合同微信提醒开始~~");
            contractService.checkoutHouse();
        logger.info("~~"+now.toString()+"即将到期合同微信提醒结束~~");

    }

    /**
     * 超期账单微信提醒
     */
    private void overdueBill(){

        LocalDateTime now = LocalDateTime.now();
        logger.info("~~"+now.toString()+"超期账单微信提醒开始~~");
        billService.overdueBill();
        logger.info("~~"+now.toString()+"超期账单微信提醒结束~~");

    }




}
