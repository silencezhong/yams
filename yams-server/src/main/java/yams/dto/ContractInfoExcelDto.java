package yams.dto;

import lombok.Getter;
import lombok.Setter;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelTarget;

import java.io.Serializable;

@Getter
@Setter
@SuppressWarnings("serial")
@ExcelTarget("contractInfoDataExcel")
public class ContractInfoExcelDto implements Serializable {
    /**城市**/
    @Excel(name="城市")
    String cityName;
    /**公寓名称**/
    @Excel(name="公寓名称")
    String apartmentName;
    /**管理员名称**/
    @Excel(name="管理员名称")
    String manageName;
    /**合同编号**/
    @Excel(name="合同编号")
    String contractNo;
    /**房号**/
    @Excel(name="房号")
    String houseNo;
    /**房屋类型**/
    @Excel(name="房屋类型")
    String houseType;
    /**客户名称**/
    @Excel(name="客户名称")
    String customerName;
    /**客户电话**/
    @Excel(name="客户电话")
    String customerPhone;
    /**客户身份证**/
    @Excel(name="客户身份证")
    String idCard;
    /**支付方式**/
    @Excel(name="支付方式")
    String payMethodStr;
    /**每月租金**/
    @Excel(name="每月租金")
    String monthlyRentStr;
    /**押金**/
    @Excel(name="押金")
    String pledgeStr;
    /**租金到期日**/
    @Excel(name="租金到期日")
    String rentalEndDate;
    /**租房开始日期**/
    @Excel(name="租房开始日期")
    String rentStartDateStr;
    /**租房结束日期**/
    @Excel(name="租房结束日期")
    String rentEndDateStr;
    /**账单金额**/
    @Excel(name="账单金额")
    String billAmountStr;
    /**是否快到期**/
    @Excel(name="是否快到期")
    String isDelayStr;
    /**超期天数**/
    @Excel(name="租金剩余天数")
    String delayDayStr;
}
