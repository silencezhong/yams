package yams.dto;

import lombok.Getter;
import lombok.Setter;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelTarget;

import java.io.Serializable;

/**
 * 月收入excel格式
 * Created by Luffy on 2018/05/15.
 */
@Getter
@Setter
@SuppressWarnings("serial")
@ExcelTarget("monthIncomeDataExcel")
public class MonthIncomeExcelDto implements Serializable {

    @Excel(name="时间")
    String billDateStr;
    @Excel(name="账单号")
    String billNo;
    @Excel(name="账单名称")
    String billName;
    @Excel(name="账单金额")
    String billAmountStr;
    @Excel(name="账单类型")
    String billTypeStr;
    @Excel(name="客户名称")
    String customerName;
    @Excel(name="客户联系方式")
    String customerTel;
    @Excel(name="收款人")
    String receiverName;
    @Excel(name="收款金额")
    String receiveAmountStr;
    @Excel(name="收款方式")
    String recieveWay;
    @Excel(name="城市")
    String cityName;
    @Excel(name="公寓")
    String apartmentName;
    @Excel(name="管家")
    String managerName;
    @Excel(name="备注")
    String remark;




}
