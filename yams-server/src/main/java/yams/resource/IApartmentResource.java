package yams.resource;

import yams.domain.ApartmentEntity;
import yams.domain.RoomGroupEntity;
import yams.response.Response;

/**
 * 公寓资源
 * Created by Silence on 2016/11/1.
 */
public interface IApartmentResource extends IResourceLayer<ApartmentEntity> {

    Response<ApartmentEntity> apartmentTree(String parentId);
    Response<RoomGroupEntity> roomGroupTree(String parentId);
    Response<ApartmentEntity> deleteRoomGroup(String id);
}
