package yams.resource;

import yams.domain.*;
import yams.response.Response;

/**
 * 资产类别树
 * Created by Silence on 2016/11/3.
 */
public interface ICategoryResource extends IResourceLayer<CategoryEntity> {

    Response<CategoryEntity> categoryTree(String parentId);

    Response<AttributeEntity> pageAttr(AttributeEntity attr);

    Response<AttributeEntity> saveAttr(AttributeEntity attr);

    Response<AttributeEntity> findAttrById(String attrId);

    Response<AttributeEntity> findAttrByCode(String attrCode);

    Response<CateAttrEntity> findCateAttr(String cateId);

    Response<CategoryEntity> saveCateAttr(CategoryEntity categoryEntity);

    Response<ProductEntity> pageProduct(ProductEntity product);

    Response<ProductEntity> saveProduct(ProductEntity product);

    Response<ProductEntity> findProductById(String prdId);

    Response<ProductEntity> findProductByCode(String code);

    Response<SkuEntity> findSkuByProduct(String productId);

    Response<SkuEntity> pageSku(SkuEntity sku);
}
