package yams.resource;

import yams.domain.RoleEntity;

/**
 * 角色服务对外接口
 * Created by Silence on 2016/11/1.
 */
public interface IRoleResource extends IResourceLayer<RoleEntity> {
}
