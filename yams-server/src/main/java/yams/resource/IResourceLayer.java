package yams.resource;

import yams.domain.AccountEntity;
import yams.response.Response;

/**
 * 资源抽象层
 * Created by Silence on 2016/10/31.
 */
public interface IResourceLayer<T> {

    Response<T> page(T t);

    Response<T> findById(String id);

    Response<T> findByCode(String code);

    Response<T> save(T t);

    Response<T> delete(String id);

    default void initService(){}
}
