package yams.resource;

import yams.domain.ModelEntity;
import yams.domain.ModelItemEntity;
import yams.response.Response;

/**
 * 模型服务对外接口
 * Created by Silence on 2016/11/1.
 */
public interface IModelResource extends IResourceLayer<ModelEntity> {

    Response<ModelEntity> modelTree(String parentId);

    Response<ModelItemEntity> saveItem(ModelItemEntity item);

    Response<ModelItemEntity> findItems(ModelItemEntity item);

    Response<ModelItemEntity> deleteItem(String itemId);

    Response<ModelItemEntity> findItem(String itemId);
}
