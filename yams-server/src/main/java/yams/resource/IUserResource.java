package yams.resource;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import yams.domain.*;
import yams.response.JsonResponse;
import yams.response.Response;

/**
 * 会员服务接口
 * Created by Silence on 2016/10/18.
 */
public interface IUserResource extends IResourceLayer<UserEntity> {

    Response<UserInspectionEntity> pageUserInspections(UserInspectionEntity inspection);
    Response<UserInspectionEntity> saveUserInspections(UserInspectionEntity inspection);
    Response<UserInspectionEntity> findUserInspection(String id);
    Response<UserInspectionEntity> delUserInspection(String id);
    Response<UserInspectionEntity> confirmUserInspection(String id);
    Response<UserInspectionEntity> cancelUserInspection(String id);

    Response<UserReserved> pageUserReserveds(UserReserved reserved);
    Response<UserReserved> saveUserReserved(UserReserved reserved);
    Response<UserReserved> findUserReserveds(String id);
    Response<UserReserved> delUserReserved(String id);
    Response<UserReserved> confirmUserReserved(String id);
    Response<UserReserved> cancelUserReserved(String id);
    Response<UserReserved> cancelReservedByUser(String id);
    Response<UserReserved> advanceOrder(String id);

    Response<UserEntity> findUserByPhone(String introducer);
    Response<UserEntity> findUserForSuggest(String keyword);
    Response<UserEntity> bindUser(UserEntity userEntity);
    Response<UserEntity> idCardUpload(UserEntity user);
    Response<UserReserved> payOrder(UserReserved param);
    Response<EmptyEntity> info( String id);


}
