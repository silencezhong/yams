package yams.resource;

import yams.domain.CouponModelEntity;

/**
 * 券模型服务接口
 * Created by Luffy on 2017/06/22.
 */
public interface ICouponModelResource extends IResourceLayer<CouponModelEntity> {


}
