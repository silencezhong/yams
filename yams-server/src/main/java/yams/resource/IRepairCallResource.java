package yams.resource;

import yams.domain.RepairCallEntity;
import yams.response.Response;

/**
 * 报修对外接口
 * Created by Silence on 2016/11/1.
 */
public interface IRepairCallResource extends IResourceLayer<RepairCallEntity> {

    Response<RepairCallEntity> handleRepairCall(RepairCallEntity repairCall);
    Response<RepairCallEntity> page_name(RepairCallEntity repairCall);
}
