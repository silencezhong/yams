package yams.resource;

import yams.domain.EnumEntity;
import yams.domain.EnumItemEntity;
import yams.response.Response;

/**
 * 枚举值接口服务
 * Created by Silence on 2016/11/1.
 */
public interface IEnumResource extends IResourceLayer<EnumEntity> {

    Response<EnumItemEntity> findItems(String code);
}
