package yams.resource;

import yams.domain.HomePageInfo;
import yams.response.Response;

/**
 * 首页信息展现
 * Created by Silence on 2017/5/4.
 */
public interface IHomePageResource extends IResourceLayer<HomePageInfo> {

    Response<HomePageInfo> homePage(HomePageInfo homePageInfo);
}
