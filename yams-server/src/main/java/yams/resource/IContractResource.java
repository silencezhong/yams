package yams.resource;

import yams.domain.ContractEntity;
import yams.response.Response;

/**
 * 合同管理
 * Created by Silence on 2016/11/1.
 */
public interface IContractResource extends IResourceLayer<ContractEntity> {
    //自动创建合同编码
    Response<ContractEntity> autoCode(ContractEntity contractEntity);

    //修改合同状态
    Response<ContractEntity> saveStatus(ContractEntity contractEntity);
}
