package yams.resource;

import yams.domain.*;
import yams.response.Response;

import java.util.Map;


/**
 * 房屋信息
 * Created by Silence on 2016/11/1.
 */
public interface IReportResource {


    Response<ReportMonthIncomeInfo> genMonthIncomeReport(ReportRequestObj condition);
    Response<ContractReportInfo>   genContractReport(ContractReportRequestObj condition);

}
