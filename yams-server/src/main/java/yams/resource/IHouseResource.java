package yams.resource;

import yams.domain.DayPriceEntity;
import yams.domain.HouseEntity;
import yams.response.Response;

import java.util.Map;


/**
 * 房屋信息
 * Created by Silence on 2016/11/1.
 */
public interface IHouseResource extends IResourceLayer<HouseEntity> {

    Response<HouseEntity> saveHouseProduct(HouseEntity house);

    Response<HouseEntity> findHouseByCity(HouseEntity house);

    Response<HouseEntity> cloneHouse(String id);

    Response<HouseEntity> saveRelation(HouseEntity houseEntity);

    Response<DayPriceEntity> findHouseDayPriceById(String houseId);

    Response<HouseEntity> saveHouseGroup(HouseEntity houseEntity);

    Response<HouseEntity> saveHouseGroup(Map<String,Object> map);

    Response<HouseEntity> mobileIndex(HouseEntity houseEntity);

    Response<HouseEntity> mobileIndexRecommend(HouseEntity houseEntity);

    Response<HouseEntity> mobileShortList(HouseEntity houseEntity);

    Response<HouseEntity> randomMonth(String houseGroup, String apartmentCity);
    Response<HouseEntity> randomDay(String houseGroup, String apartmentCity);


}
