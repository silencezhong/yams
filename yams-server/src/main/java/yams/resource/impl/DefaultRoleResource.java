package yams.resource.impl;

import org.springframework.web.bind.annotation.*;
import yams.domain.RoleEntity;
import yams.resource.IRoleResource;
import yams.resource.JsonCommonResource;
import yams.response.JsonResponse;
import yams.service.ICommonService;
import yams.service.impl.DefaultCommonService;

/**
 * 角色服务
 * Created by Silence on 2016/11/1.
 */
@SuppressWarnings("unchecked")
@RestController
@RequestMapping("/role")
public class DefaultRoleResource extends JsonCommonResource implements IRoleResource {

    private ICommonService roleService;

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @Override
    public JsonResponse<RoleEntity> page(@RequestBody RoleEntity roleEntity) {
        return _pageAll(roleService, roleEntity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Override
    public JsonResponse<RoleEntity> findById(@PathVariable String id) {
        return _findOne(roleService, id, 0);
    }

    @RequestMapping(value = "/code/{code}", method = RequestMethod.GET)
    @Override
    public JsonResponse<RoleEntity> findByCode(@PathVariable String code) {
        return _findOne(roleService, code, 1);
    }

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    @Override
    public JsonResponse<RoleEntity> save(@RequestBody RoleEntity account) {
        return _saveInfo(roleService, account);
    }

    @RequestMapping(value = "/", method = RequestMethod.DELETE)
    @Override
    public JsonResponse<RoleEntity> delete(@PathVariable String id) {
        return _delete(roleService, id);
    }

    @Override
    public void initService() {
        if (roleService == null)
            roleService = new DefaultCommonService("roleRepository");
    }
}
