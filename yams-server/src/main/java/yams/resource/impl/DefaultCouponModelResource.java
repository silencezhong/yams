package yams.resource.impl;

import javafx.util.Callback;
import org.springframework.jdbc.support.incrementer.MySQLMaxValueIncrementer;
import org.springframework.web.bind.annotation.*;
import yams.domain.CouponEntity;
import yams.domain.CouponModelEntity;
import yams.domain.UserEntity;
import yams.resource.ICouponModelResource;
import yams.resource.ICouponResource;
import yams.resource.JsonCommonResource;
import yams.response.JsonResponse;
import yams.service.ICommonService;
import yams.service.impl.DefaultCommonService;
import yams.utils.BreezeeUtils;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * 券服务
 * Created by Luffy on 2017/06/22.
 */
@SuppressWarnings("unchecked")
@RestController
@RequestMapping("/couponModel")
public class DefaultCouponModelResource extends JsonCommonResource implements ICouponModelResource {

    private ICommonService couponModelService;
    private ICommonService userService;

    @Resource
    private MySQLMaxValueIncrementer mySequence;

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @Override
    public JsonResponse<CouponModelEntity> page(@RequestBody CouponModelEntity couponModelEntity) {
        //日期查找bug---格式化日期类型
        try {
            if (couponModelEntity.getProperties().get("_startDate") != null) {
                couponModelEntity.getProperties().put("startDate_gt", BreezeeUtils.DATE_FORMAT_SHORT.parse(couponModelEntity.getProperties().get("_startDate").toString()));
            }
            if (couponModelEntity.getProperties().get("_endDate") != null) {
                couponModelEntity.getProperties().put("startDate_le", BreezeeUtils.DATE_FORMAT_SHORT.parse(couponModelEntity.getProperties().get("_endDate").toString()));
            }
        } catch (ParseException e) {
            //e.printStackTrace();
        }
        return _pageAll(couponModelService, couponModelEntity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Override
    public JsonResponse<CouponModelEntity> findById(@PathVariable String id) {
        return _findOne(couponModelService, id, 0, new Callback<CouponModelEntity, Object>() {
            @Override
            public Object call(CouponModelEntity param) {
                if (param != null) {
                    JsonResponse<UserEntity> r = _pageAll(userService, new UserEntity());
                    Long total = r.getTotal();
                    param.getProperties().put("userTotal",total);
                }
                return null;
            }
        });
    }

    @RequestMapping(value = "/code/{code}", method = RequestMethod.GET)
    @Override
    public JsonResponse<CouponModelEntity> findByCode(@PathVariable String code) {
        return _findOne(couponModelService, code, 1);
    }

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    @Override
    public JsonResponse<CouponModelEntity> save(@RequestBody CouponModelEntity couponModelEntity) {
        return _saveInfo(couponModelService, couponModelEntity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @Override
    public JsonResponse<CouponModelEntity> delete(@PathVariable String id) {
        return _delete(couponModelService, id);
    }

    @Override
    public void initService() {
        if (couponModelService == null)
            couponModelService = new DefaultCommonService("couponModelRepository");
        if (userService == null)
            userService = new DefaultCommonService("userRepository");
    }

   /* @RequestMapping(value = "/bind", method = RequestMethod.POST)
    @Override
    public JsonResponse<CouponModelEntity> bindUser(@RequestBody CouponModelEntity couponModelEntity) {
        if (StringUtils.hasText(couponModelEntity.getPhone())) {
            CouponModelEntity old = ((IUserRepository) ((IRepository) couponService.getRepository())).findByPhone(couponModelEntity.getPhone());
            if (old == null) {
                return JsonResponse.ERROR("绑定的手机号未注册，请注册");
            }
            old.setWeChat(couponModelEntity.getWeChat());
            old.setFollowDate(new Date());
            couponService.saveInfo(old);
        }
        return JsonResponse.ERROR("手机号为空，请填写");
    }*/
}
