package yams.resource.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.springframework.beans.BeanUtils;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import yams.common.InfoPage;
import yams.domain.*;
import yams.dto.ContractInfoExcelDto;
import yams.dto.MonthIncomeExcelDto;
import yams.resource.IReportResource;
import yams.resource.JsonCommonResource;
import yams.response.JsonResponse;
import yams.response.Response;
import yams.service.*;
import yams.utils.ExcelUtils;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 报表对外接口实现类
 * Created by Silence on 2016/11/1.
 */
@SuppressWarnings("unchecked")
@RestController
@RequestMapping("/report")
public class DefaultReportResource extends JsonCommonResource implements IReportResource {

    @Resource
    private IReportService reportService;

    @RequestMapping(value = "/monthIncome", method = RequestMethod.POST)
    @Override
    public JsonResponse<ReportMonthIncomeInfo> genMonthIncomeReport(@RequestBody ReportRequestObj condition) {

        InfoPage<ReportMonthIncomeInfo> page = reportService.queryMonthIncomeData(condition);

        return JsonResponse.build(page);
    }

    @RequestMapping(value = "/contractInfo", method = RequestMethod.POST)
    @Override
    public JsonResponse<ContractReportInfo> genContractReport(@RequestBody ContractReportRequestObj condition) {

        InfoPage<ContractReportInfo> list=  reportService.queryContractReportData(condition);
        return JsonResponse.build(list);
    }
    /**
     * 合同报表导出
     * @param
     * @return
     */
    @RequestMapping(value = "/export/contract_download", method = RequestMethod.POST , produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    ResponseEntity<InputStreamResource> exportContractinfo(String city,
                                                          String manager_like,
                                                          String apartment_like,
                                                          String user_like,
                                                          String _startDate,
                                                          String _endDate)throws IOException {


        List<ContractInfoExcelDto> list = Lists.newArrayList();

        ContractReportRequestObj condition = new ContractReportRequestObj();

        Map<String, Object> map = Maps.newConcurrentMap();

        map.put("city", city);
        map.put("manager_like", manager_like);
        map.put("apartment_like", apartment_like);
        map.put("user_like", user_like);
        map.put("_startDate", _startDate);
        map.put("_endDate", _endDate);
        condition.setProperties(map);
        List<ContractReportInfo> dataList = reportService.listContractInfoData(condition);

        list = dataList.stream().map(data -> {

            ContractInfoExcelDto dto = new ContractInfoExcelDto();
            BeanUtils.copyProperties(data, dto);
            return dto;

        }).collect(Collectors.toList());



        String fileName = "合同报表";
        File templateFile = null;
        templateFile = ExcelUtils.export(ContractInfoExcelDto.class, "合同报表", fileName, list);

        FileSystemResource file = new FileSystemResource(templateFile);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Content-Disposition", "attachment; filename="+new String((file.getFilename()).getBytes("gb2312"), "iso-8859-1"));
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        return ResponseEntity
                .ok()
                .headers(headers)
                .contentLength(file.contentLength())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(new InputStreamResource(file.getInputStream()));
    }
    /**
     * 月收入报表导出
     * @param
     * @return
     */
    @RequestMapping(value = "/export/monthIncome_download", method = RequestMethod.POST , produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    ResponseEntity<InputStreamResource> exportMonthIncome(String city,
                                                          String manager_like,
                                                          String apartment_like,
                                                          String user_like,
                                                          String _startDate,
                                                          String _endDate)throws IOException {


        List<MonthIncomeExcelDto> list = Lists.newArrayList();

        ReportRequestObj condition = new ReportRequestObj();
        Map<String, Object> map = Maps.newConcurrentMap();

        map.put("city", city);
        map.put("manager_like", manager_like);
        map.put("apartment_like", apartment_like);
        map.put("user_like", user_like);
        map.put("_startDate", _startDate);
        map.put("_endDate", _endDate);
        condition.setProperties(map);
        List<ReportMonthIncomeInfo> dataList = reportService.listMonthIncomeData(condition);

        list = dataList.stream().map(data -> {

            MonthIncomeExcelDto dto = new MonthIncomeExcelDto();
            BeanUtils.copyProperties(data, dto);
            return dto;

        }).collect(Collectors.toList());



        String fileName = "月收入报表";
        File templateFile = null;
        templateFile = ExcelUtils.export(MonthIncomeExcelDto.class, "月收入报表", fileName, list);

        FileSystemResource file = new FileSystemResource(templateFile);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Content-Disposition", "attachment; filename="+new String((file.getFilename()).getBytes("gb2312"), "iso-8859-1"));
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        return ResponseEntity
                .ok()
                .headers(headers)
                .contentLength(file.contentLength())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(new InputStreamResource(file.getInputStream()));
    }


}
