package yams.resource.impl;

import com.vdurmont.emoji.EmojiParser;
import javafx.util.Callback;
import org.springframework.jdbc.support.incrementer.MySQLMaxValueIncrementer;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import yams.common.IRepository;
import yams.common.constants.*;
import yams.common.exception.EntityNotFoundException;
import yams.domain.*;
import yams.repository.IUserRepository;
import yams.resource.IUserResource;
import yams.resource.JsonCommonResource;
import yams.response.JsonResponse;
import yams.response.Response;
import yams.service.*;
import yams.service.impl.DefaultCommonService;
import yams.utils.BeanUtil;
import yams.utils.BreezeeUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * 会员服务
 * Created by Silence on 2016/11/1.
 */
@SuppressWarnings("unchecked")
@RestController
@RequestMapping("/user")
public class DefaultUserResource extends JsonCommonResource implements IUserResource {


    private ICommonService userInspectionService;

    @Resource
    private IUserReservedService userReservedService;
    @Resource
    private IUserService userService;
    @Resource
    private IBillService billService;
    @Resource
    private IHouseService houseService;
    @Resource
    private ICouponService couponService;
    @Resource
    private IContractService contractService;
    @Resource
    private MySQLMaxValueIncrementer mySequence;

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @Override
    public JsonResponse<UserEntity> page(@RequestBody UserEntity userEntity) {
        //日期查找bug---格式化日期类型
        try {
            if (userEntity.getProperties().get("_startDate") != null) {
                userEntity.getProperties().put("createdDate_gt", BreezeeUtils.DATE_FORMAT_SHORT.parse(userEntity.getProperties().get("_startDate").toString()));
            }
            if (userEntity.getProperties().get("_endDate") != null) {
                userEntity.getProperties().put("createdDate_le", BreezeeUtils.DATE_FORMAT_SHORT.parse(userEntity.getProperties().get("_endDate").toString()));
            }
        } catch (ParseException e) {
            //e.printStackTrace();
        }
        if (userEntity.getMemo1() != null) {
            userEntity.getProperties().put("memo1_like", userEntity.getMemo1());
        }
        JsonResponse<UserEntity> result = _pageAll(userService, userEntity);
        Iterator iterator= (Iterator) result.getRows().iterator();
        while (iterator.hasNext()) {
            UserEntity use = (UserEntity) iterator.next();
            if(!StringUtils.isEmpty(use.getNickname())){
              String nickname = EmojiParser.parseToUnicode(use.getNickname());
              use.setNickname(nickname);
            }
        }
        return result;

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Override
    public JsonResponse<UserEntity> findById(@PathVariable String id) {
        return _findOne(userService, id, 0, new Callback<UserEntity, Object>() {
            @Override
            public Object call(UserEntity param) {
                if (param != null) {
                    BillEntity billEntity = new BillEntity();
                    billEntity.getProperties().put("status", 0);
                    billEntity.getProperties().put("user_id_obj_ae", param.getId());
                    List<BillEntity> l = billService.listAll(billEntity);
                    if (l != null && l.size() > 0) {
                        BigDecimal total = new BigDecimal(0);
                        for (BillEntity entity : l) {
                            total = total.add(entity.getActualAmount());
                        }
                        param.setToPayAmount(total);
                    }
                }
                return null;
            }
        });
    }

    @RequestMapping(value = "/findByPhone/{introducer}", method = RequestMethod.GET)
    @Override
    public Response<UserEntity> findUserByPhone(@PathVariable String introducer) {
        UserEntity old = ((IUserRepository) ((IRepository) userService.getRepository())).findByPhone(introducer);
        System.out.println(old);
        return JsonResponse.buildSingle(old);
    }

    @RequestMapping(value = "/code/{code}", method = RequestMethod.GET)
    @Override
    public JsonResponse<UserEntity> findByCode(@PathVariable String code) {
        return _findOne(userService, code, 1);
    }

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    @Override
    public JsonResponse<UserEntity> save(@RequestBody UserEntity userEntity) {
//        if (userEntity.getIdCard() != null && userEntity.getIdCard().length() < 15)
//            return JsonResponse.ERROR("证件号不符合规则");
        UserEntity old=null;
        if (StringUtils.hasText(userEntity.getPhone())) {
            old = ((IUserRepository) ((IRepository) userService.getRepository())).findByPhone(userEntity.getPhone());
            if (old != null && StringUtils.isEmpty(userEntity.getId())) {
                System.out.print(old.getPhone()+"--------------------");
                return JsonResponse.ERROR("此手机号已被注册");
            }
        }
            userEntity.setIdCard(StringUtils.isEmpty(userEntity.getIdCard()) ? "0000" : userEntity.getIdCard());
            String s = userEntity.getIdCard();
            s = s.substring(s.length() - 4, s.length());
            userEntity.setMemo1(userEntity.getPhone() + "," + s);
            //保存会员认证状态
            userEntity.setCertification(userEntity.getCertification());
            if (StringUtils.isEmpty(userEntity.getCode())) {
                //顺序错乱，暂时先这样
                /*UserEntity userEntity1=new UserEntity();
                long userCount=userService.count(userEntity1);
                String str = String.valueOf(userCount+2);
                String num="";
                if(str.length()<8){
                    for (int i=0;i<8-str.length();i++){
                        num=num+0;
                    }
                }
                userEntity.setCode(num+str);*/
//                userEntity.setCode(mySequence.nextStringValue());
//                UserEntity userEntity2=userService.autoUserCode(userEntity);
                userEntity.setCode(userEntity.getPhone());
            }

            if(old != null&&!StringUtils.isEmpty(old.getNickname())){
                String nickname=old.getNickname();
                EmojiParser.parseToAliases(nickname);
                userEntity.setNickname(nickname);
            }
            if (userEntity.getWeChat() != null)
                userEntity.setFollowDate(new Date());
            return _saveInfo(userService, userEntity);

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @Override
    public JsonResponse<UserEntity> delete(@PathVariable String id) {
        return _delete(userService, id);
    }

    @RequestMapping(value = "/uploadIdCard/", method = RequestMethod.POST)
    @Override
    public JsonResponse<UserEntity> idCardUpload(@RequestBody UserEntity user) {
        UserEntity ue = (UserEntity) userService.findById(user.getId());
        if(!StringUtils.isEmpty(user.getIdCardPhoto())){
            String[] photos = user.getIdCardPhoto().split(",");
            if(photos.length > 1){
                ue.setIcFrontImg("/idCard/"+photos[0]+".jpg");
                ue.setIcBackImg("/idCard/"+photos[1]+".jpg");
            }else{
                ue.setIcFrontImg("/idCard/"+photos[0]+".jpg");
            }
        }

        return _saveInfo(userService,ue);
    }

    @Override
    public void initService() {
        if (userInspectionService == null){
            userInspectionService =  new DefaultCommonService("userInspectionRepository");
        }
    }

    @RequestMapping(value = "/reserved/", method = RequestMethod.POST)
    @Override
    public JsonResponse<UserReserved> pageUserReserveds(@RequestBody UserReserved reserved) {
        try {
            if (reserved.getProperties().get("_startDate") != null) {
                reserved.getProperties().put("arrivedDate_gt", BreezeeUtils.DATE_FORMAT_SHORT.parse(reserved.getProperties().get("_startDate").toString()));
            }
            if (reserved.getProperties().get("_endDate") != null) {
                reserved.getProperties().put("arrivedDate_le", BreezeeUtils.DATE_FORMAT_SHORT.parse(reserved.getProperties().get("_endDate").toString()));
            }
            if (reserved.getProperties().get("_startDate_t") != null) {
                reserved.getProperties().put("leavedDate_gt", BreezeeUtils.DATE_FORMAT_SHORT.parse(reserved.getProperties().get("_startDate_t").toString()));
            }
            if (reserved.getProperties().get("_endDate_t") != null) {
                reserved.getProperties().put("leavedDate_le", BreezeeUtils.DATE_FORMAT_SHORT.parse(reserved.getProperties().get("_endDate_t").toString()));
            }
        } catch (ParseException e) {
//            e.printStackTrace();
        }

        return _pageAll(userReservedService, reserved);
    }

    /**
     * 查预约看房订单
     * @param condition
     * @return
     */
    @RequestMapping(value = "/inspection/", method = RequestMethod.POST)
    @Override
    public JsonResponse<UserInspectionEntity> pageUserInspections(@RequestBody UserInspectionEntity condition) {
        try {
            if (condition.getProperties().get("_startDate") != null) {
                condition.getProperties().put("checkRoomDate_gt", BreezeeUtils.DATE_FORMAT_SHORT.parse(condition.getProperties().get("_startDate").toString()));
            }
            if (condition.getProperties().get("_endDate") != null) {
                condition.getProperties().put("checkRoomDate_le", BreezeeUtils.DATE_FORMAT_SHORT.parse(condition.getProperties().get("_endDate").toString()));
            }
           /* if (condition.getProperties().get("_startDate_t") != null) {
                condition.getProperties().put("leavedDate_gt", BreezeeUtils.DATE_FORMAT_SHORT.parse(condition.getProperties().get("_startDate_t").toString()));
            }
            if (condition.getProperties().get("_endDate_t") != null) {
                condition.getProperties().put("leavedDate_le", BreezeeUtils.DATE_FORMAT_SHORT.parse(condition.getProperties().get("_endDate_t").toString()));
            }*/
        } catch (ParseException e) {
//            e.printStackTrace();
        }

        return _pageAll(userInspectionService, condition);
    }


    @RequestMapping(value = "/reserved/", method = RequestMethod.PUT)
    @Override
    public JsonResponse<UserReserved> saveUserReserved(@RequestBody UserReserved reserved) {
        HouseEntity he = (HouseEntity) houseService.findById(reserved.getHouse().getId());
        UserEntity user=userService.findById(reserved.getUser().getId());

        //如果是(有)同一个人且还没退房就可以将房间状态设为空可续住
        Date now=new Date();
        UserReserved reserved1=new UserReserved();
        reserved1.getProperties().put("rowNum",1);
        List<UserReserved> all=userReservedService.listAll(reserved1);
        for(int i=0;i<all.size();i++){
            if(user.getId()==all.get(i).getUser().getId() ){
                if(he.getId()==all.get(i).getHouse().getId()){
                    if(all.get(i).getLeavedDate().getTime()>now.getTime()){
                        he.setStatus(1);
                    }
                }
            }
        }
        //TODO:我们可能需要有个任务，定时去统计各个房间的预订情况
        if (he != null) {
            if (!Objects.equals(he.getStatus(), HouseStatusEnum.EMPTY.getValue())) {
                return JsonResponse.ERROR("此房间当前状态为" + he.getStatusName() + "，请重新选择");
            }
            //原来逻辑只有在后台人工确认了之后，才设置房屋的状态reserved.getStatus() == 2
            //我觉得欠妥，我觉得只要客户下单了，就应该设置房屋状态
            //否则会有这种场景，两个客户，同时订一间房间，然后管理员先确认了其中一个订单，那么另一个订单就无法确认，会被迫取消订单，需要重新下单
            //严重影响客户体验
            if (Objects.equals(reserved.getOrderStatus(),"OS01")
                    || Objects.equals(reserved.getOrderStatus(),"OS00")) {
                he.setStatus(HouseStatusEnum.BOOKED.getValue());
                if (he.getStartDate() != null && he.getStartDate().after(reserved.getArrivedDate())) {
                    he.setStartDate(reserved.getArrivedDate());
                }
                if (he.getEndDate() != null && he.getEndDate().before(reserved.getLeavedDate())) {
                    he.setEndDate(reserved.getLeavedDate());
                }
                houseService.saveInfo(he);
            }
        }
        //查看用户状态,还有出现空的情况
        if(Objects.equals(0,user.getStatus())){
            return JsonResponse.ERROR("你已被禁用，无法进行预订");
        }
        //实名认证状态
        if(!(user.getCertification().equals("2"))){
            return JsonResponse.ERROR("未通过实名认证，无法进行预订");
        }
        //若使用优惠券
        CouponEntity coupon=couponService.findById(reserved.getCoupon().getId());
        if(coupon != null){
            coupon.setStatus(3);
            couponService.saveInfo(coupon);
            reserved.setUseCoupon(true);
            reserved.setCoupon(coupon);
        }else{
            reserved.setUseCoupon(false);
            reserved.setCoupon(null);

        }

        return _saveInfo(userReservedService, reserved, (Callback<UserReserved, Object>) param -> {
            String s = String.valueOf(System.nanoTime());
            param.setName(param.getContactName() + "_" + LocalDate.now().toString());
            param.setCode(s);
            return null;
        });
    }

    /**
     * 提交短租订单后，发送微信消息
     * @param userReserved
     * @return
     */
    @RequestMapping(value = "/userReserved/sendWechatMsg", method = RequestMethod.PUT)
    public JsonResponse<UserReserved> sendWechatMsg(@RequestBody UserReserved userReserved) {

        userReservedService.sendWechatMsg(userReserved.getId());
        return JsonResponse.OK();

    }

    /**
     * 确认短租订单后，微信需要的相关消息
     * @param condition
     * @return
     */
    @RequestMapping(value = "/userReserved/infos", method = RequestMethod.POST)
    public JsonResponse<EmptyEntity> infos(@RequestBody EmptyEntity condition) {

        EmptyEntity result = userReservedService.queryInfos(condition.getProperties().get("id").toString());
        return JsonResponse.buildSingle(result);

    }

    /**
     * 保存预约看房订单
     * @param entity
     * @return
     */
    @RequestMapping(value = "/inspection/", method = RequestMethod.PUT)
    @Override
    public JsonResponse<UserInspectionEntity> saveUserInspections(@RequestBody UserInspectionEntity entity) {
        HouseEntity he = (HouseEntity) houseService.findById(entity.getHouse().getId());
        UserEntity _user=(UserEntity) userService.findById(entity.getUserId());
        //TODO:我们可能需要有个任务，定时去统计各个房间的预订情况
        if (he != null) {
            if (!Objects.equals(he.getStatus(), HouseStatusEnum.EMPTY.getValue())) {
                return JsonResponse.ERROR("此房间当前状态为" + he.getStatusName() + "，请重新选择");
            }
        }
        //查看用户状态,还有出现空的情况
        if(Objects.equals(0,_user.getStatus())){
            return JsonResponse.ERROR("你已被禁用，无法进行预订");
        }
        //实名认证状态
        System.out.println(_user.getCertification().equals("2"));
        if(!(_user.getCertification().equals("2"))){
            return JsonResponse.ERROR("未通过实名认证，无法进行预订");
        }
        return _saveInfo(userInspectionService, entity, (Callback<UserInspectionEntity, Object>) param -> {
            if(StringUtils.isEmpty(param.getId())){
                param.setOrderStatus(InspectionOrderStatusEnum.TODO.getValue());
            }
            String s = LocalDateTime.now().toString();
            param.setName(param.getContactName() + "_" + s);
            param.setCode(param.getContactMobile() + "_" + s);
            return null;
        });
    }

    /**
     * 确认订单
     * @param id
     * @return
     */
    @RequestMapping(value = "/reserved/confirm/{id}", method = RequestMethod.PUT)
    @Override
    public JsonResponse<UserReserved> confirmUserReserved(@PathVariable String id) {

        UserReserved userReserved = (UserReserved) userReservedService.findById(id);
        userReserved.setOrderStatus(OrderStatusEnum.CONFIRM.getValue());
        userReservedService.saveInfo(userReserved);
        return JsonResponse.OK();
    }

    /**
     * 确认预约申请
     * @param id
     * @return
     */
    @RequestMapping(value = "/inspection/confirm/{id}", method = RequestMethod.PUT)
    @Override
    public JsonResponse<UserInspectionEntity> confirmUserInspection(@PathVariable String id) {

        UserInspectionEntity order = (UserInspectionEntity) userInspectionService.findById(id);
        HouseEntity house = order.getHouse();
        UserEntity manager = userService.findById(house.getManager());
        order.getProperties().put("managerName",manager.getName());
        order.getProperties().put("managerPhone",manager.getPhone());
        if(order.getOrderStatus().equals(InspectionOrderStatusEnum.TODO.getValue())){
            order.setOrderStatus(InspectionOrderStatusEnum.CONFIRM.getValue());
        }
        else{
            order.setOrderStatus(InspectionOrderStatusEnum.LOOKED.getValue());
        }
        userInspectionService.saveInfo(order);

        return JsonResponse.buildSingle(order);

    }

    /**
     * 后台取消订单
     * @param id
     * @return
     */
    @RequestMapping(value = "/reserved/cancel/{id}", method = RequestMethod.DELETE)
    @Override
    public JsonResponse<UserReserved> cancelUserReserved(@PathVariable String id) {

        UserReserved userReserved = (UserReserved) userReservedService.findById(id);
        userReserved.setOrderStatus(OrderStatusEnum.W_CANCEL.getValue());
        //还原订单金额和券状态
        if(userReserved.getUseCoupon()){
            CouponEntity coupon = (CouponEntity) couponService.findById(userReserved.getCoupon().getId());
            coupon.setStatus(CouponStatusEnum.OBTAIN.getValue());
            couponService.saveInfo(coupon);

            userReserved.setRentPrice(userReserved.getRentPrice().add(userReserved.getCouponAmount()));
            userReserved.setUseCoupon(false);
            userReserved.setCoupon(null);
        }
        userReservedService.saveInfo(userReserved);

        HouseEntity he = (HouseEntity) houseService.findById(userReserved.getHouse().getId());
        he.setStatus(HouseStatusEnum.EMPTY.getValue());
        houseService.saveInfo(he);

        return JsonResponse.OK();
    }

    /**
     * 用戶取消订单
     * @param id
     * @return
     */
    @RequestMapping(value = "/reserved/cancelByUser/{id}", method = RequestMethod.GET)
    @Override
    public JsonResponse<UserReserved> cancelReservedByUser(@PathVariable String id) {

        UserReserved userReserved = (UserReserved) userReservedService.findById(id);
        userReserved.setOrderStatus(OrderStatusEnum.C_CANCEL.getValue());
        //还原订单金额和券状态
        if(userReserved.getUseCoupon()){
            CouponEntity coupon = (CouponEntity) couponService.findById(userReserved.getCoupon().getId());
            coupon.setStatus(CouponStatusEnum.OBTAIN.getValue());
            couponService.saveInfo(coupon);

            userReserved.setRentPrice(userReserved.getRentPrice().add(userReserved.getCouponAmount()));
            userReserved.setUseCoupon(false);
            userReserved.setCoupon(null);
        }


        userReservedService.saveInfo(userReserved);

        HouseEntity he = (HouseEntity) houseService.findById(userReserved.getHouse().getId());
        he.setStatus(HouseStatusEnum.EMPTY.getValue());
        houseService.saveInfo(he);

        return JsonResponse.OK();
    }

    /**
     * 后台取消预约申请
     * @param id
     * @return
     */
    @RequestMapping(value = "/inspection/cancel/{id}", method = RequestMethod.DELETE)
    @Override
    public JsonResponse<UserInspectionEntity> cancelUserInspection(@PathVariable String id) {

        UserInspectionEntity order = (UserInspectionEntity) userInspectionService.findById(id);
        order.setOrderStatus(InspectionOrderStatusEnum.W_CANCEL.getValue());
        userInspectionService.saveInfo(order);
        return JsonResponse.OK();

    }

    @RequestMapping(value = "/reserved/{id}", method = RequestMethod.GET)
    @Override
    public JsonResponse<UserReserved> findUserReserveds(@PathVariable String id) {
        return _findOne(userReservedService, id, 0);
    }

    /**
     * 提前退房
     * @param id
     * @return
     */
    @RequestMapping(value = "/reserved/cancel/{id}", method = RequestMethod.GET)
    @Override
    public JsonResponse<UserReserved> advanceOrder(@PathVariable String id) {
        UserReserved userReserved=userReservedService.findById(id);
        userReserved.setLeavedDate(new Date());
        //设置为空房
        userReserved.getHouse().setStatus(HouseStatusEnum.EMPTY.getValue());
        userReserved.setOrderStatus(OrderStatusEnum.H_EXIT.getValue());
        userReservedService.saveInfo(userReserved);
        return JsonResponse.OK();
    }

    /**
     * 根据主键查预约看房订单
     * @param id
     * @return
     */
    @RequestMapping(value = "/inspection/{id}", method = RequestMethod.GET)
    @Override
    public JsonResponse<UserInspectionEntity> findUserInspection(@PathVariable String id) {
        return _findOne(userInspectionService, id, 0);
    }

    @RequestMapping(value = "/reserved/{id}", method = RequestMethod.DELETE)
    @Override
    public JsonResponse<UserReserved> delUserReserved(@PathVariable String id) {
        return _delete(userReservedService, id);
    }

    /**
     * 删除某个预约看房订单
     * @param id
     * @return
     */
    @RequestMapping(value = "/inspection/{id}", method = RequestMethod.DELETE)
    @Override
    public JsonResponse<UserInspectionEntity> delUserInspection(@PathVariable String id) {
        return _delete(userInspectionService, id);
    }

    @RequestMapping(value = "/suggest/", method = RequestMethod.GET)
    @Override
    public JsonResponse<UserEntity> findUserForSuggest(@RequestParam("keyword") String keyword) {
        UserEntity ue = new UserEntity();
        ue.getProperties().put("memo1_like", keyword);
        List<UserEntity> l = userService.listAll(ue);
        return JsonResponse.build(l);
    }

    @RequestMapping(value = "/bind", method = RequestMethod.POST)
    @Override
    public JsonResponse<UserEntity> bindUser(@RequestBody UserEntity userEntity) {
        if (StringUtils.hasText(userEntity.getPhone())) {
            UserEntity old = ((IUserRepository) ((IRepository) userService.getRepository())).findByPhone(userEntity.getPhone());
            if (old == null) {
                return JsonResponse.ERROR("绑定的手机号未注册，请注册");
            }
            old.setWeChat(userEntity.getWeChat());
            old.setStatus(userEntity.getStatus());
            old.setFollowDate(new Date());
            userService.saveInfo(old);
            return JsonResponse.ERROR("绑定成功");
        }else{
            return JsonResponse.ERROR("手机号为空，请填写");
        }

    }

    /**
     * 支付短租订单
     * @param param
     * @return
     */
    @RequestMapping(value = "/userReserved/pay", method = RequestMethod.POST)
    @Override
    public JsonResponse<UserReserved> payOrder(@RequestBody UserReserved param) {

        try {

            UserReserved order = (UserReserved)userReservedService.findById(param.getId());
            order.setPayMethod("wepay");
            order.setOrderStatus(OrderStatusEnum.PAID.getValue());
            userReservedService.getRepository().saveEntity(order);

            HouseEntity house = houseService.findById(order.getHouseId());
            house.setStatus(HouseStatusEnum.CHECKED.getValue());
            houseService.getRepository().saveEntity(house);

        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }

        return JsonResponse.OK();

    }

    @RequestMapping(value = "/info/{id}", method = RequestMethod.GET)
    @Override
    public JsonResponse<EmptyEntity> info(@PathVariable String id) {

        Boolean isSuccess = false;
        EmptyEntity result = new EmptyEntity();
        UserEntity user = userService.findById(id);
        if(user != null){
            ContractEntity contractCondition = new ContractEntity();
            contractCondition.getProperties().put("status", ContractStatusEnum.EXECUTING.getValue());
            contractCondition.getProperties().put("user_id_obj_ae",user.getId());
            List<ContractEntity> contractList = contractService.listAll(contractCondition);
            if(!CollectionUtils.isEmpty(contractList) && contractList.size() == 1){
                ContractEntity contract = contractList.get(0);
                HouseEntity house = houseService.findById(contract.getHouseId());
                UserEntity manager = userService.findById(house.getManager());

                isSuccess = true;
                result.getProperties().put("issueWechat", user.getWeChat());
                result.getProperties().put("issueUser", user.getId());
                result.getProperties().put("issueUserName", user.getName());
                result.getProperties().put("issueUserPhone", user.getPhone());
                result.getProperties().put("address", house.getMyAddress());
                result.getProperties().put("managerWechat", manager.getWeChat());
                result.getProperties().put("managerId", manager.getId());
                result.getProperties().put("managerName", manager.getName());
                result.getProperties().put("managerPhone", manager.getPhone());

            }
        }
        result.getProperties().put("isSuccess", isSuccess);
        return JsonResponse.buildSingle(result) ;
    }

}
