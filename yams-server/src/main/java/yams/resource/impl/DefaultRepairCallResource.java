package yams.resource.impl;

import javafx.util.Callback;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import yams.callback.IdCallback;
import yams.common.constants.BillStatusEnum;
import yams.common.constants.BillTypeEnum;
import yams.common.constants.ContractStatusEnum;
import yams.common.constants.RequestStatusEnum;
import yams.domain.*;
import yams.resource.IRepairCallResource;
import yams.resource.JsonCommonResource;
import yams.response.JsonResponse;
import yams.response.Response;
import yams.service.*;
import yams.service.impl.DefaultCommonService;
import yams.utils.BreezeeUtils;

import javax.annotation.Resource;
import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 报修资源
 * Created by Silence on 2016/11/1.
 */
@SuppressWarnings("unchecked")
@RestController
@RequestMapping("/repair")
public class DefaultRepairCallResource extends JsonCommonResource implements IRepairCallResource {

    private ICommonService repairCallService;
    @Resource
    private IUserService userService;
    @Resource
    private IContractService contractService;
    @Resource
    private IHouseService houseService;
    @Resource
    private IBillService billService;
    @Resource
    private IdCallback idCallback;

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @Override
    public Response<RepairCallEntity> page(@RequestBody RepairCallEntity repairCallEntity) {
        //日期查找bug---格式化日期类型
        try {
            if (repairCallEntity.getProperties().get("_startDate") != null) {
                repairCallEntity.getProperties().put("issueTime_gt", BreezeeUtils.DATE_FORMAT_SHORT.parse(repairCallEntity.getProperties().get("_startDate").toString()));
            }
            if (repairCallEntity.getProperties().get("_endDate") != null) {
                repairCallEntity.getProperties().put("issueTime_le", BreezeeUtils.DATE_FORMAT_SHORT.parse(repairCallEntity.getProperties().get("_endDate").toString()));
            }
        } catch (ParseException e) {
           //e.printStackTrace();
        }
        return _pageAll(repairCallService, repairCallEntity);
    }


    @RequestMapping(value = "/pageWithHandlerName", method = RequestMethod.POST)
    @Override
    public Response<RepairCallEntity> page_name(@RequestBody RepairCallEntity repairCallEntity) {
        //获得所有的RepairCallEntity
        JsonResponse<RepairCallEntity> r = _pageAll(repairCallService, repairCallEntity);
        List<RepairCallEntity> repairCall= (List<RepairCallEntity>) r.getRows();
        /*for(int i=0;i<repairCall.size();i++){
            String handerId=repairCall.get(i).getHandleUser();
            //通过handleId获得用户具体信息
            if(handerId==null || handerId==""){
                repairCall.get(i).setHandleUserName("无");
            }else{
                UserEntity user=userService.findByCode(handerId);
                if(user!=null){
                    repairCall.get(i).setHandleUserName(user.getName());
                }else{
                    repairCall.get(i).setHandleUserName("无");
                }
            }
        }*/
        return JsonResponse.build(repairCall);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Override
    public Response<RepairCallEntity> findById(@PathVariable String id) {
        return _findOne(repairCallService, id, 0);
    }

    @RequestMapping(value = "/code/{code}", method = RequestMethod.GET)
    @Override
    public Response<RepairCallEntity> findByCode(@PathVariable String code) {
        return _findOne(repairCallService, code, 1);
    }

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    @Override
    public Response<RepairCallEntity> save(@RequestBody RepairCallEntity repairCallEntity) {
        repairCallEntity.setIssueTime(new Date());
        if(!StringUtils.isEmpty(repairCallEntity.getMediaIds())){
            String[] repairImgs=repairCallEntity.getMedias();
            if(repairImgs.length==1){
                repairCallEntity.setAImage(repairImgs[0]);
            }
            if(repairImgs.length==2){
                repairCallEntity.setAImage(repairImgs[0]);
                repairCallEntity.setBImage(repairImgs[1]);
            }
            if(repairImgs.length==3){
                repairCallEntity.setAImage(repairImgs[0]);
                repairCallEntity.setBImage(repairImgs[1]);
                repairCallEntity.setCImage(repairImgs[2]);
            }
            if(repairImgs.length==4){
                repairCallEntity.setAImage(repairImgs[0]);
                repairCallEntity.setBImage(repairImgs[1]);
                repairCallEntity.setCImage(repairImgs[2]);
                repairCallEntity.setDImage(repairImgs[3]);
            }
            if(repairImgs.length==5){
                repairCallEntity.setAImage(repairImgs[0]);
                repairCallEntity.setBImage(repairImgs[1]);
                repairCallEntity.setCImage(repairImgs[2]);
                repairCallEntity.setDImage(repairImgs[3]);
                repairCallEntity.setEImage(repairImgs[4]);
            }
        }
        return _saveInfo(repairCallService, repairCallEntity, (Callback<RepairCallEntity, Object>) param -> {
            if (param.getCode() == null)
                param.setCode(System.currentTimeMillis() + "");
                param.setName(LocalDate.now()+"_"+param.getIssueUserName()+"_"+param.getType()+"报修");
            return null;
        });
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @Override
    public Response<RepairCallEntity> delete(@PathVariable String id) {
        return _delete(repairCallService, id);
    }

    @Override
    public void initService() {
        if (repairCallService == null)
            repairCallService = new DefaultCommonService("repairCallRepository");
    }

    @RequestMapping(value = "/handle", method = RequestMethod.POST)
    @Override
    public Response<RepairCallEntity> handleRepairCall(@RequestBody RepairCallEntity repairCall) {
        RepairCallEntity old = (RepairCallEntity) repairCallService.findById(repairCall.getId());
        old.setHandleTime(new Date());
        old.setResult(repairCall.getResult());
        old.setRepairFee(repairCall.getRepairFee());
        old.setStatus(RequestStatusEnum.PROCESSED.getValue());


        UserEntity user = userService.findById(old.getIssueUser());
        if(user != null){
            ContractEntity contractCondition = new ContractEntity();
            contractCondition.getProperties().put("status", ContractStatusEnum.EXECUTING.getValue());
            contractCondition.getProperties().put("user_id_obj_ae",user.getId());
            List<ContractEntity> contractList = contractService.listAll(contractCondition);
            if(!CollectionUtils.isEmpty(contractList) && contractList.size() == 1){
                ContractEntity contract = contractList.get(0);
                HouseEntity house = houseService.findById(contract.getHouseId());

                BillEntity bill = new BillEntity();
                bill.setCode("3" + old.getCode());
                bill.setName(old.getName());
                bill.setCreatedDate(new Date());
                bill.setRemark(repairCall.getRemark());
                bill.setTotalAmount(repairCall.getRepairFee());
                bill.setActualAmount(repairCall.getRepairFee());
                bill.setStatus(BillStatusEnum.UNPAID.getValue());
                bill.setContract(contract);
                bill.setHouse(house);
                bill.setUser(user);
                bill.setType(BillTypeEnum.REPAIR.getValue());
                billService.getRepository().saveEntity(bill, new Callback[]{idCallback});
            }
        }

        return _saveInfo(repairCallService, old);
    }
}
