package yams.resource.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import yams.common.constants.HouseStatusEnum;
import yams.common.constants.RequestStatusEnum;
import yams.domain.*;
import yams.resource.IHomePageResource;
import yams.response.JsonResponse;
import yams.response.Response;
import yams.service.IBillService;
import yams.service.ICommonService;
import yams.service.IReportService;
import yams.service.impl.DefaultCommonService;

import javax.annotation.Resource;

/**
 * Created by Silence on 2017/5/5.
 */
@RestController
@RequestMapping("/homePage")
public class DefaultHomePageResource implements IHomePageResource {

    private ICommonService repairCallService;
    private ICommonService houseService;
    private ICommonService userService;

    @Resource
    private IBillService billService;
    @Autowired
    private IReportService reportService;



    @RequestMapping(value = "/", method = RequestMethod.POST)
    @Override
    public Response<HomePageInfo> homePage(@RequestBody HomePageInfo homePageInfo) {

        // 报修
        RepairCallEntity repairCallEntity = new RepairCallEntity();
        repairCallEntity.getProperties().put("status", RequestStatusEnum.TODO.getValue());
        homePageInfo.setRepairNum(repairCallService.count(repairCallEntity));

        // 房屋 空房
        HouseEntity houseEntity = new HouseEntity();
        homePageInfo.setHouseNum(houseService.count(houseEntity));
        houseEntity.getProperties().put("status", HouseStatusEnum.EMPTY.getValue());
        homePageInfo.setEmptyHouse(houseService.count(houseEntity));

        // 超期账单
        BillEntity billEntity = new BillEntity();
        billEntity.getProperties().put("status", 0);
        homePageInfo.setDelayBillNum(billService.count(billEntity));

        // 客户数
        UserEntity userEntity = new UserEntity();
        homePageInfo.setUserNum(userService.count(userEntity));

        // 今日收入
        homePageInfo.setTodayIncome(reportService.todayIncome());
        // 临期房屋
        homePageInfo.setDeadlineHouse(reportService.deadlineHouse());


        return JsonResponse.buildSingle(homePageInfo);
    }

    @Override
    public Response<HomePageInfo> page(HomePageInfo homePageInfo) {
        return null;
    }

    @Override
    public Response<HomePageInfo> findById(String id) {
        return null;
    }

    @Override
    public Response<HomePageInfo> findByCode(String code) {
        return null;
    }

    @Override
    public Response<HomePageInfo> save(HomePageInfo homePageInfo) {
        return null;
    }

    @Override
    public Response<HomePageInfo> delete(String id) {
        return null;
    }

    @Override
    public void initService() {
        if (userService == null)
            userService = new DefaultCommonService("userRepository");
        if (houseService == null)
            houseService = new DefaultCommonService("houseRepository");
        if (repairCallService == null)
            repairCallService = new DefaultCommonService("repairCallRepository");
    }
}
