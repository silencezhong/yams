package yams.resource.impl;

import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import yams.domain.AccountEntity;
import yams.domain.ApartmentEntity;
import yams.resource.IDataAuthorityResource;
import yams.resource.JsonCommonResource;
import yams.response.JsonResponse;
import yams.service.ICommonService;
import yams.service.IDataauthorityService;
import yams.service.impl.DefaultCommonService;
import javax.annotation.Resource;
import java.util.*;


/**
 *数据权限接口
 * Created by Silence on 2016/11/1.
 */
@SuppressWarnings("unchecked")
@RestController
@RequestMapping("/dataAuthority")
public class DefaultDataAuthorityResource extends JsonCommonResource implements IDataAuthorityResource {

    private ICommonService dataAuthorityService;
    @Resource
    private IDataauthorityService dataauthorityService;

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @Override
    public JsonResponse<AccountEntity> page(@RequestBody AccountEntity accountEntity) {
        //在关联表中的所有账户
        List<AccountEntity> accounts=new ArrayList();
        accounts=dataauthorityService.getAllacn();
       return JsonResponse.build(accounts);
    }



    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Override
    public JsonResponse<AccountEntity> findById(@PathVariable String id) {
        //通过Id获得公寓
        //获得用户所有管理的公寓
        Set<ApartmentEntity> apartments=new HashSet<ApartmentEntity>();
        apartments=dataauthorityService.setAll(id);
        //通过id获得账户

        return _findOne(dataAuthorityService, id, 0);
    }

    @RequestMapping(value = "/code/{code}", method = RequestMethod.GET)
    @Override
    public JsonResponse<AccountEntity> findByCode(@PathVariable String code) {
        return _findOne(dataAuthorityService, code, 1);
    }


    // 保存账户和公寓之间的关联
    @RequestMapping(value = "/", method = RequestMethod.PUT)
    @Override
    public JsonResponse<AccountEntity> save(@RequestBody AccountEntity enu) {
        if(enu.getApartmens().size()!=0) {
            System.out.println(enu);
            Iterator<ApartmentEntity> apts = enu.getApartmens().iterator();
            while (apts.hasNext()) {
                ApartmentEntity apt=apts.next();
                System.out.println(enu.getId());
                System.out.println(apt.getId());
                dataauthorityService.updateAcnApmRel(enu.getId(),apt.getId());
                System.out.println("success");
            }
        }
        return JsonResponse.OK();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @Override
    public JsonResponse<AccountEntity> delete(@PathVariable String id) {
        return _delete(dataAuthorityService, id);
    }

    @Override
    public void initService() {
        if(dataAuthorityService==null)
            dataAuthorityService = new DefaultCommonService("dataAuthorityRepository");
    }

    //通过账号id获得所管理的公寓信息
    @RequestMapping(value =  "/getAllapmByacn", method = RequestMethod.POST)
    @Override
    public JsonResponse<ApartmentEntity> getApmsById(@RequestBody  AccountEntity accountEntity) {
        //获得用户所有管理的公寓
        List<ApartmentEntity> apartments=new ArrayList();
        String acnId=accountEntity.getId();
        apartments=dataauthorityService.getAllapt(acnId);
        //返回到页面展示
        return JsonResponse.build(apartments);
    }
}
