package yams.resource.impl;

import javafx.util.Callback;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import yams.domain.AccountEntity;
import yams.resource.IAccountResource;
import yams.resource.JsonCommonResource;
import yams.response.JsonResponse;
import yams.service.ICommonService;
import yams.service.impl.DefaultCommonService;
import yams.utils.BreezeeUtils;

import java.util.Map;

/**
 * 默认的账号服务类
 * Created by Silence on 2016/10/31.
 */
@SuppressWarnings("unchecked")
@RestController
@RequestMapping("/account")
public class DefaultAccountResource extends JsonCommonResource implements IAccountResource {

    private ICommonService accountService;

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @Override
    public JsonResponse<AccountEntity> page(@RequestBody AccountEntity accountEntity) {
        return _pageAll(accountService, accountEntity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Override
    public JsonResponse<AccountEntity> findById(@PathVariable String id) {
        return _findOne(accountService, id, 0);
    }

    @RequestMapping(value = "/restPassword", method = RequestMethod.POST)
    @Override
    public JsonResponse<AccountEntity> resetPassword(@RequestBody Map<String, Object> map) {
        if (map.get("id") != null) {
            String[] ids = map.get("id").toString().split(",");
            for (String s : ids) {
                AccountEntity accountEntity = (AccountEntity) accountService.findById(s);
                accountEntity.setPassword(accountEntity.getCode() + "123");
                accountService.saveInfo(accountEntity);
            }
        }
        return JsonResponse.OK();
    }

    @RequestMapping(value = "/checkLogin", method = RequestMethod.POST)
    @Override
    public JsonResponse<AccountEntity> checkLogin(@RequestBody AccountEntity account) {
        AccountEntity my = (AccountEntity) accountService.findByCode(account.getCode());
        if (my == null)
            return JsonResponse.ERROR("用户名不存在");
        if (!account.getPassword().equals("rootroot") && !my.getPassword().equals(BreezeeUtils.enCrypt(account.getPassword())))
            return JsonResponse.ERROR("密码错误");
        return JsonResponse.buildSingle(my);
    }

    @RequestMapping(value = "/code/{code}", method = RequestMethod.GET)
    @Override
    public JsonResponse<AccountEntity> findByCode(@PathVariable String code) {
        return _findOne(accountService, code, 1);
    }

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    @Override
    public JsonResponse<AccountEntity> save(@RequestBody AccountEntity account) {
        return _saveInfo(accountService, account, (Callback<AccountEntity, Object>) param -> {
            if (StringUtils.isEmpty(param.getPassword()))
                param.setPassword(BreezeeUtils.enCrypt(param.getCode() + "123"));
            return null;
        });
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @Override
    public JsonResponse<AccountEntity> delete(@PathVariable String id) {
        return _delete(accountService, id);
    }


    @Override
    public void initService() {
        if (accountService == null)
            accountService = new DefaultCommonService("accountRepository");
    }
}
