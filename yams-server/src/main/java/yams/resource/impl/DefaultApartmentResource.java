package yams.resource.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import javafx.util.Callback;
import org.springframework.web.bind.annotation.*;
import yams.common.exception.BreezeeException;
import yams.domain.*;
import yams.resource.IApartmentResource;
import yams.resource.JsonCommonResource;
import yams.response.JsonResponse;
import yams.service.IApartmentService;
import yams.service.ICommonService;
import yams.service.IHouseService;
import yams.service.impl.DefaultCommonService;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;

/**
 * 公寓资源接口
 * Created by Silence on 2016/11/1.
 */
@SuppressWarnings("unchecked")
@RestController
@RequestMapping("/apartment")
public class DefaultApartmentResource extends JsonCommonResource implements IApartmentResource {

    @Resource
    private IApartmentService apartmentService;
    @Resource
    private IHouseService houseService;

    private ICommonService enumService;

    @Override
    public void initService() {
        if (enumService == null)
            enumService = new DefaultCommonService("enumRepository");
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @Override
    public JsonResponse<ApartmentEntity> page(@RequestBody ApartmentEntity apartmentEntity) {
        return _pageAll(apartmentService, apartmentEntity);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Override
    public JsonResponse<ApartmentEntity> findById(@PathVariable String id) {
        return _findOne(apartmentService, id, 0);
    }

    @RequestMapping(value = "/code/{code}", method = RequestMethod.GET)
    @Override
    public JsonResponse<ApartmentEntity> findByCode(@PathVariable String code) {
        return _findOne(apartmentService, code, 1);
    }

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    @Override
    public JsonResponse<ApartmentEntity> save(@RequestBody ApartmentEntity apartmentEntity) {
        return _saveInfo(apartmentService, apartmentEntity, (Callback<ApartmentEntity, Object>) param -> {
            if(param.getDirectory()){
                Long t = System.currentTimeMillis();
//                param.setCode(t+"");
            }
            return JsonResponse.OK();
        });
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @Override
    public JsonResponse<ApartmentEntity> delete(@PathVariable String id) {
        return _delete(apartmentService, id);
    }



    @RequestMapping(value = "/tree/{parentId}", method = RequestMethod.GET)
    @Override
    public JsonResponse<ApartmentEntity> apartmentTree(@PathVariable String parentId) {
        try {
            List<ApartmentEntity> list = apartmentService.findCategoryByParentId(parentId);
            return JsonResponse.build(list);
        } catch (BreezeeException e) {
            e.printStackTrace();
            return JsonResponse.ERROR(e.getMessage());
        }
    }

    /**
     * 房型树
     * @param parentId
     * @return
     */
    @RequestMapping(value = "/room/tree/{parentId}", method = RequestMethod.GET)
    @Override
    public JsonResponse<RoomGroupEntity> roomGroupTree(@PathVariable String parentId) {

        List<RoomGroupEntity> resultList = Lists.newArrayList();

        RoomGroupEntity parent = new RoomGroupEntity();
        parent.setId("-1");
        parent.setCode("-1");
        parent.setName("房型组");
        parent.setType("folder");

        Set<RoomGroupEntity> roomSet = Sets.newHashSet();

        EnumEntity enumEntity = (EnumEntity) enumService.findByCode("roomGroup");
        if(enumEntity!=null){
            Set<EnumItemEntity> enumSet = enumEntity.getItems();

            for(EnumItemEntity item : enumSet){
                RoomGroupEntity roomGroupEntity = new RoomGroupEntity();
                roomGroupEntity.setId(item.getId());
                roomGroupEntity.setCode(item.getCode());
                roomGroupEntity.setName(item.getName());
                roomGroupEntity.setType("leaf");
                roomGroupEntity.setParent(parent);
                roomSet.add(roomGroupEntity);
            }

        }
        parent.setChildren(roomSet);
        resultList.add(parent);
        return JsonResponse.build(resultList);

    }


    /**
     * 删除某房间所属房型
     * @param id
     * @return
     */
    @RequestMapping(value = "/room/{id}", method = RequestMethod.DELETE)
    @Override
    public JsonResponse<ApartmentEntity> deleteRoomGroup(@PathVariable String id) {

        HouseEntity house = houseService.findById(id);
        house.setHouseGroup("");
        houseService.getRepository().saveEntity(house);

        return JsonResponse.OK();
    }

}
