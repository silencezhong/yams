package yams.resource.impl;

import javafx.util.Callback;
import org.springframework.web.bind.annotation.*;
import yams.callback.IdCallback;
import yams.domain.EmptyEntity;
import yams.domain.ModelEntity;
import yams.domain.ModelItemEntity;
import yams.resource.IModelResource;
import yams.resource.JsonCommonResource;
import yams.response.JsonResponse;
import yams.service.ICommonService;
import yams.service.impl.DefaultCommonService;

import javax.annotation.Resource;

/**
 * 模型对外接口
 * Created by Silence on 2016/11/1.
 */
@SuppressWarnings("unchecked")
@RestController
@RequestMapping("/model")
public class DefaultModelResource extends JsonCommonResource implements IModelResource {

    private ICommonService modelService;

    private ICommonService modelItemService;

    @Resource
    private IdCallback idCallback;

    @Override
    public JsonResponse<ModelEntity> page(@RequestBody ModelEntity modelEntity) {
        return JsonResponse.ERROR("Not Support Page");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @Override
    public JsonResponse<ModelEntity> findById(@PathVariable String id) {
        return _findOne(modelService, id, 0);
    }

    @RequestMapping(value = "/code/{code}", method = RequestMethod.GET)
    @Override
    public JsonResponse<ModelEntity> findByCode(@PathVariable String code) {
        return _findOne(modelService, code, 1);
    }

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    @Override
    public JsonResponse<ModelEntity> save(@RequestBody ModelEntity model) {
        return _saveInfo(modelService, model);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @Override
    public JsonResponse<ModelEntity> delete(@PathVariable String id) {
        return _delete(modelService, id);
    }

    @RequestMapping("/tree")
    @Override
    public JsonResponse<ModelEntity> modelTree(@RequestParam(name = "parentId") String parentId) {
        return _pageAll(modelService, new EmptyEntity());
    }

    @RequestMapping(value = "/item", method = RequestMethod.PUT)
    @Override
    public JsonResponse<ModelItemEntity> saveItem(@RequestBody ModelItemEntity item) {
        return _saveInfo(modelItemService,item,(Callback<ModelItemEntity, Object>) param -> {
            idCallback.call(param);
            if (param.getCode() == null)
                param.setCode(param.getId());
            return null;
        });
    }

    @RequestMapping(value = "/item/", method = RequestMethod.POST)
    @Override
    public JsonResponse<ModelItemEntity> findItems(@RequestBody ModelItemEntity item) {
        return _pageAll(modelItemService,item);
    }

    @RequestMapping(value = "/item/{itemId}", method = RequestMethod.DELETE)
    @Override
    public JsonResponse<ModelItemEntity> deleteItem(@PathVariable String itemId) {
        return _delete(modelItemService,itemId);
    }

    @RequestMapping(value = "/item/{itemId}", method = RequestMethod.GET)
    @Override
    public JsonResponse<ModelItemEntity> findItem(@PathVariable String itemId) {
        return _findOne(modelItemService,itemId,0);
    }

    @Override
    public void initService() {
        if(modelService==null)
            modelService = new DefaultCommonService("modelRepository");
        if(modelItemService ==null)
            modelItemService = new DefaultCommonService("modelItemRepository");
    }
}
