/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved. 
 */

package yams.resource;

import javafx.util.Callback;
import yams.common.IServiceLayer;
import yams.common.PersistObject;
import yams.common.exception.BreezeeException;
import yams.common.exception.EntityNotFoundException;
import yams.response.JsonResponse;
import yams.utils.ContextUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Jackjson形式的数据序列化方式，实现的保存与查询
 *
 * 为了兼容更多的service，我们采用把service当作参数传入的方式
 *
 * Created by Silence on 2016/6/9.
 */
@SuppressWarnings("unchecked")
public class JsonCommonResource {

    /**
     * 只是供子类调用使用
     * 内部使用, 保存方法，只是为了减少重复代码罢了
     *
     * @param service  管理服务
     * @param info     界面传入对象
     * @param callback 回调方法
     * @return 返回界面对象
     */
    protected <T extends PersistObject> JsonResponse _saveInfo(IServiceLayer service, T info, Callback... callback) {
        try {
            Callback cb = ContextUtil.getBean("idCallback", Callback.class);
            if (cb != null) {
                List<Callback> l = new ArrayList<>(Arrays.asList(callback));
                Callback[] cbs = new Callback[l.size()];
                l.add(ContextUtil.getBean("idCallback", Callback.class));
                service.saveInfo(info, l.toArray(cbs));
            } else {
                service.saveInfo(info, callback);
            }
        } catch (BreezeeException e) {
            e.printStackTrace();
            return JsonResponse.ERROR(e.getMessage());
        }
        return JsonResponse.buildSingle(info);
    }

    /**
     * 查找唯一对象.
     * 内部使用,不建议供外部调用
     *
     * @param service 服务
     * @param id      唯一标识，可以是Id，也可以是code
     * @param type    id:0, code:1
     * @return 界面对象
     */
    protected JsonResponse _findOne(IServiceLayer service, String id, int type, Callback... callback) {
        PersistObject info = null;
        try {
            if (type == 0)
                info = service.findById(id);
            else if (type == 1)
                info = service.findByCode(id);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
            return JsonResponse.ERROR(e.getMessage());
        }
        if(callback!=null && callback.length>0){
            for (Callback<PersistObject, ?> rCallback : callback) {
                rCallback.call(info);
            }
        }
        return JsonResponse.buildSingle(info);
    }

    /**
     * 内部列表查询方法
     *
     * @param service 管理服务
     * @param info    如果pagesize为-1，则不分页
     * @return 界面对象
     */
    protected JsonResponse _pageAll(IServiceLayer service, PersistObject info) {

        long t = System.currentTimeMillis();
        try {
            if (info.getProperties().get("pageSize") == null ||
                    (info.getProperties().get("pageSize").toString()).equals("-1")) {
                return JsonResponse.build(service.listAll(info), t);
            } else {
                return JsonResponse.build(service.pageAll(info), t);
            }
        } catch (BreezeeException e) {
            e.printStackTrace();
            return JsonResponse.ERROR(e.getMessage());
        }
    }

    protected JsonResponse _delete(IServiceLayer service, String id) {
        try {
            service.deleteById(id);
        } catch (BreezeeException e) {
            return JsonResponse.ERROR(e.getMessage());
        }
        return JsonResponse.OK();
    }
}
