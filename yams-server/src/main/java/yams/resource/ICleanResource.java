package yams.resource;

import yams.domain.CleanEntity;
import yams.response.Response;

/**
 * Created by LX on 2017/7/24.
 */
public interface ICleanResource  extends IResourceLayer<CleanEntity>{
    Response<CleanEntity> handleCleanCall(CleanEntity cleanEntity);
    Response<CleanEntity> page_name(CleanEntity cleanEntity);
}
