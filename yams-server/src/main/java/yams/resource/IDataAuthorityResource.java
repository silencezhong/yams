package yams.resource;

import yams.domain.AccountEntity;
import yams.domain.ApartmentEntity;
import yams.response.Response;

import java.util.Set;

/**
 * 数据权限接口服务
 * Created by Silence on 2016/11/1.
 */
public interface IDataAuthorityResource extends IResourceLayer<AccountEntity> {

   /* Response<ApartmentEntity> findItems(String code);*/
    /*Response<AccountEntity> saveAcnandapt(AccountEntity accountEntity,ApartmentEntity apartmentEntity);*/
    Response<ApartmentEntity> getApmsById(AccountEntity accountEntity);
}
