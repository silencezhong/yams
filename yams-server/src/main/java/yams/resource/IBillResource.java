package yams.resource;

import yams.common.InfoPage;
import yams.domain.BillEntity;
import yams.domain.EmptyEntity;
import yams.response.Response;

/**
 * 账单资源接口
 * Created by Silence on 2017/5/2.
 */
public interface IBillResource extends IResourceLayer<BillEntity> {

    Response<BillEntity> payBill(BillEntity bill);
    Response<BillEntity> addBill(BillEntity bill);
    Response<EmptyEntity> other(String id);
    InfoPage<BillEntity> findBillsCountByUserId(String id);
}
