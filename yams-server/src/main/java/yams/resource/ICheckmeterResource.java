package yams.resource;

import org.springframework.web.bind.annotation.RequestBody;
import yams.domain.CheckmeterEntity;
import yams.response.JsonResponse;
import yams.response.Response;

/**
 * 抄表管理
 * Created by Luffy on 2017/06/13.
 */
public interface ICheckmeterResource extends IResourceLayer<CheckmeterEntity> {

    //查找抄表数据，稍微复杂，所以不用默认查询，需要重写
    Response<CheckmeterEntity> query(CheckmeterEntity checkmeterEntity);

    //生成水电费账单
    Response<CheckmeterEntity> generateBill(CheckmeterEntity checkmeterEntity);

}
