package yams.resource;

import yams.domain.AccountEntity;
import yams.response.Response;

import java.util.Map;

/**
 * 账号对外接口
 * Created by Silence on 2016/10/31.
 */
public interface IAccountResource extends IResourceLayer<AccountEntity> {

    Response<AccountEntity> resetPassword(Map<String,Object> map);

    Response<AccountEntity> checkLogin(AccountEntity account);
}
