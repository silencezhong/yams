package yams.resource;

import yams.domain.CouponEntity;
import yams.response.Response;

/**
 * 券服务接口
 * Created by Luffy on 2017/06/22.
 */
public interface ICouponResource extends IResourceLayer<CouponEntity> {

    /**
     * 查找某用户的优惠券
     * @param id
     * @return
     */
    Response<CouponEntity> queryCouponByUser(String id);

}
