package yams.resource;

import yams.domain.*;
import yams.response.Response;

/**
 * Created by LX on 2017/7/21.
 */
public interface IMobileHomeResource {

    Response<MobileHomePageInfo> mobileHomePage( EmptyEntity emptyEntity);
    Response<HouseEntity> house( HouseEntity emptyEntity);

}
