/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */

package yams.utils;

import java.text.SimpleDateFormat;

/**
 * 项目中需要使用的一些工具类
 * Created by Silence on 2016/6/2.
 */
public final class BreezeeUtils {

    public final static SimpleDateFormat DATE_FORMAT_LONG = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public final static SimpleDateFormat DATE_FORMAT_SHORT = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * 加密字符串
     *
     * @param s 需要加密字符串
     * @return 加密后的结果
     */
    public static String enCrypt(String s) {
        return s;
    }

    /**
     * 解密字符串
     *
     * @param s 需要解密的字符串
     * @return 解密后的结果
     */
    public static String deCrypt(String s) {
        return s;
    }

}
