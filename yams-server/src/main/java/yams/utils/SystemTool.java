/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */

package yams.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

/**
 * 与系统相关的一些常用工具方法
 * Created by Silence on 2016/6/1.
 */
public class SystemTool {

    private static InetAddress ia;

    static {
        try {
            ia = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取当前操作系统名称
     * return 操作系统名称
     */
    public static String getOSName() {
        return System.getProperty("os.name").toLowerCase();
    }


    /**
     * 获取本地主机名
     *
     * @return 本机主机名
     */
    public static String getHostName() {
        return ia == null ? "unknownHost" : ia.getHostName();
    }

    /**
     * 本机IP地址
     *
     * @return 本机IP地址
     */
    public static String getIPAddress() {
        return ia == null ? "unknownIp" : ia.getHostAddress();
    }

    public static String uuid() {
        return UUID.randomUUID().toString().replaceAll("-","");
    }

    public static String timeLine(int offset) {
        return LocalDateTime.now().toString().replaceAll("-", "").replaceAll(":", "").replaceAll("T", "").replaceAll("\\.", "").substring(offset);
    }

    public static int stringValue(String s) {
        int sum = 0;
        for (char c : s.toCharArray()) {
            sum += (int) c;
        }
        return sum;
    }

    /**
     * 生成随机整数
     * @param n 位数
     * @param ran 随机实例
     * @return 获取的结果
     * @throws IllegalArgumentException
     */
    public static String random(int n, Random ran) throws IllegalArgumentException {
        if (n < 1 || n > 10) {
            throw new IllegalArgumentException("cannot random " + n + " bit number");
        }
        if (ran == null)
            ran = new Random();
        if (n == 1) {
            return String.valueOf(ran.nextInt(10));
        }
        int bitField = 0;
        char[] chs = new char[n];
        for (int i = 0; i < n; i++) {
            while (true) {
                int k = ran.nextInt(10);
                if ((bitField & (1 << k)) == 0) {
                    bitField |= 1 << k;
                    chs[i] = (char) (k + '0');
                    break;
                }
            }
        }
        return new String(chs);
    }

    /**
     * Date类型 转换成 String类型 目前仅支持yyyy-MM-dd HH:mm:ss格式
     * @param date
     * @param format
     * @return
     */
    public static String dateToString(Date date , String format){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format);
        LocalDateTime newDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        String newDateStr = newDate.format(dateTimeFormatter);
        return newDateStr;
    }

    /**
     *  Date类型 转换成 LocalDate类型
     * @param date
     * @return
     */
    public static LocalDate dateToLocalDate(Date date){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        return localDate;
    }

    /**
     * 计算两个日期之间相差几天
     * @param begin
     * @param end
     * @return
     */
    public static long between(Date begin, Date end){
        LocalDate beginDate = dateToLocalDate(begin);
        LocalDate endDate = dateToLocalDate(end);
        return endDate.toEpochDay() - beginDate.toEpochDay();
    }
}
