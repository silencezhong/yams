package yams.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.Set;

/**
 * 房型组实体
 * Created by Luffy on 2017/8/7.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"parent"})
public class RoomGroupEntity extends AbstractEntity {

    RoomGroupEntity parent;

    Set<RoomGroupEntity> children;

    String type;

    @Override
    public String toString() {
        return "RoomGroupEntity{" +
                "id='" + id + '\'' +
                ", code='" + code + '\'' +
                ", name=" + name +
                '}';
    }
}
