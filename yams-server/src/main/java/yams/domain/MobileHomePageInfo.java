package yams.domain;

import lombok.*;
import java.io.Serializable;

/**
 * 移动端首页信息展现
 * Created by Luffy on 2017/07/26.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class MobileHomePageInfo implements Serializable {

    /**
     *  房屋总数，空房数，已出租，即将退房，超期账单，抄表，报修，保洁，预约看房，账单数，用户数
     */
    Long totalHouseCnt,emptyHouseCnt,rentedHouseCnt,checkoutHouseCnt,
            overdueBillCnt, checkMeterCnt,repairCnt,cleanCnt,
            makeAppointmentCnt,billCnt,userCnt;


}
