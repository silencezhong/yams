package yams.domain;

import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ContractReportInfo implements Serializable {
    /**城市**/
    String cityName;
    /**公寓名称**/
    String apartmentName;
    /**管理员名称**/
    String manageName;
    /**合同编号**/
    String contractNo;
    /**房号**/
    String houseNo;
    /**房屋类型**/
    String houseType;
    /**客户名称**/
    String customerName;
    /**客户电话**/
    String customerPhone;
    /**客户身份证**/
    String idCard;
    /**支付方式**/
    Integer payMethod;
    String payMethodStr;
    /**每月租金**/
    Double  monthlyRent;
    String monthlyRentStr;
    /**押金**/
    BigDecimal pledge;
    String pledgeStr;
    /**租金到期日**/
    String rentalEndDate;
    /**租房开始日期**/
    Date rentStartDate;
    String rentStartDateStr;
    /**租房结束日期**/
    Date  rentEndDate;
    String rentEndDateStr;
    /**账单金额**/
    BigDecimal  billAmount;
    String billAmountStr;
    /**是否快到期**/
    Integer isDelay;
    String isDelayStr;
    /**超期天数**/
    Integer delayDay;
    String delayDayStr;


}
