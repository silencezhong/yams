package yams.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;

/**
 * 客户余额实体
 * Created by Silence on 2016/11/6.
 */
@Entity(name = "T_USER_BALANCE")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonIgnoreProperties({"user"})
public class UserBalanceEntity extends AbstractEntity {

    Double balance=0d;

    @OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID", referencedColumnName = "PK_ID")
    UserEntity user;
}
