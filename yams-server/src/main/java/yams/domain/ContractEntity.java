package yams.domain;

import lombok.*;
import yams.common.constants.ContractStatusEnum;
import yams.utils.BeanUtil;
import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

/**
 * 会员合同实体
 * Created by Silence on 2016/10/16.
 */
@Entity(name = "T_CONTRACT")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ContractEntity extends AbstractEntity {

    /**
     * 合同开始，结束，签署时间
     */
    Date startDate, endDate, signDate;

    /**
     * 房租，押金
     */
    Double rental, pledge, water, electric, gas;

    /**
     * 合同付款周期, 是否支付过押金1代表支付过押金 其它代表没付过
     */
    Integer payCycle,payStatus;

    Boolean generateBill;

    /**
     * 下次付款日期
     */
    String nextPayDate;

    /**
     *  富文本插件，原来用于存储合同，现在用了合同模板，暂时不需要了
     */
    @Lob
    @Column(length = 20971520)
    String uedit;

    /**
     *  电视费、其他费用、其他约定
     */
    Double tvFee,otherFee;

    String otherPlan;


    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    @JoinColumn(name = "HOUSE_ID", referencedColumnName = "PK_ID")
    HouseEntity house;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID", referencedColumnName = "PK_ID")
    UserEntity user;

    public String getUserId() {
        String userId = "";
        if(user != null){
            userId = user.getId();
        }
        return userId;
    }
    public String getUserName() {
        String userName = "";
        if(user != null){
            userName = user.getName();
        }
        return userName;
    }
    public UserEntity getUser() {
        UserEntity userEntity = new UserEntity();
        if(user != null){
            BeanUtil.beanCopy(user, userEntity, "contracts", "userReserveds","coupons", "balance");
        }
        return userEntity;
    }

    public String getHouseId() {
        String houseId = "";
        if(house != null){
            houseId = house.getId();
        }
        return houseId;
    }
    public String getHouseName() {
        String houseName = "";
        if(house != null){
            houseName = house.getName();
        }
        return houseName;
    }

    public HouseEntity getHouse() {

        HouseEntity result = null;
        if(this.house != null){
            HouseEntity houseEntity = new HouseEntity();
            BeanUtil.beanCopy(this.house, houseEntity, "contracts", "apartment", "userReserveds");
            result = houseEntity;
        }
        return result;

    }

    public String getStatusName() {
        for (ContractStatusEnum anEnum : ContractStatusEnum.values()) {
            if (Objects.equals(anEnum.getValue(), this.status)) {
                return anEnum.getText();
            }
        }
        return status + "";
    }
}
