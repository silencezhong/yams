package yams.domain;

import lombok.*;
import yams.common.constants.AttributeTypeEnum;
import yams.common.constants.ConstantEnum;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Map;

/**
 * 属性实体
 * Created by Silence on 2016/11/3.
 */
@Entity
@Table(name = "T_ATTRIBUTE", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AttributeEntity extends AbstractEntity {
    /**
     * 字段类型，参数，单位
     */
    String fieldType, arguments, unitCode;

    @Transient
    Map<String, Object> argumentsJson;

    public String getAttrId() {
        return this.getId();
    }

    public String getAttrCode() {
        return this.getCode();
    }

    public String getAttrName() {
        return this.getName();
    }

    public String getTypeName() {
        for (AttributeTypeEnum anEnum : AttributeTypeEnum.values()) {
            if (anEnum.toString().equals(fieldType)) {
                return anEnum.getText();
            }
        }
        return fieldType;
    }
}
