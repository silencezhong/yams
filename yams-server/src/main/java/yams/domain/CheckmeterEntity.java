package yams.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;

/**
 * 水电气
 * Created by Luffy on 2017/06/13.
 */
@Entity(name = "T_Checkmeter")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonIgnoreProperties({"house"})
public class CheckmeterEntity extends AbstractEntity {

    /**
     * 抄表日期 年，月，日
     */
    String meterYear, meterMonth, meterDay;

    /**
     * 水，电，燃气
     */
    String water, electric, gas;
    /**
     * 是否已经生成账单 Y已生成 N未生成
     */
    String isGenBill;

    @Transient
    String houseName;
    @Transient
    String houseId;
    @Transient
    String lastDate;
    @Transient
    String lastMeterInfo;
    /**
     * 区分抄表房屋的标识
     */
    @Transient
    String houseIdentites;

    @ManyToOne(cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinColumn(name = "HOUSE_ID")
    HouseEntity house;

    /*@Override
    public String toString() {
        return "UtilityBillEntity{" +
                "id='" + id + '\'' +
                "date='" + meterYear +"-"+ meterMonth + "-" +meterDay+'\'' +
                "water='" + water + '\'' +
                "electric='" + electric + '\'' +
                "gas='" + gas + '\'' +
                '}';
    }*/
}
