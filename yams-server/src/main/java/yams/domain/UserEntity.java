package yams.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

/**
 * 会员信息实体
 * Created by Silence on 2016/10/16.
 */
@Entity(name = "T_USER")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonIgnoreProperties({"userReserveds", "contracts","coupons","balance"})
public class UserEntity extends AbstractEntity {

    //status 0禁用 4启用 3新用户注册
    //密码，性别，家庭住址，dn,身份证号,微信openid,电邮，备注，系统旧版本存身份证照片路径的字段(暂不使用)，微信昵称
    String password, sex, address, dn, idCard, weChat, email, memo1, idCardPhoto, nickname;
    //实名认证 0未验证1审核中2通过验证
    String certification;
    //用户类型 1管理员2普通用户
    String userType;
    //身份证正面照片，身份证反面照片，头像照片
    String icFrontImg , icBackImg, avatarImg;
    //推荐人手机号
    String introducer;
    //绑定公众号日期
    Date followDate;
    //手机号
    @Column(name = "PHONE", unique = true, length = 20)
    String phone;
    //关联的合同
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @OrderBy(value = "modifiedDate DESC")
    Set<ContractEntity> contracts;
    //关联的短租订单
    @OneToMany(mappedBy = "user", cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    @OrderBy(value = "modifiedDate DESC")
    Set<UserReserved> userReserveds;
    /**
     * 一个会员手上可能拥有多张优惠券
     */
    @OneToMany(mappedBy = "user", cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    @OrderBy(value = "modifiedDate DESC")
    Set<CouponEntity> coupons;

    @OneToOne(mappedBy = "user", cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    UserBalanceEntity balance;

    @Transient
    BigDecimal toPayAmount;
}
