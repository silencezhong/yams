package yams.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.util.Map;
import java.util.Set;

/**
 * 品类属性
 * Created by Silence on 2016/11/3.
 */
@Entity
@Table(name = "T_CATE_ATTR", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonIgnoreProperties({"attribute"})
public class CateAttrEntity extends AbstractEntity {

    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "CATE_ID", referencedColumnName = "PK_ID")
    CategoryEntity category;

    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "ATTR_ID", referencedColumnName = "PK_ID")
    AttributeEntity attribute;

    Boolean inheritable=true, nullable=true, display=true;

    String defaultValue;

    @Transient
    private Map<String,Object> arguments;

    public String getAttrId() {
        if (attribute != null)
            return attribute.getId();
        return "";
    }

    public String getAttrCode() {
        if (attribute != null)
            return attribute.getCode();
        return "";
    }

    public String getAttrName(){
        if (attribute != null)
            return attribute.getName();
        return "";
    }

    public String getAttrType(){
        if (attribute != null)
            return attribute.getFieldType();
        return "";
    }
}
