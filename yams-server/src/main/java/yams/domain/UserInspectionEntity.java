package yams.domain;

import lombok.*;
import org.hibernate.annotations.SQLDelete;
import yams.common.constants.InspectionOrderStatusEnum;
import yams.common.constants.OrderStatusEnum;
import yams.utils.BeanUtil;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

/**
 * 预约看房订单
 * Created by Luffy on 2017/07/20.
 */
@Entity(name = "T_USER_INSPECTION")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserInspectionEntity extends AbstractEntity {

    /**
     * 看房时间
     */
    Date checkRoomDate;
    /**
     * 入住时间,退房时间
     */
    Date arrivedDate, leavedDate;
    /**
     * 冗余字段 看房人姓名,看房人手机号码,房屋名称,备注
     */
    String contactName, contactMobile , houseName,remark;
    /**
     *
     * 订单状态
     */
    String orderStatus;
    /**
     *  看房人员
     */
    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID", referencedColumnName = "PK_ID")
    UserEntity user;
    /**
     *  预看房间
     */
    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    @JoinColumn(name = "HOUSE_ID", referencedColumnName = "PK_ID")
    HouseEntity house;

    @Transient
    String houseId;

    @Transient
    String userId;

    public String getHouseName() {
        if (house != null)
            return house.getName();
        return null;
    }
    public String getHouseCode() {
        if (house != null)
            return house.getCode();
        return null;
    }

    public HouseEntity getHouse() {
        HouseEntity houseEntity = new HouseEntity();
        BeanUtil.beanCopy(this.house, houseEntity, "contracts", "apartment", "products", "userReserveds");
        return houseEntity;
    }
    public UserEntity getUser() {
        UserEntity ue = new UserEntity();
        ue.setId(this.user.getId());
        ue.setName(this.user.getName());
        ue.setCode(this.user.getCode());
        return ue;
    }

    public String getUserId() {
        if (user != null)
            return user.getId();
        return null;
    }

    public String getStatusName() {
        for (InspectionOrderStatusEnum anEnum : InspectionOrderStatusEnum.values()) {
            if (Objects.equals(anEnum.getValue(), this.orderStatus)) {
                return anEnum.getText();
            }
        }
        return orderStatus;
    }
}
