package yams.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;

/**
 * 产品的明细
 * Created by Silence on 2016/11/10.
 */
@Entity
@Table(name = "T_SKU", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonIgnoreProperties({"product","house"})
public class SkuEntity extends AbstractEntity {

    @ManyToOne(cascade = CascadeType.REFRESH, fetch= FetchType.LAZY)
    @JoinColumn(name="PRODUCT_ID", referencedColumnName = "PK_ID", nullable=false)
    ProductEntity product;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    @JoinColumn(name = "HOUSE_ID", referencedColumnName = "PK_ID")
    HouseEntity house;

    Boolean myself;

    @Transient
    String houseId;
}
