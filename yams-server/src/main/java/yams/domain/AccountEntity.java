/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */

package yams.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

/**
 * 持久域：账号实体
 * Created by Silence on 2016/5/5.
 */
@Entity
@Table(name = "T_ACCOUNT", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonIgnoreProperties({"apartmens"})
public class AccountEntity extends AbstractEntity {

    @ManyToOne(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    @JoinTable(name = "R_ORG_ACN", joinColumns = @JoinColumn(name = "ACN_ID", referencedColumnName = "PK_ID"),
            inverseJoinColumns = @JoinColumn(name = "ORG_ID", referencedColumnName = "PK_ID"))
    OrganizationEntity organization;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "R_ROLE_ACN", joinColumns = @JoinColumn(name = "ACN_ID", referencedColumnName = "PK_ID"),
            inverseJoinColumns = @JoinColumn(name = "ROLE_ID", referencedColumnName = "PK_ID"))
    @OrderBy(value = "permits asc ")
    Set<RoleEntity> roles;

    /*公寓和账户多对多关系*/
    @ManyToMany(cascade = {CascadeType.ALL}, fetch =FetchType.LAZY)
    @JoinTable(name = "R_APM_ACN",joinColumns = @JoinColumn(name = "ACN_ID",referencedColumnName ="PK_ID" ),
            inverseJoinColumns = @JoinColumn(name = "APM_ID",referencedColumnName = "PK_ID"))
    Set<ApartmentEntity> apartmens;

    String password,mobile,email,job;

    Integer type,sex;



}
