/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */

package yams.domain;

import javax.persistence.*;
import java.util.Set;

/**
 * 持久域：组织实体。
 * 省，城市，营销部 构成一个树形结构
 * Created by Silence on 2016/5/10.
 */
@Entity
@Table(name = "T_ORGANIZATION", schema = AbstractEntity.DB_SCHEMA)
public class OrganizationEntity extends AbstractEntity {

    Integer category;

    @OneToOne
    @JoinColumn(name = "PARENT_ID", referencedColumnName = "PK_ID")
    OrganizationEntity parent;

    @OneToMany(mappedBy = "parent", cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    Set<OrganizationEntity> children;

    @OneToMany(mappedBy = "organization", cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @OrderBy(value = "code ASC")
    Set<AccountEntity> accounts;

}
