package yams.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;

/**
 * 优惠券模型实体
 * Created by Luffy on 2017/06/22.
 */
@Entity
@Table(name = "T_COUPON_MODEL", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CouponModelEntity extends AbstractEntity {

    /**
     *  券类型（现金券，折扣券） 券使用范围 券使用规则 券状态
     */
    String couponType,couponRange,couponRule,couponStatus;

    /**
     * 公用字段  优惠金额，优惠折扣
     */
    BigDecimal couponVal;

    /**
     * 券使用期间(有效开始时间，有效截至时间)
     */
    Date startDate,endDate;

    @OneToMany(mappedBy = "couponModel", cascade = {CascadeType.ALL}, fetch = FetchType.EAGER, orphanRemoval = true)
    @OrderBy(value = "modifiedDate DESC")
    Set<CouponEntity> couopons;

}
