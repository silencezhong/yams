package yams.domain;

import yams.common.PersistObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Empty Entity
 * Created by Silence on 2016/11/1.
 */
public class EmptyEntity extends PersistObject {

    private Map<String,Object> properties = new HashMap<>();

    public EmptyEntity() {
    }
    public EmptyEntity(Map<String, Object> properties) {
        this.properties = properties;
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public String getCode() {
        return null;
    }

    @Override
    public void setModifiedDate(Date date) {
        //Nothing
    }

    @Override
    public Map<String, Object> getProperties() {
        return properties;
    }

    @Override
    public Integer getRowNum() {
        return null;
    }

    @Override
    public void setRowNum(Integer rowNum) {

    }
}
