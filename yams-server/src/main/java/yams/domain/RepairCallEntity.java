package yams.domain;

import lombok.*;
import yams.common.constants.RequestStatusEnum;
import javax.persistence.Entity;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

/**
 * 故障报修实体
 * Created by Silence on 2016/11/1.
 */
@Entity(name = "T_REPAIR")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class RepairCallEntity extends AbstractEntity {

    /**
     * 提出时间，处理时间
     */
    Date issueTime, handleTime;
    /**
     * 提出人ID，姓名，联系方式，处理结果，报修类型，地址，处理人ID，姓名,联系方式，
     */
    String issueUser,issueUserName,issueUserPhone,result, type,address, handleUser,handleUserName,handleUserPhone, userImg;
    /**
     * 存储最多5张图片
     */
    String aImage, bImage, cImage, dImage, eImage, mediaIds;
    /**
     * 费用金额
     */
    BigDecimal repairFee;

    public String[] getMedias() {
        String[] ss = new String[]{"", "", "", "", ""};
        if (mediaIds != null) {
            String[] tmp = mediaIds.split(",");
            System.arraycopy(tmp, 0, ss, 0, tmp.length);
        }
        return ss;
    }

    public String getStatusName() {
        for (RequestStatusEnum anEnum : RequestStatusEnum.values()) {
            if (Objects.equals(anEnum.getValue(), this.getStatus()))
                return anEnum.getText();
        }
        return this.getStatus() + "";
    }
}
