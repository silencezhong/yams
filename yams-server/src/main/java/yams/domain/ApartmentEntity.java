package yams.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

/**
 * 公寓实体
 * Created by Silence on 2016/11/1.
 */
@Entity
@Table(name = "T_APARTMENT", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"parent", "children", "houses","accounts"})
public class ApartmentEntity extends AbstractEntity {

    /**
     * 地址，值班电话
     */
    String address, phone, city;

    /**
     * 楼层总数
     */
    Long totalFloor;

    /**
     * 经度，纬度
     */
    Double longitude = 0d, latitude = 0d;

    /**
     * 水电费
     */
    BigDecimal waterUnitPrice,electricUnitPrice;

    /**
     * 竣工日期，使用日期
     */
    Date buildDate, startDate;

    /**
     * 是否只是目录
     */
    Boolean directory = true;

    /**
     * 水电费体系  商用 民用,经纬度
     */
    String utilityBillType,lngAndLat;

    @OneToMany(mappedBy = "apartment", cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @OrderBy("rowNum")
    Set<HouseEntity> houses;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "PARENT_ID")
    ApartmentEntity parent;

    @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @OrderBy(value = "rowNum DESC")
    Set<ApartmentEntity> children;

    /*公寓和账户多对多关系*/
    @ManyToMany(mappedBy = "apartmens", fetch = FetchType.LAZY)
    Set<AccountEntity> accounts;

    public String getType() {
        String type;
        if (this.getChildren() != null && this.getChildren().size() > 0)
            type = "folder";
        else
            type = "leaf";
        return type;
    }

    @Override
    public String toString() {
        return "ApartmentEntity{" +
                "id='" + id + '\'' +
                ", code='" + code + '\'' +
                ", name=" + name +
                '}';
    }
}
