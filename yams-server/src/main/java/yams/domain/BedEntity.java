package yams.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import yams.common.PersistObject;
import yams.common.constants.HouseStatusEnum;
import yams.repository.IEnumRepository;
import yams.utils.ContextUtil;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * 床铺信息
 * Created by Luffy on 2017/06/06.
 */
@Entity(name = "T_BED")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonIgnoreProperties({"house"})
public class BedEntity extends PersistObject {

    public final static String DB_SCHEMA = "";

    @Id
    @GeneratedValue(generator = "assigned-uid")
    @GenericGenerator(name = "assigned-uid", strategy = "assigned")
    @Column(name = "PK_ID", unique = true, nullable = false, updatable = false, length = 64)
    String id;

    /**
     * 床铺类型  单人床 双人床
     */
    String bedType;
    /**
     * 床长 宽 单位米
     */
    String bedLength,bedWidth;

    @ManyToOne(cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinColumn(name = "HOUSE_ID")
    HouseEntity house;

    String code;


    @LastModifiedDate
    Date modifiedDate;


    @Column(name = "ROW_NUM", nullable = false)
    Integer rowNum = 1;

    @Transient
    Map<String, Object> properties;

    public Map<String, Object> getProperties() {
        if (properties == null)
            this.properties = new HashMap<>();
        return properties;
    }

    public void setProperties(Map<String, Object> properties) {
        this.properties = properties;
    }

    @Override
    public String toString() {
        return "BedEntity{" +
                "id='" + id + '\'' +
                "bedType='" + bedType + '\'' +
                "bedLength='" + bedLength + '\'' +
                "bedWidth='" + bedWidth + '\'' +
                '}';
    }
}
