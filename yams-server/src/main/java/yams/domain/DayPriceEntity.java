package yams.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.LastModifiedDate;
import yams.common.PersistObject;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 日租价格
 * Created by Luffy on 2017/06/07.
 */
@Entity(name = "T_DAY_PRICE")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonIgnoreProperties({"house"})
public class DayPriceEntity extends PersistObject {

    public final static String DB_SCHEMA = "";

    @Id
    @GeneratedValue(generator = "assigned-uid")
    @GenericGenerator(name = "assigned-uid", strategy = "assigned")
    @Column(name = "PK_ID", unique = true, nullable = false, updatable = false, length = 64)
    String id;

    String code;

    /**
     * 短租类型 3小时 5小时 8小时 1天
     */
    String shortRentType;
    /**
     * 价格 单位元
     */
    BigDecimal dayPrice;

    @ManyToOne(cascade = {CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinColumn(name = "HOUSE_ID")
    HouseEntity house;

    @LastModifiedDate
    Date modifiedDate;

    @Column(name = "ROW_NUM", nullable = false)
    Integer rowNum = 1;

    @Transient
    Map<String, Object> properties;

    public Map<String, Object> getProperties() {
        if (properties == null)
            this.properties = new HashMap<>();
        return properties;
    }

    public void setProperties(Map<String, Object> properties) {
        this.properties = properties;
    }

    @Override
    public String toString() {
        return "DayPriceEntity{" +
                "id='" + id + '\'' +
                "shortRentType='" + shortRentType + '\'' +
                "dayPrice='" + dayPrice + '\'' +
                '}';
    }
}
