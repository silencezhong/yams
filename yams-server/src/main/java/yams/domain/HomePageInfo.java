package yams.domain;

import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 首页信息展现
 * Created by Silence on 2017/5/4.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class HomePageInfo implements Serializable {

    Long repairNum,houseNum,emptyHouse,delayBillNum,userNum,deadlineHouse;

    BigDecimal todayIncome;


}
