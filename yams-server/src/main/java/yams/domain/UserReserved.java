package yams.domain;

import lombok.*;
import org.hibernate.annotations.SQLDelete;
import yams.common.constants.OrderStatusEnum;
import yams.common.constants.ReservedStatusEnum;
import yams.utils.BeanUtil;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

/**
 * 房屋预定信息 针对短租
 * Created by Silence on 2016/11/5.
 */
@Entity(name = "T_HOUSE_RESERVED")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@SQLDelete(sql = "UPDATE T_HOUSE_RESERVED SET STATUS = 0 WHERE PK_ID = ?")
public class UserReserved extends AbstractEntity {


    /**
     * 入住时间,退房时间
     */
    Date arrivedDate, leavedDate;
//  Date checkRoomDate;

    /**
     * 冗余字段 入住人姓名,手机号码,房屋名称
     */
    String contactName, contactMobile , houseName;
    /**
     * 短租类型,支付方式(微信，支付宝，刷卡，现金)，
     * 订单状态(未确认，已确认，自主取消，后取消)，订单类型(钟点房,日租)，入住天数，
     */
    String shortRentType,payMethod,orderStatus, orderType , days;
    /**
     * 租金,佣金
     */
    BigDecimal rentPrice,commission;

    /**
     * 是否使用优惠券
     */
    Boolean useCoupon;

    /**
     * 优惠金额
     */
    BigDecimal couponAmount;

    @OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    @JoinColumn(name = "COUPON_ID", referencedColumnName = "PK_ID")
    CouponEntity coupon;

    /**
     *  入住人员
     */
    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID", referencedColumnName = "PK_ID")
    UserEntity user;

    /**
     * 入住房间
     */
    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    @JoinColumn(name = "HOUSE_ID", referencedColumnName = "PK_ID")
    HouseEntity house;


    @Transient
    String houseId;

    @Transient
    String userId;

    public String getHouseName() {
        if (house != null)
            return house.getName();
        return null;
    }

    public String getHouseCode() {
        if (house != null)
            return house.getCode();
        return null;
    }

    public HouseEntity getHouse() {
        HouseEntity houseEntity = new HouseEntity();
        BeanUtil.beanCopy(this.house, houseEntity, "contracts", "apartment", "products", "userReserveds");
        return houseEntity;
    }

    public UserEntity getUser() {
        UserEntity userEntity = new UserEntity();
        BeanUtil.beanCopy(this.user, userEntity, "userReserveds", "contracts","coupons","balance");
        return userEntity;
    }

    public CouponEntity getCoupon() {
        CouponEntity couponEntity = new CouponEntity();
        if(this.coupon != null){
            BeanUtil.beanCopy(this.coupon, couponEntity, "couponModel", "userReserved");
        }
        return couponEntity;
    }

    public String getUserId() {
        if (user != null)
            return user.getId();
        return null;
    }

    public String getStatusName() {
        for (OrderStatusEnum anEnum : OrderStatusEnum.values()) {
            if (Objects.equals(anEnum.getValue(), this.orderStatus)) {
                return anEnum.getText();
            }
        }
        return orderStatus;
    }
}
