package yams.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.SQLDelete;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 账单
 * 支付状态 0是待支付 1是已支付 2是作废
 * Created by Silence on 2017/5/2.
 */
@Entity(name = "T_BILL")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonIgnoreProperties({"user", "house", "contract"})
@SQLDelete(sql = "UPDATE T_BILL SET STATUS = 2 WHERE PK_ID = ?")
public class BillEntity extends AbstractEntity {

    /**
     * 账单类型,合同，水电，其他
     * 1=合同
     * 2=水电
     * 3=报修
     * 4=保洁
     * 5=其他
     */
    Integer type;

    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "cnr_id", referencedColumnName = "pk_id")
    ContractEntity contract;

    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "pk_id")
    UserEntity user;

    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "house_id", referencedColumnName = "pk_id")
    HouseEntity house;

    /**
     * 折扣
     */
    BigDecimal discount;

    /**
     * 租金，押金
     */
    BigDecimal rentalAmount, pledgeAmount;

    /**
     * 水电气的上次值，当前值
     */
    BigDecimal preWater, preElectric, preGas, waterNum, electricNum, gasNum;

    /**
     * 水电气的金额
     */
    BigDecimal waterAmount, electricAmount, gasAmount;

    /**
     * 水电气的单价
     */
    BigDecimal waterPrice, electricPrice, gasPrice;

    /**
     * 其他的费用,总支付费用
     */
    BigDecimal otherFee, totalAmount, actualAmount, payedAmount;

    Date payedDate;

    /**
     * 付款方式 wepay微信 cash现金
     */
    String payMethod, payedPerson;

    Boolean delay;

    public void water() {
        this.waterAmount = waterNum.subtract(preWater).multiply(waterPrice);
    }

    public void electric() {
        this.electricAmount = electricNum.subtract(preElectric).multiply(electricPrice);
    }

    public void gas() {
        this.gasAmount = gasNum.subtract(preGas).multiply(gasPrice);
    }
}
