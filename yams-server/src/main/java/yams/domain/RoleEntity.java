/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */

package yams.domain;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

/**
 * 持久域：角色实体
 * Created by Silence on 2016/5/10.
 */
@Entity
@Table(name = "T_ROLE", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class RoleEntity extends AbstractEntity {

    @Column(name = "PERMITS", length = 4000)
    String permits;

    @ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY)
    Set<AccountEntity> accounts;

}
