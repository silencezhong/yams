package yams.domain;

import lombok.*;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 合同报表明细 查询条件
 * Created by Luffy on 2018/05/14.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ContractReportRequestObj implements Serializable {
    /**城市**/
    String cityName;
    /**公寓名称**/
    String apartmentName;
    /**管理员名称**/
    String manageName;
    /**租房开始日期**/
    Date rentStartDate;
    /**租房结束日期**/
     Date rentEndDate;
    /**客户 包括姓名手机号身份证**/
    String customer;


    Map<String, Object> properties;

    public Map<String, Object> getProperties() {
        if (properties == null)
            this.properties = new HashMap<>();
        return properties;
    }

}
