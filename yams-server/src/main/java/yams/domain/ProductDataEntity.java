package yams.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;

/**
 * 商品属性值
 * Created by Silence on 2016/11/3.
 */
@Entity
@Table(name = "T_PRODUCT_DATA", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonIgnoreProperties({"product"})
public class ProductDataEntity extends AbstractEntity{

    Long dataId;

    @Lob
    @Column(length=20971520)
    String attrValue;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch=FetchType.LAZY)
    @JoinColumn(name="ATTR_ID", referencedColumnName = "PK_ID", nullable=false)
    AttributeEntity attribute;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch=FetchType.LAZY)
    @JoinColumn(name="PRODUCT_ID", referencedColumnName = "PK_ID", nullable=false)
    ProductEntity product;
}
