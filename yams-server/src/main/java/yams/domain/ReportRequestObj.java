package yams.domain;

import lombok.*;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 报表 月收入明细 查询条件
 * Created by Luffy on 2018/05/14.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ReportRequestObj implements Serializable {

   /**时间**/
   Date startDate;
   Date endDate;
   /**城市**/
   String cityName;
   /**公寓**/
   String apartmentName;
   /**管家**/
   String managerName;
   /**客户 包括姓名手机号身份证**/
   String customer;

   Map<String, Object> properties;

   public Map<String, Object> getProperties() {
      if (properties == null)
         this.properties = new HashMap<>();
      return properties;
   }



}
