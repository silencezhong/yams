package yams.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;

/**
 * 优惠券实体
 * Created by Luffy on 2017/06/23.
 */
@Entity
@Table(name = "T_COUPON", schema = AbstractEntity.DB_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"couponModel","userReserved"})
public class CouponEntity extends AbstractEntity {

    @ManyToOne(cascade = {CascadeType.REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = "COUPON_MODEL_ID")
    CouponModelEntity couponModel;

    /**
     *  会员，一张券只对应一个会员
     */
    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID", referencedColumnName = "PK_ID")
    UserEntity user;

    @OneToOne(mappedBy = "coupon", cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    UserReserved userReserved;

    /**
     * 状态 1 可用  2 被领用 0 过期 3已使用
     */

    public BigDecimal getCouponVal() {
        if (couponModel != null){
            return couponModel.getCouponVal();
        }else{
            return BigDecimal.ZERO;
        }
    }

    public String getCouponType() {
        if (couponModel != null){
            return couponModel.getCouponType();
        }else{
            return "";
        }
    }

    public String getUserInfo(){
        if (user != null){
            return user.getName()+"("+user.getCode()+","+user.getPhone()+")";
        }else{
            return "";
        }
    }



}
