package yams.domain;

import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 报表 月收入明细
 * Created by Luffy on 2018/05/14.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ReportMonthIncomeInfo implements Serializable {

   /**时间**/
   Date billDate;
   String billDateStr;
   /**账单号**/
   String billNo;
   /**账单名称**/
   String billName;
   /**账单金额**/
   BigDecimal billAmount;
   String billAmountStr;
   /**账单类型**/
   Integer billType;
   String billTypeStr;
   /**客户名称**/
   String customerName;
   /**客户联系方式**/
   String customerTel;
   /**收款人**/
   String receiverName;
   /**收款金额**/
   BigDecimal receiveAmount;
   String receiveAmountStr;
   /**收款方式**/
   String recieveWay;
   /**城市**/
   String city;
   String cityName;
   /**公寓**/
   String apartmentName;
   /**管家**/
   String managerName;
   /**备注**/
   String remark;


}
