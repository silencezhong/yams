package yams.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import yams.common.IRepository;
import yams.common.InfoPage;
import yams.common.PageInfo;
import yams.common.exception.BreezeeException;
import yams.domain.SkuEntity;
import yams.repository.ISkuRepository;
import yams.service.ISkuService;
import yams.utils.DynamicSpecifications;

/**
 * sku服务实现
 * Created by Silence on 2016/11/11.
 */
@Service("skuService")
public class DefaultSkuService implements ISkuService {

    private final ISkuRepository skuRepository;

    @Autowired
    public DefaultSkuService(ISkuRepository skuRepository) {
        this.skuRepository = skuRepository;
    }

    @Override
    public InfoPage<SkuEntity> findNullHouse(SkuEntity sku) {
        PageInfo pageInfo = new PageInfo(sku.getProperties());
        Page<SkuEntity> page = skuRepository.findNullHouse(pageInfo);
        return new InfoPage<>(page.getContent(), page.getTotalElements());
    }

    @Override
    public IRepository<SkuEntity> getRepository() {
        return skuRepository;
    }
}
