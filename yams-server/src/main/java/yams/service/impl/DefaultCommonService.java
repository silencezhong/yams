package yams.service.impl;

import yams.common.IRepository;
import yams.domain.AbstractEntity;
import yams.service.ICommonService;
import yams.utils.ContextUtil;

/**
 * 默认的方法
 * Created by Silence on 2016/11/3.
 */
public class DefaultCommonService implements ICommonService {

    private IRepository repository;

    public DefaultCommonService(String repositoryBean) {
        setRepository(repositoryBean);
    }

    @Override
    public IRepository<AbstractEntity> getRepository() {
        return repository;
    }

    @Override
    public void setRepository(String repositoryBean) {
        repository = ContextUtil.getBean(repositoryBean,IRepository.class);
    }
}
