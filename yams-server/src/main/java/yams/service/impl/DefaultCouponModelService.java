package yams.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import yams.common.IRepository;
import yams.domain.CouponModelEntity;
import yams.repository.ICouponModelRepository;
import yams.service.ICouponModelService;

/**
 * 券模型服务实现
 * Created by Luffy on 2017/06/22.
 */
@Service("couponModelService")
public class DefaultCouponModelService implements ICouponModelService {

    @Autowired
    private ICouponModelRepository couponModelRepository;

    @Override
    public IRepository<CouponModelEntity> getRepository() {
        return couponModelRepository;
    }


}
