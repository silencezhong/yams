package yams.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import yams.common.IRepository;
import yams.common.exception.BreezeeException;
import yams.domain.CategoryEntity;
import yams.repository.ICategoryRepository;
import yams.service.ICategoryService;

import java.util.ArrayList;
import java.util.List;

/**
 * 资产模型服务实现类
 * Created by Silence on 2016/11/3.
 */
@Service("categoryService")
public class DefaultCategoryService implements ICategoryService {

    private final ICategoryRepository categoryRepository;

    @Autowired
    public DefaultCategoryService(ICategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public IRepository<CategoryEntity> getRepository() {
        return categoryRepository;
    }


    @Override
    public List<CategoryEntity> findCategoryByParentId(String parentId) throws BreezeeException {
        List<CategoryEntity> l = new ArrayList<>();
        if (parentId.equals("-1")) {
            l = categoryRepository.findTop();
        } else {
            CategoryEntity en = categoryRepository.findOne(parentId);
            if (en != null)
                l.addAll(en.getChildren());
        }
        return l;
    }
}
