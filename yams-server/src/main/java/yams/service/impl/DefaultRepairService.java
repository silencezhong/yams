package yams.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import yams.common.IRepository;
import yams.domain.RepairCallEntity;
import yams.repository.IRepairCallRepository;
import yams.service.IRepairService;


/**
 * 报修服务实现
 * Created by Luffy on 2017/07/31.
 */
@Service("repairService")
public class DefaultRepairService implements IRepairService {

    @Autowired
    private IRepairCallRepository repairRepository;

    @Override
    public IRepository<RepairCallEntity> getRepository() {
        return repairRepository;
    }


}
