package yams.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import yams.common.IRepository;
import yams.common.constants.Constants;
import yams.domain.*;
import yams.repository.*;
import yams.service.IUserReservedService;
import yams.service.IWeChatService;
import yams.utils.SystemTool;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.TreeMap;

/**
 * 短租服务
 * Created by Luffy on 2017/08/09.
 */
@Service("userReservedService")
public class DefaultUserReservedService implements IUserReservedService {

    @Resource
    private IWeChatService weChatService;
    @Autowired
    private IHouseRepository houseRepository;
    @Autowired
    private IUserRepository userRepository;

    private final IUserReservedRepository userReservedRepository;
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public DefaultUserReservedService(IUserReservedRepository userReservedRepository,
                                      JdbcTemplate jdbcTemplate) {
        this.userReservedRepository = userReservedRepository;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public IRepository<UserReserved> getRepository() {
        return userReservedRepository;
    }

    /**
     * 发送微信消息
     * @param id
     */
    @Override
    public void sendWechatMsg(String id){

        UserReserved order = userReservedRepository.findOne(id);
        HouseEntity house = houseRepository.findOne(order.getHouse().getId());
        UserEntity manager =  userRepository.findOne(house.getManager());

            //给客户发送账单消息
            WechatTemplateMsg templateMsg = new WechatTemplateMsg();
            templateMsg.setTouser(order.getUser().getWeChat());
            templateMsg.setTemplate_id("8TBpNPfFKp8OEOVQqXHsPEcXHUvQuGjTZa9HKQuePqM");
            templateMsg.setUrl("http://"+ Constants.WECHAT_URL+"/yams/view/myShortRent?openId="+order.getUser().getWeChat());
            TreeMap<String,TreeMap<String,String>> map = Maps.newTreeMap();
//          酒店名称：{{keyword1.DATA}}
//          入住时间：{{keyword2.DATA}}
//          退房时间：{{keyword3.DATA}}
//          订单金额：{{keyword4.DATA}}
//          订单类型：{{keyword5.DATA}}
            map.put("first", templateMsg.item("尊敬的客户，您好！您已成功预订房间，请在12小时内完成支付","#173177"));
            map.put("keyword1", templateMsg.item(house.getApartmentName()+"(房间号:"+house.getHouseNumber()+")","#173177"));
            map.put("keyword2", templateMsg.item(SystemTool.dateToString(order.getArrivedDate(),
                    "yyyy-MM-dd HH:mm:ss"),"#173177"));
            map.put("keyword3", templateMsg.item(SystemTool.dateToString(order.getLeavedDate(),
                    "yyyy-MM-dd HH:mm:ss"),"#173177"));
            map.put("keyword4", templateMsg.item(order.getRentPrice()+"元","#173177"));
            map.put("keyword5", templateMsg.item( "短租","#173177"));
            map.put("remark", templateMsg.item("若有疑问请与管理员("+manager.getName()+","+manager.getPhone()+")联系","#173177"));
            templateMsg.setData(map);
            String jsonTempl = JSONObject.toJSONString(templateMsg);

            try {
                weChatService.sendTemplate(jsonTempl);
            } catch (IOException e) {
                e.printStackTrace();
            }

    }

    /**
     * 获取订单相关信息
     * @param id
     */
    @Override
    public EmptyEntity queryInfos(String id){

        EmptyEntity result = new EmptyEntity();

        UserReserved order = userReservedRepository.findOne(id);
        HouseEntity house = houseRepository.findOne(order.getHouse().getId());
        UserEntity manager =  userRepository.findOne(house.getManager());

        String userName = order.getUser().getName();
        String userPhone = order.getUser().getPhone();
        String weChat = order.getUser().getWeChat();
        String apartmentName = house.getApartmentName();
        String houseNumber = house.getHouseNumber();
        String arrivedDate = SystemTool.dateToString(order.getArrivedDate(),
                "yyyy-MM-dd HH:mm:ss");
        String leavedDate = SystemTool.dateToString(order.getLeavedDate(),
                "yyyy-MM-dd HH:mm:ss");
        String rentPrice = order.getRentPrice()+"元";
        String managerName = manager.getName();
        String managerPhone = manager.getPhone();

        result.getProperties().put("userName",userName);
        result.getProperties().put("userPhone",userPhone);
        result.getProperties().put("weChat",weChat);
        result.getProperties().put("managerWeChat",manager.getWeChat());
        result.getProperties().put("apartmentName",apartmentName);
        result.getProperties().put("houseNumber",houseNumber);
        result.getProperties().put("arrivedDate",arrivedDate);
        result.getProperties().put("leavedDate",leavedDate);
        result.getProperties().put("rentPrice",rentPrice);
        result.getProperties().put("managerName",managerName);
        result.getProperties().put("managerPhone",managerPhone);

        return result;

    }

}
