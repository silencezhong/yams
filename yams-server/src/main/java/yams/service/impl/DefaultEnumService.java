package yams.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import yams.common.IRepository;
import yams.domain.EnumEntity;
import yams.repository.IEnumRepository;
import yams.service.IEnumService;


/**
 * 数据字典服务实现
 * Created by Luffy on 2017/08/14.
 */
@Service("enumService")
public class DefaultEnumService implements IEnumService {

    @Autowired
    private IEnumRepository enumRepository;

    @Override
    public IRepository<EnumEntity> getRepository() {
        return enumRepository;
    }


}
