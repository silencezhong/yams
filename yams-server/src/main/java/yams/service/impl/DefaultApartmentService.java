package yams.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import yams.common.IRepository;
import yams.common.exception.BreezeeException;
import yams.domain.ApartmentEntity;
import yams.repository.IApartmentRepository;
import yams.service.IApartmentService;

import java.util.ArrayList;
import java.util.List;

/**
 * 公寓服务
 * Created by Silence on 2016/11/5.
 */
@Service("apartmentService")
public class DefaultApartmentService implements IApartmentService {

    private final IApartmentRepository apartmentRepository;

    @Autowired
    public DefaultApartmentService(IApartmentRepository apartmentRepository) {
        this.apartmentRepository = apartmentRepository;
    }

    @Override
    public IRepository<ApartmentEntity> getRepository() {
        return apartmentRepository;
    }

    @Override
    public List<ApartmentEntity> findCategoryByParentId(String parentId) throws BreezeeException {
        List<ApartmentEntity> l = new ArrayList<>();
        if (parentId.equals("-1")) {
            l = apartmentRepository.findTop();
        } else {
            ApartmentEntity en = apartmentRepository.findOne(parentId);
            if (en != null)
                l.addAll(en.getChildren());
        }
        return l;
    }
}
