package yams.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import yams.common.IRepository;
import yams.domain.CleanEntity;
import yams.repository.ICleanRepository;
import yams.service.ICleanService;


/**
 * 保洁服务实现
 * Created by Luffy on 2017/07/31.
 */
@Service("cleanService")
public class DefaultCleanService implements ICleanService {

    @Autowired
    private ICleanRepository cleanRepository;

    @Override
    public IRepository<CleanEntity> getRepository() {
        return cleanRepository;
    }


}
