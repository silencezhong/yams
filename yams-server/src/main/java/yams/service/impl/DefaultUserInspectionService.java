package yams.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import yams.common.IRepository;
import yams.domain.UserInspectionEntity;
import yams.repository.IUserInspectionRepository;
import yams.service.IUserInspectionService;

/**
 * 看房预约服务
 * Created by Luffy on 2017/07/29.
 */
@Service("userInspectionService")
public class DefaultUserInspectionService implements IUserInspectionService {

    private final IUserInspectionRepository userInspectionRepository;
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public DefaultUserInspectionService(IUserInspectionRepository userInspectionRepository,
                                        JdbcTemplate jdbcTemplate) {
        this.userInspectionRepository = userInspectionRepository;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public IRepository<UserInspectionEntity> getRepository() {
        return userInspectionRepository;
    }

}
