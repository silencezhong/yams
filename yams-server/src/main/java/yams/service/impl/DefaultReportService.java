package yams.service.impl;

import com.google.common.collect.Lists;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import yams.callback.IdCallback;
import yams.common.IRepository;
import yams.common.InfoPage;
import yams.common.constants.BillTypeEnum;
import yams.common.constants.CityEnum;
import yams.common.constants.PayCycleEnum;
import yams.domain.*;
import yams.repository.*;
import yams.service.IContractService;
import yams.service.IReportService;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 报表服务
 * Created by luffy on 2018/05/14.
 */
@Service("reportService")
public class DefaultReportService implements IReportService {

    @Autowired
    private IReportRepository reportRepository;
    @Autowired
    private IContractService contractService;

    @Override
    public InfoPage<ReportMonthIncomeInfo> queryMonthIncomeData(ReportRequestObj condition) {

        List<ReportMonthIncomeInfo> list = reportRepository.monthIncomeReportData(condition);
        Long total = reportRepository.monthIncomeReportTotal(condition);

        List<ReportMonthIncomeInfo> resultList = list.stream().map(obj -> {
            ReportMonthIncomeInfo info = new ReportMonthIncomeInfo();
            BeanUtils.copyProperties(obj, info);
            info.setBillAmountStr(obj.getBillAmount().toString());
            info.setBillDateStr(obj.getBillDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().toString());
            info.setBillTypeStr(BillTypeEnum.getTextByVal(obj.getBillType()));
            info.setReceiveAmountStr(obj.getReceiveAmount().toString());
            info.setCityName(CityEnum.getTextByVal(obj.getCity()));

            return info;
        }).collect(Collectors.toList());

        InfoPage<ReportMonthIncomeInfo> page = new InfoPage<>();
        page.setContent(resultList);
        page.setTotal(total);


        return page;
    }

    @Override
    public List<ReportMonthIncomeInfo> listMonthIncomeData(ReportRequestObj condition) {
        List<ReportMonthIncomeInfo> list = reportRepository.listMonthIncomeReportData(condition);
        List<ReportMonthIncomeInfo> resultList = list.stream().map(obj -> {
            ReportMonthIncomeInfo info = new ReportMonthIncomeInfo();
            BeanUtils.copyProperties(obj, info);
            info.setBillAmountStr(obj.getBillAmount().toString());
            info.setBillDateStr(obj.getBillDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().toString());
            info.setBillTypeStr(BillTypeEnum.getTextByVal(obj.getBillType()));
            info.setReceiveAmountStr(obj.getReceiveAmount().toString());
            info.setCityName(CityEnum.getTextByVal(obj.getCity()));

            return info;
        }).collect(Collectors.toList());
        return resultList;
    }

    //合同
    @Override
    public InfoPage<ContractReportInfo> queryContractReportData(ContractReportRequestObj condition) {

        List<ContractReportInfo> list = reportRepository.contractReportData(condition);
        Long total = reportRepository.contractReportTotal(condition);
        List<ContractReportInfo> resultList = list.stream().map(obj -> {
            ContractReportInfo crdata = new ContractReportInfo();
            BeanUtils.copyProperties(obj, crdata);
            crdata.setCityName(CityEnum.getTextByVal(obj.getCityName()));
            crdata.setPayMethodStr(PayCycleEnum.getTextByVal(obj.getPayMethod()));
            crdata.setMonthlyRentStr(obj.getMonthlyRent().toString());
            if (obj.getPledge() != null)
                crdata.setPledgeStr(obj.getPledge().toString());
            crdata.setRentStartDateStr(obj.getRentStartDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().toString());
            crdata.setRentEndDateStr(obj.getRentEndDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().toString());
            if (obj.getIsDelay() < 0 && Math.abs(obj.getIsDelay()) <= 15) {
                crdata.setIsDelayStr("是");
            } else if (obj.getIsDelay() < 0 && Math.abs(obj.getIsDelay()) > 15) {
                crdata.setIsDelayStr("否");
            }

            if (obj.getDelayDay() != null){
                if (obj.getDelayDay() < 0) {
                    Integer integer = Math.abs(obj.getDelayDay());
                    crdata.setDelayDayStr(integer.toString());
                } else {
                    Integer integer = 0-obj.getDelayDay();
                    crdata.setDelayDayStr(integer.toString());
                }
            }
            if (obj.getBillAmount() != null)
                crdata.setBillAmountStr(obj.getBillAmount().toString());
            return crdata;
        }).collect(Collectors.toList());


        InfoPage<ContractReportInfo> page = new InfoPage<>();
        page.setContent(resultList);
        page.setTotal(total);


        return page;
    }

    @Override
    public List<ContractReportInfo> listContractInfoData(ContractReportRequestObj condition) {
        List<ContractReportInfo> list = reportRepository.listContractReportData(condition);
        List<ContractReportInfo> resultList = list.stream().map(obj -> {
            ContractReportInfo crdata = new ContractReportInfo();
            BeanUtils.copyProperties(obj, crdata);
            crdata.setCityName(CityEnum.getTextByVal(obj.getCityName()));
            crdata.setPayMethodStr(PayCycleEnum.getTextByVal(obj.getPayMethod()));
            crdata.setMonthlyRentStr(obj.getMonthlyRent().toString());
            if (obj.getPledge() != null)
                crdata.setPledgeStr(obj.getPledge().toString());
            crdata.setRentStartDateStr(obj.getRentStartDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().toString());
            crdata.setRentEndDateStr(obj.getRentEndDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().toString());
            if (obj.getIsDelay() < 0 && Math.abs(obj.getIsDelay()) <= 15) {
                crdata.setIsDelayStr("是");
            } else if (obj.getIsDelay() < 0 && Math.abs(obj.getIsDelay()) > 15) {
                crdata.setIsDelayStr("否");
            }
            if (obj.getDelayDay() != null){
                if (obj.getDelayDay() < 0) {
                    Integer integer = Math.abs(obj.getDelayDay());
                    crdata.setDelayDayStr(integer.toString());
                } else {
                    Integer integer = 0-obj.getDelayDay();
                    crdata.setDelayDayStr(integer.toString());
                }
            }
            if (obj.getBillAmount() != null)
                crdata.setBillAmountStr(obj.getBillAmount().toString());
            return crdata;


        }).collect(Collectors.toList());
        return resultList;
    }

    @Override
    public BigDecimal todayIncome() {

        LocalDate now = LocalDate.now();
        LocalDate tomorrow = now.plusDays(1);
        DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String startDate = now.format(formatters) + " 00:00:00";
        String endDate = tomorrow.format(formatters) + " 00:00:00";

        BigDecimal result = reportRepository.sumTodayIncome(startDate, endDate);

        return result;
    }

    @Override
    public Long deadlineHouse() {
        /*即将到期*/
        ContractEntity contractCondition1 = new ContractEntity();
        contractCondition1.getProperties().put("endDate_gt", Date.from(LocalDateTime.now()
                .atZone(ZoneId.systemDefault()).toInstant()));
        contractCondition1.getProperties().put("endDate_le", Date.from(LocalDateTime.now().plusDays(15)
                .atZone(ZoneId.systemDefault()).toInstant()));
        return contractService.count(contractCondition1);
    }

}
