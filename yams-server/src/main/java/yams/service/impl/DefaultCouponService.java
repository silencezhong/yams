package yams.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import yams.common.IRepository;
import yams.domain.*;
import yams.repository.ICouponRepository;
import yams.service.ICouponService;


/**
 * 券服务实现
 * Created by Luffy on 2017/06/22.
 */
@Service("couponService")
public class DefaultCouponService implements ICouponService {

    @Autowired
    private ICouponRepository couponRepository;

    @Override
    public IRepository<CouponEntity> getRepository() {
        return couponRepository;
    }


}
