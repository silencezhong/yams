package yams.service.impl;

import org.hibernate.mapping.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;
import yams.domain.AccountEntity;
import yams.domain.ApartmentEntity;
import yams.repository.IDataAuthorityRepository;
import yams.service.IDataauthorityService;

import javax.persistence.Id;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.*;

/**
 * Created by LX on 2017/7/31.
 */
@Service("dataauthorityService")
public class DefaultDataauthorityService implements IDataauthorityService {
    private final IDataAuthorityRepository dataAuthorityRepository;
    private final JdbcTemplate jdbcTemplate;
    private final PlatformTransactionManager transactionManager;
    @Autowired
    public DefaultDataauthorityService(IDataAuthorityRepository dataAuthorityRepository, JdbcTemplate jdbcTemplate, PlatformTransactionManager transactionManager) {
        this.dataAuthorityRepository = dataAuthorityRepository;
        this.jdbcTemplate = jdbcTemplate;
        this.transactionManager = transactionManager;
    }
    //保存关联关系
    @Override
    public void updateAcnApmRel(String acnId,String apmId){
        String insertSql="insert into R_APM_ACN (ACN_ID,APM_ID) values(?,?)";
        TransactionTemplate transactionTemplate=new TransactionTemplate(transactionManager);
        transactionTemplate.execute(status ->{
            jdbcTemplate.update(insertSql,new Object[]{acnId,apmId});
            return null;
        });
    }
    //获得所有的管理用户
    @Override
    public List<AccountEntity> getAllacn() {
        String selectAllSql="select DISTINCT ACN_ID,T_ACCOUNT.CODE,MOBILE,T_ACCOUNT.NAME from R_APM_ACN,T_ACCOUNT where R_APM_ACN.ACN_ID=T_ACCOUNT.PK_ID";
        TransactionTemplate transactionTemplate=new TransactionTemplate(transactionManager);
        return transactionTemplate.execute(status ->{
            List<AccountEntity> accounts=new ArrayList<>();
            List<java.util.Map<String, Object>> rows=jdbcTemplate.queryForList(selectAllSql);
            for (java.util.Map row:rows){
                AccountEntity account=new AccountEntity();
                account.setId((String)(row.get("ACN_ID")));
                account.setMobile((String)(row.get("MOBILE")));
                account.setName((String)(row.get("NAME")));
                account.setCode((String)(row.get("CODE")));
                accounts.add(account);
            }
            return accounts;
        });
    }

    //获某个用户所有的管理公寓
    @Override
    public List<ApartmentEntity> getAllapt(String id) {
        String selectAllSql="select * from R_APM_ACN,T_APARTMENT where R_APM_ACN.APM_ID=T_APARTMENT.PK_ID AND R_APM_ACN.ACN_ID=?";
        TransactionTemplate transactionTemplate=new TransactionTemplate(transactionManager);
        return transactionTemplate.execute(status ->{
            List<ApartmentEntity> apartments=new ArrayList<>();
            List<java.util.Map<String, Object>> rows=jdbcTemplate.queryForList(selectAllSql,new Object[]{id});
            for (java.util.Map row:rows){
                ApartmentEntity apartment=new ApartmentEntity();
                apartment.setId((String)(row.get("APM_ID")));
                apartment.setAddress((String)(row.get("ADDRESS")));
                apartment.setName((String)(row.get("NAME")));
                apartment.setCode((String)(row.get("CODE")));
                apartments.add(apartment);
            }
            return apartments;
        });
    }


    //获得账户和公寓通过acnid
    @Override
    public Set<ApartmentEntity> setAll(String id) {
        String selectAllSql="select * from R_APM_ACN,T_APARTMENT where R_APM_ACN.APM_ID=T_APARTMENT.PK_ID AND R_APM_ACN.ACN_ID=?";
        TransactionTemplate transactionTemplate=new TransactionTemplate(transactionManager);
        return transactionTemplate.execute(status ->{
            Set<ApartmentEntity> apartments=new HashSet<>();
            List<java.util.Map<String, Object>> rows=jdbcTemplate.queryForList(selectAllSql,new Object[]{id});
            for (java.util.Map row:rows){
                ApartmentEntity apartment=new ApartmentEntity();
                apartment.setId((String)(row.get("APM_ID")));
                apartment.setAddress((String)(row.get("ADDRESS")));
                apartment.setName((String)(row.get("NAME")));
                apartment.setCode((String)(row.get("CODE")));
                apartments.add(apartment);
            }
            return apartments;
        });
    }


}
