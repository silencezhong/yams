package yams.service;

import yams.common.IServiceLayer;
import yams.common.InfoPage;
import yams.domain.CheckmeterEntity;


/**
 * 抄表服务
 * Created by Luffy on 2017/06/13.
 */
public interface ICheckmeterService extends IServiceLayer<CheckmeterEntity> {

    InfoPage<CheckmeterEntity> queryCheckmeter(CheckmeterEntity checkmeterEntity);
    InfoPage<CheckmeterEntity> filterCheckmeter(CheckmeterEntity checkmeterEntity);
    void test();

}
