package yams.service;

import yams.common.IServiceLayer;
import yams.common.exception.BreezeeException;
import yams.domain.ApartmentEntity;

import java.util.List;

/**
 * 公寓服务接口
 * Created by Silence on 2016/11/5.
 */
public interface IApartmentService extends IServiceLayer<ApartmentEntity> {

    List<ApartmentEntity> findCategoryByParentId(String parentId) throws BreezeeException;
}
