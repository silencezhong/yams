package yams.service;

import yams.common.IServiceLayer;
import yams.domain.UserEntity;

/**
 * 会员服务接口
 * Created by Luffy on 2017/07/29.
 */
public interface IUserService extends IServiceLayer<UserEntity> {

    UserEntity autoUserCode(UserEntity userEntity);

}
