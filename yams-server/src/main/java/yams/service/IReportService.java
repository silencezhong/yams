package yams.service;

import yams.common.InfoPage;
import yams.domain.ContractReportInfo;
import yams.domain.ContractReportRequestObj;
import yams.domain.ReportMonthIncomeInfo;
import yams.domain.ReportRequestObj;

import java.math.BigDecimal;
import java.util.List;


/**
 * 报表服务接口
 * Created by Luffy on 2018/05/14.
 */
public interface IReportService {

    InfoPage<ReportMonthIncomeInfo> queryMonthIncomeData(ReportRequestObj condition);
    List<ReportMonthIncomeInfo> listMonthIncomeData(ReportRequestObj condition);
    InfoPage<ContractReportInfo> queryContractReportData(ContractReportRequestObj condition);
    List<ContractReportInfo> listContractInfoData(ContractReportRequestObj condition);

    BigDecimal todayIncome();
    Long deadlineHouse();

}
