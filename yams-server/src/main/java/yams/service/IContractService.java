package yams.service;

import yams.common.IServiceLayer;
import yams.domain.ContractEntity;
import java.util.List;

/**
 * 合同服务
 * Created by Silence on 2017/5/3.
 */
public interface IContractService extends IServiceLayer<ContractEntity> {

    void generateRentBill(List<String> resultList);
    void finishContracts(List<String> resultList);
    void checkoutHouse();
    ContractEntity gaenerateContractCode(ContractEntity contractEntity);

}
