package yams.service;

import yams.common.IServiceLayer;
import yams.domain.EnumEntity;


/**
 * 数据字典服务
 * Created by Luffy on 2017/08/14.
 */
public interface IEnumService extends IServiceLayer<EnumEntity> {


}
