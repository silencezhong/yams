package yams.service;

import yams.common.IServiceLayer;
import yams.domain.CouponModelEntity;


/**
 * 券模型服务
 * Created by Luffy on 2017/06/22.
 */
public interface ICouponModelService extends IServiceLayer<CouponModelEntity> {


}
