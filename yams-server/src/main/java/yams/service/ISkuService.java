package yams.service;

import yams.common.IServiceLayer;
import yams.common.InfoPage;
import yams.domain.SkuEntity;

/**
 * sku服务接口
 * Created by Silence on 2016/11/11.
 */
public interface ISkuService extends IServiceLayer<SkuEntity> {

    InfoPage<SkuEntity> findNullHouse(SkuEntity sku);
}
