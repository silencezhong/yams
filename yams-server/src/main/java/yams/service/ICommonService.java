package yams.service;

import yams.common.IServiceLayer;
import yams.domain.AbstractEntity;

/**
 * 通用的服务
 * Created by Silence on 2016/11/3.
 */
public interface ICommonService extends IServiceLayer<AbstractEntity> {
}
