package yams.service;

import yams.common.IServiceLayer;
import yams.common.exception.BreezeeException;
import yams.domain.BillEntity;
import yams.domain.ContractEntity;

/**
 * 账单服务接口
 * Created by Silence on 2017/5/2.
 */
public interface IBillService extends IServiceLayer<BillEntity> {

    void generateBill(ContractEntity contract) throws BreezeeException;
    void payBill(BillEntity billEntity) throws BreezeeException;
    void overdueBill();

}
