package yams.service;

import yams.domain.AccountEntity;
import yams.domain.ApartmentEntity;

import java.util.List;
import java.util.Set;

/**
 * Created by LX on 2017/7/31.
 * 权限服务接口
 */
public interface IDataauthorityService {
   void updateAcnApmRel (String acnId,String apmId);
   List<AccountEntity> getAllacn();
   List<ApartmentEntity> getAllapt(String id);
   Set<ApartmentEntity> setAll(String id);
}
