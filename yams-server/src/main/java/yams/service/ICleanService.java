package yams.service;

import yams.common.IServiceLayer;
import yams.domain.CleanEntity;


/**
 * 保洁服务
 * Created by Luffy on 2017/07/31.
 */
public interface ICleanService extends IServiceLayer<CleanEntity> {


}
