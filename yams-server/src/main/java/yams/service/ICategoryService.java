package yams.service;

import yams.common.IServiceLayer;
import yams.common.exception.BreezeeException;
import yams.domain.CateAttrEntity;
import yams.domain.CategoryEntity;

import java.util.List;

/**
 * 资产模型服务
 * Created by Silence on 2016/11/3.
 */
public interface ICategoryService extends IServiceLayer<CategoryEntity> {

    /**
     * 获取指定父类的子类
     * @param parentId 父类
     * @return 子类类别
     */
    List<CategoryEntity> findCategoryByParentId(String parentId) throws BreezeeException;
}
