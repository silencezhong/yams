package yams.service;

import yams.common.IServiceLayer;
import yams.common.InfoPage;
import yams.domain.CheckmeterEntity;
import yams.domain.CouponEntity;
import yams.domain.CouponModelEntity;


/**
 * 券服务
 * Created by Luffy on 2017/06/22.
 */
public interface ICouponService extends IServiceLayer<CouponEntity> {


}
