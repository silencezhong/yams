package yams.service;

import yams.common.IServiceLayer;
import yams.domain.UserInspectionEntity;

/**
 * 看房预约服务接口
 * Created by Luffy on 2017/07/29.
 */
public interface IUserInspectionService extends IServiceLayer<UserInspectionEntity> {


}
