package yams.service;

import yams.common.IServiceLayer;
import yams.domain.EmptyEntity;
import yams.domain.UserReserved;

/**
 * 短租服务接口
 * Created by Luffy on 2017/08/09.
 */
public interface IUserReservedService extends IServiceLayer<UserReserved> {

    void sendWechatMsg(String id);
    EmptyEntity queryInfos(String id);

}
