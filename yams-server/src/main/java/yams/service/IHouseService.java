package yams.service;

import yams.common.IServiceLayer;
import yams.common.exception.BreezeeException;
import yams.domain.ApartmentEntity;
import yams.domain.HouseEntity;

import java.util.List;

/**
 * 房间服务接口
 * Created by Luffy on 2017/06/06.
 */
public interface IHouseService extends IServiceLayer<HouseEntity> {

    void saveBeds(HouseEntity obj);
    void saveDayPrice(HouseEntity obj);

}
