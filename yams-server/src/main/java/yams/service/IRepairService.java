package yams.service;

import yams.common.IServiceLayer;
import yams.domain.RepairCallEntity;


/**
 * 报修服务
 * Created by Luffy on 2017/07/31.
 */
public interface IRepairService extends IServiceLayer<RepairCallEntity> {


}
