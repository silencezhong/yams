package yams.repository;

import org.springframework.stereotype.Repository;
import yams.common.IRepository;
import yams.domain.DayPriceEntity;

/**
 * 短租价格持久化服务
 * Created by Luffy on 2017/06/08.
 */
@Repository("dayPriceRepository")
public interface IDayPriceRepository extends IRepository<DayPriceEntity> {

}
