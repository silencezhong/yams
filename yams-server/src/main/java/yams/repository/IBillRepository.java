package yams.repository;

import org.springframework.stereotype.Repository;
import yams.common.IRepository;
import yams.domain.BillEntity;

/**
 * 账单的持久化服务
 * Created by Silence on 2017/5/2.
 */
@Repository("billRepository")
public interface IBillRepository extends IRepository<BillEntity> {
}
