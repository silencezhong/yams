package yams.repository;

import org.springframework.stereotype.Repository;
import yams.common.IRepository;
import yams.domain.RoleEntity;

/**
 * 角色持久化服务
 * Created by Silence on 2016/10/25.
 */
@Repository("roleRepository")
public interface IRoleRepository extends IRepository<RoleEntity> {
}
