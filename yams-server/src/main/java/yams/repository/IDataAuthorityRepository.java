package yams.repository;

import org.springframework.stereotype.Repository;
import yams.common.IRepository;
import yams.domain.AccountEntity;
import yams.domain.EnumEntity;

/**
 * 数据权限持久化服务
 * Created by Silence on 2016/10/25.
 */
@Repository("dataAuthorityRepository")
public interface IDataAuthorityRepository extends IRepository<AccountEntity> {
}
