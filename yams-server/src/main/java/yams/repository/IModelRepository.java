package yams.repository;

import org.springframework.stereotype.Repository;
import yams.common.IRepository;
import yams.domain.ModelEntity;

/**
 * 模型持久化服务
 * Created by Silence on 2016/11/1.
 */
@Repository("modelRepository")
public interface IModelRepository extends IRepository<ModelEntity> {
}
