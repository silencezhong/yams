package yams.repository;

import org.springframework.stereotype.Repository;
import yams.common.IRepository;
import yams.domain.HouseEntity;

/**
 * 房屋持久化服务
 * Created by Silence on 2016/10/11.
 */
@Repository("houseRepository")
public interface IHouseRepository extends IRepository<HouseEntity> {
}
