package yams.repository;

import yams.common.IRepository;
import yams.domain.OrganizationEntity;

/**
 * 组织持久化服务
 * Created by Silence on 2016/10/25.
 */
public interface IOrganizationRepository extends IRepository<OrganizationEntity> {
}
