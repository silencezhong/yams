package yams.repository;

import org.springframework.stereotype.Repository;
import yams.common.IRepository;
import yams.domain.UserReserved;

/**
 * 预定持久化服务
 * Created by Silence on 2016/11/5.
 */
@Repository("userReservedRepository")
public interface IUserReservedRepository extends IRepository<UserReserved> {
}
