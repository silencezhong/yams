package yams.repository;

import org.springframework.stereotype.Repository;
import yams.common.IRepository;
import yams.domain.BedEntity;

/**
 * 床铺持久化服务
 * Created by Luffy on 2017/06/06.
 */
@Repository("bedRepository")
public interface IBedRepository extends IRepository<BedEntity> {

}
