package yams.repository;

import org.springframework.stereotype.Repository;
import yams.common.IRepository;
import yams.domain.ModelItemEntity;

/**
 * 模型条目持久化
 * Created by Silence on 2016/11/1.
 */
@Repository("modelItemRepository")
public interface IModelItemRepository extends IRepository<ModelItemEntity> {
}
