package yams.repository;

import org.springframework.stereotype.Repository;
import yams.common.IRepository;
import yams.domain.AttributeEntity;

/**
 * 属性持久化服务
 * Created by Silence on 2016/11/3.
 */
@Repository("attributeRepository")
public interface IAttributeRepository extends IRepository<AttributeEntity> {
}
