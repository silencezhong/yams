package yams.repository;

import org.springframework.stereotype.Repository;
import yams.common.IRepository;
import yams.domain.UserEntity;

/**
 * 会员持久化服务
 * Created by Silence on 2016/10/16.
 */
@Repository("userRepository")
public interface IUserRepository extends IRepository<UserEntity> {

    UserEntity findByPhone(String phone);

}
