package yams.repository;

import org.springframework.stereotype.Repository;
import yams.common.IRepository;
import yams.domain.CouponEntity;
import yams.domain.CouponModelEntity;
import yams.domain.UserEntity;

/**
 * 券持久化服务
 * Created by Luffy on 2017/06/22.
 */
@Repository("couponRepository")
public interface ICouponRepository extends IRepository<CouponEntity> {


}
