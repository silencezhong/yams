package yams.repository;

import org.springframework.stereotype.Repository;
import yams.common.IRepository;
import yams.domain.CouponModelEntity;

/**
 * 券模型持久化服务
 * Created by Luffy on 2017/06/22.
 */
@Repository("couponModelRepository")
public interface ICouponModelRepository extends IRepository<CouponModelEntity> {


}
