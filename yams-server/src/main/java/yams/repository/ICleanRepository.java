package yams.repository;

import org.springframework.stereotype.Repository;
import yams.common.IRepository;
import yams.domain.CleanEntity;


/**
 * 保洁实体持久化
 * Created by LX on 2017/7/25.
 */
@Repository("cleanRepository")
public interface ICleanRepository extends IRepository<CleanEntity> {
}
