package yams.repository;

import org.springframework.stereotype.Repository;
import yams.common.IRepository;
import yams.domain.ProductEntity;

/**
 * 产品持久化服务
 * Created by Silence on 2016/11/3.
 */
@Repository("productRepository")
public interface IProductRepository extends IRepository<ProductEntity> {
}
