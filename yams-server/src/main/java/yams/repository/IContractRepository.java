package yams.repository;

import org.springframework.stereotype.Repository;
import yams.common.IRepository;
import yams.domain.ContractEntity;

/**
 * 合同持久化服务
 * Created by Silence on 2016/10/16.
 */
@Repository("contractRepository")
public interface IContractRepository extends IRepository<ContractEntity> {
}
