package yams.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import yams.common.IRepository;
import yams.domain.ApartmentEntity;
import yams.domain.CategoryEntity;

import java.util.List;

/**
 * 公寓持久化服务
 * Created by Silence on 2016/11/1.
 */
@Repository("apartmentRepository")
public interface IApartmentRepository extends IRepository<ApartmentEntity> {

    @Query("select c from ApartmentEntity c where c.parent is null")
    List<ApartmentEntity> findTop();
}
