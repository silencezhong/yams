package yams.repository;

import org.springframework.stereotype.Repository;
import yams.common.IRepository;
import yams.domain.RepairCallEntity;

/**
 * 报修实体持久化
 * Created by Silence on 2016/11/1.
 */
@Repository("repairCallRepository")
public interface IRepairCallRepository extends IRepository<RepairCallEntity> {
}
