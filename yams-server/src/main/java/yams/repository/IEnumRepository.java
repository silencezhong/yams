package yams.repository;

import org.springframework.stereotype.Repository;
import yams.common.IRepository;
import yams.domain.EnumEntity;

/**
 * 枚举值持久化服务
 * Created by Silence on 2016/10/25.
 */
@Repository("enumRepository")
public interface IEnumRepository extends IRepository<EnumEntity> {
}
