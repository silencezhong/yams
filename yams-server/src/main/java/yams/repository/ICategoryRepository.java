package yams.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import yams.common.IRepository;
import yams.domain.CategoryEntity;

import java.util.List;

/**
 * 模型持久化服务
 * Created by Silence on 2016/11/3.
 */
@Repository("categoryRepository")
public interface ICategoryRepository extends IRepository<CategoryEntity> {

    @Query("select c from CategoryEntity c where c.parent is null")
    List<CategoryEntity> findTop();
}
