package yams.repository;

import org.springframework.stereotype.Repository;
import yams.common.IRepository;
import yams.domain.AccountEntity;

/**
 * 账号的持久化服务
 * Created by Silence on 2016/10/24.
 */
@Repository("accountRepository")
public interface IAccountRepository extends IRepository<AccountEntity> {
}
