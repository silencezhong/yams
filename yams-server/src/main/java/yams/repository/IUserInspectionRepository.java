package yams.repository;

import org.springframework.stereotype.Repository;
import yams.common.IRepository;
import yams.domain.UserInspectionEntity;
import yams.domain.UserReserved;

/**
 * 预约看房持久化服务
 * Created by Luffy on 2017/07/20.
 */
@Repository("userInspectionRepository")
public interface IUserInspectionRepository extends IRepository<UserInspectionEntity> {
}
