/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */

module.exports = {
    title: '逸庭公寓管理平台',
    contextPath: '/yams-bo',
    viewPrefix: '/view',
    publicPath: "/static-yams-bo",

    mode: "",
    mock: false,
    cdn: '//cdn.bootcss.com',
    //cdn: '',
    uploadPath: '/home/yams/app/uploadFiles/images',

    apiServerUrl: 'http://127.0.0.1:9130',
    apiFilePath: '/../apiConfig.json'
};
