/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved. 
 */
"use strict";
let api = require('../../utils/api');

module.exports = {

    index: function (req, res, callback) {
        global.tool.send({
            method: 'post',
            uri: api.get('c97b00aa70fc4ed1b5922bced4468a0b'),
            json: {
                properties: {
                    model_code_obj_ae: 'systemNotice'
                }
            }
        }, function (error, response, body) {
           let da = body.rows || [];
            callback(da);
        });
    },

    houseSearch: function (req,res,callback) {
        global.tool.send({
            method: 'get',
            uri: api.get('5a543d0c5b214d2c855124d15e1fca75')
        }, function (error, response, body) {
            body = body || [];
            callback({houseList: body});
        });
    },

    account: function (req, res, callback) {
        global.tool.send({
            method: 'get',
            uri: api.get('5b90151b418f4ceb9469a6b265b0d617').replace(new RegExp('{userId}'),req.session.userData.userId)
        }, function (error, response, body) {
            callback(body || {});
        });
    },

    news:function (req, res, callback) {
        global.tool.send({
            method: 'get',
            uri: api.get('3a3e8083744e4d6d84f0f28e686552cc')
        }, function (error, response, body) {
            callback(body || []);
        });
    },

    message: function (req, res, callback) {
        global.tool.send({
            method: 'get',
            uri: api.get('3a3e8083744e4d6d84f0f28e686552cc')
        }, function (error, response, body) {
            callback(body || []);
        });
    }
};