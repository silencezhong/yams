/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved. 
 */
"use strict";
module.exports = {

    billManagement : function (req, res, callback) {

        if('todayIncome' == req.query.fromFlag){
            callback({
                fromFlag : 'todayIncome'
            });

        }else {
            callback({});
        }

    },
    contractManagement : function (req, res, callback) {

        if('deadlineHouse' == req.query.fromFlag){
            callback({
                fromFlag : 'deadlineHouse'
            });

        }else {
            callback({});
        }

    }

};