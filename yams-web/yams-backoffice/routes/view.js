/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */
"use strict";
const extend = require('extend');
const router = require('express').Router();
const security = require('../utils/auth');
const _route = {
    index: require('./viewRoutes/index'),
    house: require('./viewRoutes/house'),
    crm: require('./viewRoutes/crm')
};

router.all('*', security.checkLogin, security.requireAuthentication, security.loadUser);

// 该路由使用的中间件
router.use(function timeLog(req, res, next) {
    console.log('view::' + req.path, Date.now());
    next();
});

router.get('/logout', function (req, res, next) {
    delete req.session.userData;
    res.redirect(global.config.contextPath + global.config.viewPrefix + '/');
});

/**
 * 实现路由转发
 */
router.use('/', function (req, res, next) {
    let url = req.session.endType + (req.path.endsWith('/') ? req.path + "index" : req.path)
        , routes = req.path.split("/")
        , skip = true
        , renderParam = {
        path: url,
        queryData: req.query || {},
        session: req.session || {},
        cookie: req.cookies || {},
        userData: req.session.userData,
        data: {},
        title: global.config.title
    };
    if (routes.length > 1) { //目前我们支持二层的url结构
        try {
            let fun;
            if(routes.length==2)
                fun = _route['index'][routes[1]];
            else if(routes.length=3)
                fun = _route[routes[1]][routes[2]];
            if (fun != null) {
                skip = false;
                fun(req, res, function (data, err) {
                    if (err) {
                        next(err);
                    } else {
                        renderParam.data = data;
                        res.render(url, renderParam);
                    }
                });
            }
        } catch (e) {
        }
    }
    if (skip) {
        res.render(url, renderParam);
    }
});

module.exports = router;
