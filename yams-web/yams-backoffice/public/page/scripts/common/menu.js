/**
 * 实现菜单的选择
 */
yams.menu = (function () {

    this.select = function (code) {
        var curItem = $('[menuCode="'+code+'"]');
        curItem.addClass('active').closest('.dropdown').addClass('active');

        if(/^workspace-/.test(code)){
            $('#pageHeader [menuCode="workspace"]').addClass('active');
        }
    };

    this._initMenu = function () {
        $("#navbar li[menuCode='none']").remove();
    };

    this._initMenu();

    return this;
})();





