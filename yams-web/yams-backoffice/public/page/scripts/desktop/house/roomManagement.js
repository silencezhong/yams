"use strict";
$(function () {
    yams.menu.select('roomManagement');

    yams.page = {
        /**
         * 初始化页面
         */
        init: function () {
            Dolphin.form.parse();
            this.initDataList();
            this.categoryTree();
            this.itemLine();
            this.initHouseList();
            this.initEvent();
            this.initSearch();
        },
        /**
         * 初始化页面事件
         */
        initEvent: function () {

            let me = this;

            /**
             * 房型关联房屋
             */
            $("#houseAdd").click(function () {
                $("#houseDataModal").modal('show');
                let condition = { "houseGroup_is_empty" : "0"};
                me._houseList.query(condition);
            });

            /**
             * 确定选中的房屋
             */
            $(".confirmHouse").click(function(){

                let houseArray = me._houseList.getChecked();
                var ids = [];
                $.each(houseArray,function(idx,obj){
                    ids.push(obj.id);
                })
                var param = {"code" : $("#houseGroupCode").val(),
                             "ids" : ids};
                Dolphin.ajax({
                    url: '/api/afcbfc26b4d34776a6fb2d951b1ac510',
                    type: Dolphin.requestMethod.PUT,
                    data : Dolphin.json2string(param),
                    onSuccess: function (reData) {
                        me._itemList.reload();
                        $('#houseDataModal').modal('hide');
                    }
                });
            });

            /**
             * 修改房型
             */
            $(".modifyRoomGroup").click(function () {
                var formData = Dolphin.form.getValue('.room-group-form');

                var paramData = {"id" : formData.roomGroupHouseId,
                                 "houseGroup" : formData.roomGroup};
                Dolphin.ajax({
                    url: '/api/f3ea9eb2de6e47bfb897481d857a1056',
                    type: Dolphin.requestMethod.PUT,
                    data: Dolphin.json2string(paramData),
                    onSuccess: function (reData) {
                        me._itemList.reload();
                        $('#roomGroupModal').modal('hide');
                    }
                });

            });

            $(".btn-query").click(function () {
                me._itemList.load(null, Dolphin.form.getValue('.query-form'))
            });
        },

        initDataList: function () {
            let me = this;
        },

        categoryTree: function () {
            let me = this;
            this._categoryTree = new Dolphin.TREE({
                defaultId: -1,
                requestKey: 'parentId',
                panel: '#categoryTree',
                url: '/api/bf6230f25b974827bc3dec9dd789edc5@parentId={parentId}',
                multiple: false,
                onChecked: function (data) {

                    $("#houseGroupCode").val(data.code);

                    me.selectNode = data;
                    console.log(data);
                    if (data.directory) {
                        me._itemList.query({
                            _cityId: data.code !== 'rootRoom'?data.id:''
                        });
                    } else {
                        me._itemList.query({
                            houseGroup : data.code
                        });
                    }
                },
                onLoad: function () {
                    if (this.data && this.data.length > 0) {
                        let n = this.data[0];
                        this.expend(n);
                    }
                },
                nameField: function (data) {
                    if (!data.directory) {
                        return "<span style='color: #00ff;'>" + data.name + "</span>";
                    }
                    return data.name;
                }
            });
        },

        initHouseList: function () {
            let me = this;
            this._houseList = new Dolphin.LIST({
                panel: '#houseList',
                idField: 'id',
                queryParams:{
                    status : 1
                },
                columns: [{
                    code: 'name',
                    title: '名称'
                }, {
                    code: 'houseType',
                    title: '户型',
                    formatter: function (val) {
                        return Dolphin.enum.getEnumText("houseType", val);
                    }
                }, {
                    code: 'floor',
                    title: '楼层'
                }, {
                    code: 'area',
                    title: '面积'
                }, {
                    code: 'monthly',
                    title: '月租'
                }, {
                    code: 'recommend',
                    title: '推荐',
                    formatter: function (val) {
                        return val ? "是" : "否";
                    }
                }, {
                    code: 'statusName',
                    title: '状态'
                }],
                multiple: true,
                rowIndex: true,
                checkbox: true,
                pagination: true,

                ajaxType: Dolphin.requestMethod.POST,
                url: '/api/9651431c68a04d54a22f1e2e7faf827b',
                onLoadSuccess: function (data) {
                }
            });
        },

        itemLine: function () {
            let me = this;
            this._itemList = new Dolphin.LIST({
                panel: '#itemList',
                url: '/api/9651431c68a04d54a22f1e2e7faf827b',
                ajaxType: 'post',
                mockPathData: ['id'],
                data: {rows: [], total: 0},
                pagination: true,
                multiple: false,
                columns: [{
                    code: 'name',
                    title: '名称'
                }, {
                    code: 'houseType',
                    title: '户型',
                    formatter: function (val) {
                        return Dolphin.enum.getEnumText("houseType", val);
                    }
                }, {
                    code: 'floor',
                    title: '楼层'
                }, {
                    code: 'area',
                    title: '面积'
                }, {
                    code: 'monthly',
                    title: '月租'
                }, {
                    code: 'recommend',
                    title: '推荐',
                    formatter: function (val) {
                        return val ? "是" : "否";
                    }
                }, {
                    code: 'statusName',
                    title: '状态'
                }, {
                    code: 'id',
                    title: '&nbsp;',
                    width: '160px',
                    formatter: function (val, row, index) {
                        let html = [];
                        var editBtn = '<a class="btn btn-outline btn-circle btn-sm dark editBtn" data-id="' + val
                            + '" data-housegroup="'+row.houseGroup+'"  href="javascript:void(0);"><i class="fa fa-edit"></i>修改</a>';
                        var delBtn = '<a class="btn btn-outline btn-circle btn-sm dark delBtn" data-id="' + val
                            + '"  href="javascript:void(0);"><i class="fa fa-trash"></i>删除</a>'

                        html.push(editBtn);
                        html.push(delBtn);
                        return html.join('');
                    }
                }],
                onLoadSuccess: function (data) {

                    $('.editBtn').click(function () {

                        $("#roomGroupModal").modal('show');
                        $("#roomGroup").val($(this).data('housegroup'));
                        $("#roomGroupHouseId").val($(this).data('id'));

                    });
                    $('.delBtn').click(function () {
                        var _this = $(this);
                        Dolphin.confirm('确认删除？', {
                            callback: function (flag) {
                                if (flag) {
                                    Dolphin.ajax({
                                        url: '/api/b76c368f1ad34798b9baabf03e897b48@id=' + _this.data('id'),
                                        type: Dolphin.requestMethod.DELETE,
                                        onSuccess: function (reData) {
                                            me._itemList.reload();
                                        }
                                    });
                                }
                            }
                        })
                    });
                },
                onCheck: function (data, row, thisInput) {
                },
                onChecked: function (data, row, thisCheckbox) {
                    me.curSelect = data;
                },
                onUnchecked: function (data) {
                }
            });
        },

        initSearch: function () {
            let me = this;
            $("#skuId").bsSuggest({
                delayUntilKeyup: true,
                allowNoKeyword: false,   //是否允许无关键字时请求数据。为 false 则无输入时不执行过滤请求
                multiWord: false,         //以分隔符号分割的多关键字支持
                separator: ",",          //多关键字支持时的分隔符，默认为空格
                getDataMethod: "url",    //获取数据的方式，总是从 URL 获取
                effectiveFields: ["code", "name", "typeName"],
                effectiveFieldsAlias: {code: "编码", name: "名称", typeName: "类型"},
                idField: 'id',
                keyField: 'code',
                url: (yams.context.contextPath == '/' ? '' : yams.context.contextPath) + '/api/ffd4a6ca2c74462ba709ce09d61071db', //优先从url ajax 请求 json 帮助数据，注意最后一个参数为关键字请求参数
                listStyle: {
                    'padding-top': 0,
                    'max-height': '425px',
                    'max-width': '800px',
                    'overflow': 'auto',
                    'border': '1px solid',
                    'width': 'auto',
                    'transition': '0.3s',
                    '-webkit-transition': '0.3s',
                    '-moz-transition': '0.3s',
                    '-o-transition': '0.3s'
                },                              //列表的样式控制
                listAlign: 'left',              //提示列表对齐位置，left/right/auto
                listHoverStyle: 'background: #07d; color:#000', //提示框列表鼠标悬浮的样式
                fnAdjustAjaxParam: function (keyword, opt) {
                    return {
                        type: 'POST',
                        data: {
                            houseId: '-1',
                            code: keyword
                        },
                        dataType: 'json',
                        beforeSend: function (XMLHttpRequest) {
                            let requestHeaderParam = Dolphin.defaults.ajax.requestHeader;
                            for (let key in requestHeaderParam) {
                                XMLHttpRequest.setRequestHeader(key, requestHeaderParam[key]);
                            }
                        }
                    };
                },
                fnProcessData: function (json) {    // url 获取数据时，对数据的处理，作为 fnGetData 的回调函数
                    let index, len, data = {value: []};
                    data.value = json.rows;
                    //字符串转化为 js 对象
                    return data;
                }
            }).on('onDataRequestSuccess', function (e, result) {
                console.log('--', result);
                if (result.total == 0) {
                    console.log($(this).parent());
                }
            }).on('onSetSelectValue', function (e, keyword, data) {

            }).on('onUnsetSelectValue', function () {
            });
        },

        /**
         * 一些页面DOM对象和事件的销毁
         */
        destroy: function () {
        }
    };
});