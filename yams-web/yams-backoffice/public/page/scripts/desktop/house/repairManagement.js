$(function () {
    yams.menu.select('repairManagement');

    yams.page = {
        /**
         * 初始化页面
         */
        init: function () {
            Dolphin.form.parse();
            this.initDataList();
            this.initEvent();
            $(".btn-query").click();
        },
        /**
         * 初始化页面事件
         */
        initEvent: function () {
            let me = this;

            $(".btn-query").click(function () {
                me._dataList.load(null,Dolphin.form.getValue('.query-form'))
            });

            $(".modelSave").click(function () {
                let ef = $(".edit-form");
                if (Dolphin.form.validate(ef)) {
                    let data = Dolphin.form.getValue(ef, '"');
                    Dolphin.ajax({
                        url: '/api/0aac1b853c024d4c9dddae858934a38d',
                        type: Dolphin.requestMethod.POST,
                        data: Dolphin.json2string(data),
                        onSuccess: function (reData) {
                            me._dataList.load();
                            $('#dataModal').modal('hide');
                        }
                    });
                }
            });
        },

        initDataList: function () {
            let me = this;
            me._dataList = new Dolphin.LIST({
                panel: "#dataList",
                title:'报修列表',
                idField: 'id',
                columns: [{
                    code: 'issueTime',
                    title: '报修时间',
                    width: '160px',
                },{
                    code: 'statusName',
                    title: '当前状态',
                    width: '130px'
                }, {
                    code: 'type',
                    title: '报修类型',
                    width: '90px'
                }, {
                    code:'address',
                    title:'地址',
                    width:'160px'
                },{
                    code: 'result',
                    title: '处理结果',
                    formatter:function (val) {
                        return val || '-';
                    }
                }, {
                    code: 'handleTime',
                    title: '处理时间',
                    width: '160px',
                    formatter:function (val) {
                        return val || '-';
                    }
                }, {
                    code: 'handler',
                    title: '处理人',
                    width: '130px',
                    formatter:function (val) {
                        return val || '-';
                    }
                },{
                    code: 'id',
                    title: '&nbsp;',
                    width: '110px',
                    formatter: function (val, row, index) {
                        return yams.buttons.edit({
                            id: row.id
                        });
                    }
                }],
                multiple: true,
                rowIndex: true,
                checkbox: true,
                ajaxType: Dolphin.requestMethod.POST,
                url: '/api/1703c6d52d86434fb88314d910fcf5c1',
                data:{rows:[]},
                queryParams:{},
                pagination: true,
                onLoadSuccess: function () {
                    yams.buttons.editCallback('170448e0874143b392e55a9925d9638c', 'id', function (data) {
                        console.log(data.value.medias);
                        if(data.value.medias){
                            let html = [];
                            for(let i=0;i<data.value.medias.length;i++){
                                if(data.value.medias[i]){
                                    html.push('<div class="col-md-6"><div class="thumbnail"><img src="/uploadFiles/repair/'+data.value.medias[i]+'.jpg"></div></div>');
                                }
                            }
                            $("#uploadImage").append(html.join(''));
                        }
                        $('#dataModal').modal('show');
                    });
                }
            });
        },

        /**
         * 一些页面DOM对象和事件的销毁
         */
        destroy: function () {
        }
    };
});