$(function () {
    yams.menu.select('houseManagement');

    yams.page = {
        /**
         * 初始化页面
         */
        init: function () {
            this.initDataList();
            this.initEvent();
        },
        /**
         * 初始化页面事件
         */
        initEvent: function () {
            var me = this;

            $(".newData").click(function () {
                Dolphin.form.empty('.edit-form');
                $('#dataModal').modal('show');
            });

            $(".modelSave").click(function () {
                var ef = $(".edit-form");
                if (Dolphin.form.validate(ef)) {
                    var data = Dolphin.form.getValue(ef, '"');
                    Dolphin.ajax({
                        url: '/api/9651431c68a04d54a22f1e2e7faf827b',
                        type: Dolphin.requestMethod.PUT,
                        data: Dolphin.json2string(data),
                        onSuccess: function (reData) {
                            me._dataList.load();
                            $('#dataModal').modal('hide');
                        }
                    });

                }
            });
        },

        initDataList: function () {
            var me = this;
            me._dataList = new Dolphin.LIST({
                panel: "#dataList",
                title:'房屋列表',
                idField: 'id',
                columns: [{
                    code: 'name',
                    title: '房屋名称'
                }, {
                    code: 'code',
                    title: '房屋编码',
                    width: '150px'
                }, {
                    code: 'status',
                    title: '房屋状态',
                    width: '130px',
                    formatter: function (val) {
                        return val ? '正常' : '<span style="color: red;">禁用</span>';
                    }
                }, {
                    code: 'floor',
                    title: '所在楼层'
                }, {
                    code: 'houseType',
                    title: '房屋类型'
                }, {
                    code: 'area',
                    title: '房屋面积'
                }, {
                    code: 'monthly',
                    title: '月度租金'
                },{
                    code: 'recommend',
                    title: '推荐房源'
                }, {
                    code: 'id',
                    title: '&nbsp;',
                    width: '180px',
                    formatter: function (val, row, index) {
                        return yams.buttons.edit({
                            id: row.id
                        });
                    }
                }],
                multiple: true,
                rowIndex: true,
                checkbox: true,
                ajaxType: Dolphin.requestMethod.POST,
                url: '/api/9651431c68a04d54a22f1e2e7faf827b',
                pagination: true,
                onLoadSuccess: function () {
                    yams.buttons.editCallback('319bc41c5fe44dfc9f396b5ace5faf4a', 'id', function (data) {
                        $('#dataModal').modal('show');
                    });
                }
            });
        },

        /**
         * 一些页面DOM对象和事件的销毁
         */
        destroy: function () {
        }
    };
});