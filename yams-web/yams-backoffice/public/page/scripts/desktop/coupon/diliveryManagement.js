'use strict';
$(function () {
    yams.menu.select('userManagement');

    yams.page = {
        /**
         * 初始化页面
         */
        init: function () {
            this.initDataList();
            this.initCouponModelList();
            this.initEvent();
        },
        /**
         * 初始化页面事件
         */
        initEvent: function () {
            let me = this;

            $(".patchGenerate").click(function () {
                // Dolphin.form.empty('.edit-form');
                $('#couponModelModal').modal('show');
            });
            $("#queryCouponModel").click(function () {
                let d = Dolphin.form.getValue('.query-coupon-form');
                me._couponModelList.query(d);
            });
            $("#patchUpdateNum").blur(function(){
                var num = $(this).val();
                $(".couponCnt").each(function(){
                    $(this).val(num);
                })
            });

            //用户确定
            $(".confirmCouponModel").click(function(){

                Dolphin.confirm('确认生成这些券吗？', {
                    callback: function (flag) {
                        if (flag) {
                            let couponArray = me._couponModelList.getChecked();
                            var newCouponArray = $.map(couponArray,function(n){

                                var id = n.id;
                                $(".couponCnt").each(function(){

                                    if($(this).data("id") == id){
                                        n.properties.couponCnt = $(this).val();
                                    }

                                });
                                return n;
                            });
                            console.log(Dolphin.json2string(newCouponArray));
                            var paramData = {};
                            paramData.properties = { "paramData" : newCouponArray};
                            Dolphin.ajax({
                                url: '/api/972eb264bcae478c9cbf182cef0aaa1d',
                                type: Dolphin.requestMethod.PUT,
                                data: Dolphin.json2string(paramData),
                                onSuccess: function (reData) {
                                    $('#couponModelModal').modal('hide');
                                    // me._dataList.reload();
                                }
                            });
                        }
                    }
                })
            });


            $(".modelSave").click(function () {
                let ef = $(".edit-form");
                if (Dolphin.form.validate(ef)) {
                    let data = Dolphin.form.getValue(ef, '"');
                    Dolphin.ajax({
                        url: '/api/8f7def63cc5845468b2c65a69abb0664',
                        type: Dolphin.requestMethod.PUT,
                        data: Dolphin.json2string(data),
                        onSuccess: function (reData) {
                            me._dataList.load();
                            $('#dataModal').modal('hide');
                        }
                    });
                }
            });

            $(".btn-query").click(function () {
                let d = Dolphin.form.getValue('.query-form');
                me._dataList.query(d);
            });
        },

        initDataList: function () {
            let me = this;
            me._dataList = new Dolphin.LIST({
                panel: "#dataList",
                title:'会员列表',
                idField: 'id',
                columns: [{
                    code: 'name',
                    title: '会员名称'
                }, {
                    code: 'code',
                    title: '会员编码'
                }, {
                    code: 'phone',
                    title: '联系方式'
                }, {
                    code: 'idCard',
                    title: '身份证号'
                }, {
                    code: 'sex',
                    title: '性别',
                    formatter: function (val) {
                        return val;
                    }
                }, {
                    code: 'id',
                    title: '&nbsp;',
                    width: '90px',
                    formatter: function (val, row, index) {
                        return yams.buttons.edit({
                            id: row.id
                        });
                            //+'<a class="btn btn-outline btn-circle btn-sm '+(row.idCardPhoto?'blue':'red')+' idCartBtn" data-id="'+val+'" href="javascript:;"><i class="fa fa-upload"></i>身份证上传</a>';
                    }
                }],
                multiple: true,
                rowIndex: true,
                checkbox: true,
                ajaxType: Dolphin.requestMethod.POST,
                url: '/api/8f7def63cc5845468b2c65a69abb0664',
                pagination: true,
                onLoadSuccess: function () {
                    yams.buttons.editCallback('0d644ca2f7394092b84a1fb0c7aa0821', 'id', function (reData) {
                        var publicContextPath = $("#publicContextPath").val() == '/' ? '' : $("#publicContextPath").val();

                        if(reData.value.icFrontImg != '' && reData.value.icFrontImg != null){
                            var flink = '';
                            flink = reData.value.icFrontImg;
                            var fp = $('.fimgupload').closest('.add-image');
                            fp.empty();
                            $(".fjs-remove-sku-atom").show();
                            $('<img style="width : 100px ; height : 80px ;" src="'+flink+'" />')
                                .click(function(){
                                    $("#showBigImg").attr("src", flink);
                                    $('#myModal').modal('show');
                                })
                                .appendTo(fp);
                        }
                        if(reData.value.icBackImg != '' && reData.value.icBackImg != null){
                            var blink = '';
                            blink = reData.value.icBackImg;
                            var bp = $('.bimgupload').closest('.add-image');
                            bp.empty();
                            $(".bjs-remove-sku-atom").show();
                            $('<img style="width : 100px ; height : 80px ;" src="'+blink+'" />')
                                .click(function(){
                                    $("#showBigImg").attr("src", blink);
                                    $('#myModal').modal('show');
                                })
                                .appendTo(bp);
                        }
                        if(reData.value.avatarImg != '' && reData.value.avatarImg != null){
                            var alink = '';
                            alink = reData.value.avatarImg;
                            var ap = $('.aimgupload').closest('.add-image');
                            ap.empty();
                            $(".ajs-remove-sku-atom").show();
                            $('<img style="width : 100px ; height : 80px ;" src="'+alink+'" />')
                                .click(function(){
                                    $("#showBigImg").attr("src", alink);
                                    $('#myModal').modal('show');
                                })
                                .appendTo(ap);
                        }
                        $('#dataModal').modal('show');
                    });

                    yams.buttons.delCallback('0d644ca2f7394092b84a1fb0c7aa0821',function () {
                        me._dataList.reload();
                    });
                    $(".idCartBtn").click(function () {
                        let id = $(this).data('id');
                        Dolphin.ajax({
                            url: '/api/0d644ca2f7394092b84a1fb0c7aa0821@id=' + id,
                            type: Dolphin.requestMethod.GET,
                            onSuccess: function (reData) {

                                if(reData && reData.value && reData.value['idCardPhoto']) {
                                    yams.page.um.setContent(reData.value['idCardPhoto']);
                                } else {
                                    yams.page.um.setContent("");
                                }
                                $(".modelUpload").data('id',id);
                                $('#idCardDataModal').modal('show');
                            }
                        });


                    });
                }
            });
        },

        //券类型
        initCouponModelList: function () {
            let me = this;
            me._couponModelList = new Dolphin.LIST({
                panel: "#couponModelList",
                idField: 'id',
                columns: [{
                    code: 'name',
                    title: '券名称'
                },  {
                    code: 'couponType',
                    title: '券类型',
                    formatter: function(val, row, index){
                        return Dolphin.enum.getEnumText("couponType", val);
                    }
                },{
                    code: 'couponVal',
                    title: '券优惠',
                    formatter: function(val, row, index){
                        if(row.couponType == 'CT01'){
                            return val + '元';
                        }else if(row.couponType == 'CT02'){
                            return val + '%';
                        }else{
                            return '无';
                        }
                    }
                }, {
                    code : 'startDate',
                    title : '有效开始时间',
                    formatter : function(val, row, index){
                        return val.substr(0,10);
                    }
                },{
                    code : 'endDate',
                    title : '有效截至时间',
                    formatter : function(val, row, index){
                        return val.substr(0,10);
                    }
                }, {
                    code: 'id',
                    title: '券数量<input type="text" id="patchUpdateNum" style="width:50px;" />',
                    width: '90px',
                    formatter: function (val, row, index) {
                        return '<input type="text" data-id="'+val+'" class="couponCnt" />';
                    }
                }],
                multiple: true,
                rowIndex: true,
                checkbox: true,
                pagination: false,
                ajaxType: Dolphin.requestMethod.POST,
                url: '/api/0321a98b44054407b45038df79e4eaa2',
                onLoadSuccess: function () {

                }
            });
        },

        /**
         * 一些页面DOM对象和事件的销毁
         */
        destroy: function () {
        }
    };
});