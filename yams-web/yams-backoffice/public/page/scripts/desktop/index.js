/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved. 
 */

$(function () {
    yams.menu.select('index');

    yams.page = {
        /**
         * 初始化页面
         */
        init: function () {
            this.initEvent();
            this.initData();
        },
        /**
         * 初始化页面事件
         */
        initEvent: function () {
            let me = this;
        },

        initData:function () {
            Dolphin.ajax({
                url: '/api/561d201614c1417696c739c3bb1870ad',
                data: Dolphin.json2string({}),
                type: Dolphin.requestMethod.POST,
                onSuccess: function (reData) {
                    let da = reData.value || {};
                    for(let k in da){
                        $("#"+k).html(da[k]);
                    }
                }
            });
        },

        /**
         * 一些页面DOM对象和事件的销毁
         */
        destroy: function () {
        }
    };
});