"use strict";
$(function () {
    yams.menu.select('menuManagement');

    yams.page = {
        /**
         * 初始化页面
         */
        init: function () {
            this.fetchMenu();
            this.initEvent();
        },
        /**
         * 初始化页面事件
         */
        initEvent: function () {
            let me = this;

            $('.newWechatMenu').click(function () {
                let menu = me.getValue();
                Dolphin.ajax({
                    url: '/wexin/menu',
                    type: Dolphin.requestMethod.POST,
                    data: Dolphin.json2string(menu),
                    onSuccess: function (reData) {
                        Dolphin.alert('保存成功', {
                            callback: function () {
                                location.reload();
                            }
                        })
                    }
                })
            });
        },

        setValue: function (data) {
            $("#msgShow").hide();
            $("#menuTable").show().find('input').val('');
            let i, j;
            for (i = 0; i < data.length; i++) {
                if(data[i].sub_button.length > 0){
                    $('input[menu_level_1="' + i + '"][name="_name"]').val(data[i].name);
                    for (j = 0; j < data[i].sub_button.length; j++) {
                        $('input[menu_level_1="' + i + '"][menu_level_2="' + j + '"][name="name"]').val(data[i].sub_button[j].name);
                        $('select[menu_level_1="' + i + '"][menu_level_2="' + j + '"][name="type"]').val(data[i].sub_button[j].type);
                        $('input[menu_level_1="' + i + '"][menu_level_2="' + j + '"][name="content"]').val(data[i].sub_button[j].url);
                    }
                }else{
                    $('input[menu_level_1="' + i + '"][name="_name"]').val('-');
                    $('input[menu_level_1="' + i + '"][menu_level_2="' + 0 + '"][name="name"]').val(data[i].name);
                    $('select[menu_level_1="' + i + '"][menu_level_2="' + 0 + '"][name="type"]').val(data[i].type);
                    $('input[menu_level_1="' + i + '"][menu_level_2="' + 0 + '"][name="content"]').val(data[i].url);
                }

            }
        },

        getValue: function () {
            let data = [], _data, __data;
            for (let i = 0; i < 3; i++) {
                _data = {};
                _data.name = $('input[menu_level_1="' + i + '"][name="_name"]').val();
                if (_data.name) {
                    _data.sub_button = [];
                    for (let j = 0; j < 5; j++) {
                        __data = {};
                        __data.name = $('input[menu_level_1="' + i + '"][menu_level_2="' + j + '"][name="name"]').val();
                        __data.type = $('select[menu_level_1="' + i + '"][menu_level_2="' + j + '"][name="type"]').val();
                        __data.url = $('input[menu_level_1="' + i + '"][menu_level_2="' + j + '"][name="content"]').val();
                        if (__data.name && __data.type && __data.url) {
                            _data.sub_button.push(__data);
                        }
                    }
                    if (_data.sub_button.length > 0) {
                        if(_data.name == '-'){
                            data.push(_data.sub_button[0]);
                        }else{
                            data.push(_data);
                        }
                    }
                }
            }
            return data;
        },

        fetchMenu: function () {
            let me = this;
            Dolphin.ajax({
                url: '/wexin/menu',
                onSuccess: function (reData) {
                    if(reData.value && reData.value.menu) {
                        me.setValue(reData.value.menu.button);
                    }
                }
            });
        },

        /**
         * 一些页面DOM对象和事件的销毁
         */
        destroy: function () {
        }
    };
});