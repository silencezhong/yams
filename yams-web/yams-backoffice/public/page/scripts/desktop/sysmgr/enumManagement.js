$(function () {
    yams.menu.select('enumManagement');

    yams.page = {
        /**
         * 初始化页面
         */
        init: function () {
            this.initDataList("#enumList");
            this._enumItemList = this.dataItemList('#enumItemList', false);
            this._enumWinList = this.dataItemList('#enum_option_list', true);
            this.initEvent();
        },
        /**
         * 初始化页面事件
         */
        initEvent: function () {
            var me = this;

            $(".newEnum").click(function () {
                Dolphin.form.empty('.edit-form');
                $('#dataModal').modal('show');
            });

            $(".modelSave").click(function () {
                var ef = $(".edit-form");
                if (Dolphin.form.validate(ef)) {
                    var data = Dolphin.form.getValue(ef, '"');
                    Dolphin.ajax({
                        url: '/api/7de6bd4a4b1941698babee70447ada68',
                        type: Dolphin.requestMethod.PUT,
                        data: Dolphin.json2string(data),
                        onSuccess: function (reData) {
                            me._dataList.load();
                            $('#dataModal').modal('hide');
                        }
                    });
                }
            });
        },

        initDataList: function (panelId) {
            var me = this;
            this._dataList = new Dolphin.LIST({
                panel: panelId,
                idField: 'id',
                columns: [{
                    code: 'name',
                    title: '枚举名称',
                    textAlign: 'left'
                }, {
                    code: 'code',
                    title: '枚举编码'
                }, {
                    code: 'status',
                    title: '是否启用'
                }, {
                    code: '',
                    title: '&nbsp;',
                    width: '180px',
                    formatter: function (val, row, index) {
                        return yams.buttons.edit({
                                id: row.id
                            }) + yams.buttons.del({
                                id: row.id
                            });
                    }
                }],
                multiple: false,
                ajaxType: Dolphin.requestMethod.POST,
                url: '/api/7de6bd4a4b1941698babee70447ada68',
                pagination: false,
                onClick: function (data, thisRow, event) {
                    me._enumItemList.loadData({rows: data.items});
                },
                onLoadSuccess: function () {
                    yams.buttons.editCallback('fb941394b425441b858d31a2a75db60e', 'id', function (data) {
                        me._enumWinList.loadData({rows: data.value.items});
                        $('#dataModal').modal('show');
                    });
                    yams.buttons.delCallback('fb941394b425441b858d31a2a75db60e', function () {
                        me._enumList.reload();
                    });
                }
            });
        },

        dataItemList: function (panelId, edit) {
            return new Dolphin.LIST({
                panel: panelId,
                idField: 'pkId',
                columns: [{
                    code: 'id',
                    hidden: true,
                    title: '数据主键'
                }, {
                    code: 'name',
                    title: '项名称'
                }, {
                    code: 'code',
                    title: '项编码'
                }, {
                    code: 'rowNum',
                    title: '排序'
                }],
                multiple: false,
                rowIndex: false,
                checkbox: false,
                editFlag: edit,
                data: {rows: []},
                pagination: false,
                editListName: 'items',
                onClick: function (data, thisRow, event) {

                },
                onRemoveRow: function (data, event, thisRow) {
                    if (thisRow.find('[listName="id"]').val()) {
                        thisRow.remove();
                    }
                }
            });
        },

        /**
         * 一些页面DOM对象和事件的销毁
         */
        destroy: function () {
        }
    };
});