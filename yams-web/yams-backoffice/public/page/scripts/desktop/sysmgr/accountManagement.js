$(function () {
    yams.menu.select('accountManagement');

    yams.page = {
        /**
         * 初始化页面
         */
        init: function () {
            this.initDataList();
            this.initEvent();
        },
        /**
         * 初始化页面事件
         */
        initEvent: function () {
            var me = this;

            $(".newAccount").click(function () {
                Dolphin.form.empty('.edit-form');
                $('#dataModal').modal('show');
            });

            $(".modelSave").click(function () {
                var ef = $(".edit-form");
                if (Dolphin.form.validate(ef)) {
                    var data = Dolphin.form.getValue(ef, '"');
                    Dolphin.ajax({
                        url: '/api/756af3ae31a64565a54ec0221f84d377',
                        type: Dolphin.requestMethod.PUT,
                        data: Dolphin.json2string(data),
                        onSuccess: function (reData) {
                            me._dataList.load();
                            $('#dataModal').modal('hide');
                        }
                    });
                }
            });
            
            $(".restPwd").click(function () {
                var items = me._dataList.getChecked();
                if (items.length > 0) {
                    Dolphin.confirm("确认重置所选择账号的密码?", {
                        callback: function (flg) {
                            if (flg) {
                                var pl = [];
                                for (var i = 0; i < items.length; i++) {
                                    pl.push(items[i].id);
                                }
                                Dolphin.ajax({
                                    url: '/api/15f66c54650c443386878f5b818dc48f',
                                    data: Dolphin.json2string({id: pl.join(',')}),
                                    type: Dolphin.requestMethod.POST,
                                    onSuccess: function (reData) {
                                        $(".btn-query").click();
                                    }
                                });
                            }
                        }
                    });
                }
            });

            $(".btn-query").click(function () {
                me._dataList.load(null,Dolphin.form.getValue('.query-form'))
            });
        },

        initDataList: function () {
            var me = this;
            me._dataList = new Dolphin.LIST({
                panel: "#dataList",
                title:'账号列表',
                idField: 'id',
                columns: [{
                    code: 'name',
                    title: '用户名称'
                }, {
                    code: 'code',
                    title: '用户编码',
                    width: '150px'
                }, {
                    code: 'status',
                    title: '状态',
                    width: '90px',
                    formatter: function (val) {
                        return val ? '正常' : '<span style="color: red;">禁用</span>';
                    }
                }, {
                    code: 'sex',
                    title: '性别',
                    width: '75px',
                    formatter: function (val) {
                        return val == 1 ? '男' : '女';
                    }
                }, {
                    code: 'mobile',
                    title: '手机',
                    width: '150px'
                }, {
                    code: 'email',
                    title: '电子邮件',
                    width: '160px'
                }, {
                    code: 'id',
                    title: '&nbsp;',
                    width: '180px',
                    formatter: function (val, row, index) {
                        return yams.buttons.edit({
                                id: row.id
                            })+yams.buttons.del({id:row.id});
                    }
                }],
                multiple: true,
                rowIndex: true,
                checkbox: true,
                ajaxType: Dolphin.requestMethod.POST,
                url: '/api/756af3ae31a64565a54ec0221f84d377',
                pagination: true,
                onLoadSuccess: function () {
                    yams.buttons.editCallback('a0f1e5cde2084416946ed6c4c2a25da9', 'id', function (data) {
                        $('#dataModal').modal('show');
                    });
                    yams.buttons.delCallback('a0f1e5cde2084416946ed6c4c2a25da9',function (data) {
                        me._dataList.load();
                    });
                }
            });
        },

        /**
         * 一些页面DOM对象和事件的销毁
         */
        destroy: function () {
        }
    };
});