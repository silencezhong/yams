$(function () {
    yams.menu.select('setting');

    yams.page = {
        /**
         * 初始化页面
         */
        init: function () {
            this.initDataList();
            this.initEvent();
        },
        /**
         * 初始化页面事件
         */
        initEvent: function () {
            var me = this;

            $(".newSetting").click(function () {
                $('#dataModal').modal('show');
            });

            $(".modelSave").click(function () {
                var ef = $(".edit-form");
                if (Dolphin.form.validate(ef)) {
                    var data = Dolphin.form.getValue(ef, '"');
                    Dolphin.ajax({
                        url: '/api/39f139515f18427984e8774c59ba727b',
                        type: Dolphin.requestMethod.PUT,
                        data: Dolphin.json2string(data),
                        onSuccess: function (reData) {
                            me._dataList.load();
                            $('#dataModal').modal('hide');
                        }
                    });

                }
            });
        },

        initDataList: function () {
            var me = this;
            me._dataList = new Dolphin.LIST({
                panel: "#dataList",
                title:'设置列表',
                idField: 'id',
                columns: [{
                    code: 'name',
                    title: '配置名称'
                }, {
                    code: 'code',
                    title: '配置编码',
                    width: '150px'
                }, {
                    code: 'mobile',
                    title: '配置值',
                    width: '120px'
                }, {
                    code: 'email',
                    title: '电子邮件',
                    width: '160px'
                }, {
                    code: 'id',
                    title: '&nbsp;',
                    width: '180px',
                    formatter: function (val, row, index) {
                        return org.breezee.buttons.edit({
                            id: row.id
                        });
                    }
                }],
                multiple: true,
                rowIndex: true,
                checkbox: true,
                ajaxType: Dolphin.requestMethod.POST,
                url: '/api/3569ed456e5545d5afeece71d333677a',
                pagination: true,
                onLoadSuccess: function () {
                    yams.buttons.editCallback('25274ec2bcc14367be8e57c1f10a58b1', 'id', function (data) {
                        $('#dataModal').modal('show');
                    });
                }
            });
        },

        /**
         * 一些页面DOM对象和事件的销毁
         */
        destroy: function () {
        }
    };
});