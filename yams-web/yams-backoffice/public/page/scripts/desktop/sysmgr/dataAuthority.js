/**
 * Created by LX on 2017/7/28.
 */
$(function () {
    yams.menu.select('dataAuthority');

    yams.page = {
        /**
         * 初始化页面
         */
        init: function () {
            this.initDataList("#enumList");
            //公寓面板
            this._enumItemList = this.dataItemList('#enumItemList', false);
            //账户面板
            this._enumWinList = this.dataItemList('#enum_option_list', true);
            this.initEvent();
            this.initUserList();
            Dolphin.form.parse();

        },
        /**
         * 初始化页面事件
         */
        initEvent: function () {
            var me = this;

            $(".newEnum").click(function () {
                Dolphin.form.empty('.edit-form');
                $('#dataModal').modal('show');
            });
            //添加用户公寓关联权限
            $(".modelSave").click(function () {
                var selectList=$('#selectHouse').val();
                //把公寓Id放进账户中
                var ef = $(".edit-form");
                if (Dolphin.form.validate(ef)) {
                    var data = Dolphin.form.getValue(ef, '"');
                    data.apartmens = [];
                    selectList.forEach(function (apartmen) {
                        data.apartmens.push({id:apartmen});
                    });
                    console.log(data);
                    //保存用户级管理的公寓
                    Dolphin.ajax({
                        url: '/api/ad758810e22c4726860dd7d663e0da82',
                        type: Dolphin.requestMethod.PUT,
                        data: Dolphin.json2string(data),
                        onSuccess: function (reData) {
                            me._dataList.load();
                            $('#dataModal').modal('hide');
                        }
                    });
                }
            });
            //用户选择器
            $("#selectUser").click(function(){
                $('#userModal').modal('show');
            });

            //用户选择器查询
            $(".btn-query").click(function () {
                me._userList.load(null,Dolphin.form.getValue('.query-user-form'))
            });


            //用户选择器确定
            $(".confirmUser").click(function(){
                let userArray = me._userList.getChecked();
                $("#userId").val(userArray[0].id);
                $("#contactName").val(userArray[0].name);
                $("#contactMobile").val(userArray[0].mobile);
                $('#userModal').modal('hide');
            });
        },

        initDataList: function (panelId) {
            var me = this;
            this._dataList = new Dolphin.LIST({
                panel: panelId,
                idField: 'id',
                columns: [{
                    code: 'id',
                    hidden: true,
                    title: '会员主键',
                },{
                    code: 'name',
                    title: '会员名称',
                    textAlign: 'left'
                }, {
                    code: 'code',
                    title: '会员编码'
                }, {
                    code: 'mobile',
                    title: '联系方式'
                }, {
                    code: '',
                    title: '&nbsp;',
                    width: '180px',
                    formatter: function (val, row, index) {
                        return yams.buttons.edit({
                                id: row.id
                            }) + yams.buttons.del({
                                id: row.id
                            });
                    }
                }],
                multiple: false,
                ajaxType: Dolphin.requestMethod.POST,
                //获得所有账户
                url: '/api/ad758810e22c4726860dd7d663e0da82',
                pagination: false,
                //点击具体一条记录获得所有的公寓(通过id获得所有的房屋信息)
                onClick: function (data, thisRow, event) {
                    Dolphin.ajax({
                        url: '/api/fc8dad71e7004928b91864fa81a9719c',
                        type: Dolphin.requestMethod.POST,
                        data: Dolphin.json2string(data),
                        onSuccess: function (reData) {
                            me._enumItemList.loadData({rows: reData.rows});
                        }
                    });
                 },
                onLoadSuccess: function () {
                    /*编辑和删除*/
                    yams.buttons.editCallback('e1cd8e334e5c4c2eb7fe170eb3d31798', 'id', function (data) {
                        me._enumItemList.loadData({rows: data.value.apartmens});
                        $('#dataModal').modal('show');
                    });
                    yams.buttons.delCallback('e1cd8e334e5c4c2eb7fe170eb3d31798', function () {
                        me._enumList.reload();
                    });
                }
            });
        },

        dataItemList: function (panelId, edit) {
            return new Dolphin.LIST({
                panel: panelId,
                idField: 'pkId',
                columns: [{
                    code: 'id',
                    hidden: true,
                    title: '数据主键'
                }, {
                    code: 'name',
                    title: '公寓名称'
                }, {
                    code: 'code',
                    title: '公寓编码'
                }, {
                    code: 'address',
                    title: '公寓地址'
                }],
                multiple: false,
                rowIndex: false,
                checkbox: false,
                editFlag: edit,
                data: {rows: []},
                pagination: false,
                editListName: 'apartmens',
                onClick: function (data, thisRow, event) {
                    //把索引拼接到id选择器当中
                    $('#houseModal').modal('show');
                },
                onRemoveRow: function (data, event, thisRow) {
                    if (thisRow.find('[listName="id"]').val()) {
                        thisRow.remove();
                    }
                }
            });
        },



        initUserList: function () {
            let me = this;
            me._userList = new Dolphin.LIST({
                panel: "#userList",
                idField: 'id',
                columns: [{
                    code: 'name',
                    title: '会员名称'
                }, {
                    code: 'code',
                    title: '会员编码'
                }, {
                    code: 'mobile',
                    title: '联系方式'
                }],
                multiple: false,
                rowIndex: true,
                checkbox: true,
                pagination: true,
                ajaxType: Dolphin.requestMethod.POST,
                url: '/api/756af3ae31a64565a54ec0221f84d377',
                onLoadSuccess: function () {

                }
            });
        },

        /**
         * 一些页面DOM对象和事件的销毁
         */
        destroy: function () {
        }
    };
});