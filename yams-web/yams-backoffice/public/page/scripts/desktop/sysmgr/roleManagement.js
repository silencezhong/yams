$(function () {
    yams.menu.select('roleManagement');

    yams.page = {
        /**
         * 初始化页面
         */
        init: function () {
            this.initDataList();
            this.initEvent();
        },
        /**
         * 初始化页面事件
         */
        initEvent: function () {
            var me = this;

            $(".newRole").click(function () {
                Dolphin.form.empty('.edit-form');
                $('#dataModal').modal('show');
            });

            $(".modelSave").click(function () {
                var ef = $(".edit-form");
                if (Dolphin.form.validate(ef)) {
                    var data = Dolphin.form.getValue(ef, '"');
                    Dolphin.ajax({
                        url: '/api/94c352955d4047f39c3b8bc6c4a12341',
                        type: Dolphin.requestMethod.PUT,
                        data: Dolphin.json2string(data),
                        onSuccess: function (reData) {
                            me._dataList.load();
                            $('#dataModal').modal('hide');
                        }
                    });

                }
            });
        },

        initDataList: function () {
            var me = this;
            me._dataList = new Dolphin.LIST({
                panel: "#dataList",
                title:'角色列表',
                idField: 'id',
                columns: [{
                    code: 'name',
                    title: '角色名称',
                    width: '120px'
                }, {
                    code: 'code',
                    title: '角色编码',
                    width: '100px'
                }, {
                    code: 'permits',
                    title: '角色权限',
                    className: 'text-overflow',
                    formatter: function (val) {
                        return '<div style="max-width: 350px;overflow: hidden;text-overflow:ellipsis;white-space:nowrap;">' + val + '</div>'
                    }
                }, {
                    code: 'id',
                    title: '&nbsp;',
                    width: '180px',
                    formatter: function (val, row, index) {
                        return yams.buttons.edit({
                            id: row.id
                        })+yams.buttons.del({
                                id: row.id
                            });
                    }
                }],
                multiple: true,
                rowIndex: true,
                checkbox: true,
                ajaxType: Dolphin.requestMethod.POST,
                url: '/api/94c352955d4047f39c3b8bc6c4a12341',
                pagination: true,
                onLoadSuccess: function () {
                    yams.buttons.editCallback('7c744c17b1c5427894f592a5e2cd0b67', 'id', function (data) {
                        $('#dataModal').modal('show');
                    });
                    yams.buttons.delCallback('7c744c17b1c5427894f592a5e2cd0b67',function () {
                        me._dataList.load();
                    })
                }
            });
        },

        /**
         * 一些页面DOM对象和事件的销毁
         */
        destroy: function () {
        }
    };
});