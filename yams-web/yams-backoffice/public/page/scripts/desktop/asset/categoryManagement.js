$(function () {
    yams.menu.select('categoryManagement');

    yams.page = {

        init: function () {
            this.categoryTree();
            this.itemLine();
            this.initEvent();
            this._selectList = this.selectedList('#selectedList');
            this._unSelectList = this.unSelectList('#unselectedList');
        },

        initEvent: function () {
            var me = this;
            $('.add_new').click(function () {
                Dolphin.form.empty('#categoryForm');
                $('#myModal').modal('show');
            });

            $('.add_sub').click(function () {
                Dolphin.form.empty('#categoryForm');
                var checkedData = me._categoryTree.getChecked();
                if (checkedData.length != 1) {
                    Dolphin.alert('请选择一个品类节点。');
                    return;
                }
                Dolphin.form.setValue({
                    parent: {
                        id: checkedData[0].id
                    }
                }, '#categoryForm');
                $('#myModal').modal('show');
            });

            $('.modelSave').click(function () {
                var data = Dolphin.form.getValue('categoryForm', '"');
                if (!data.parent.id)
                    delete data.parent;
                Dolphin.ajax({
                    url: '/api/ccea79179c644a3381f1429c44291797',
                    type: Dolphin.requestMethod.PUT,
                    data: Dolphin.json2string(data),
                    onSuccess: function (reData) {
                        Dolphin.alert('保存成功', {
                            callback: function () {
                                me._categoryTree.reload();
                                $('#myModal').modal('hide');
                            }
                        })
                    }
                });
            });

            /**
             * 选择面板中的按钮
             */
            $('[change]').click(function () {
                var thisButton = $(this), allFlag = !!thisButton.attr('all'),
                    sourceList, targetList,
                    checkedData,
                    i;
                if (thisButton.attr("change") == "select") {
                    sourceList = me._unSelectList;
                    targetList = me._selectList;
                } else {
                    sourceList = me._selectList;
                    targetList = me._unSelectList;
                }
                if (allFlag) {
                    checkedData = [].concat(sourceList.data.rows);
                } else {
                    checkedData = sourceList.getChecked();
                }
                for (i = 0; i < checkedData.length; i++) {
                    if (!checkedData[i].inheritFlag) {
                        sourceList.removeRow(checkedData[i].__id__);
                        targetList.addRowWithData(checkedData[i]);
                    }
                }
            });

            $("#multipleUpdateAttr").click(function () {
                if (!me.selectNode) {
                    alert('请选择一个模型');
                    return;
                }
                $('#attr_relation_win').modal('show');
            });

            $('#attr_relation_win').on('show.bs.modal', function (e) {
                me._selectList.query(null, {
                    cateId: me.selectNode.id
                });
                me._unSelectList.load('/api/918d7c1c91bb4f61824f5371804efdaa');
            });

            $('.relation_submit').click(function () {
                var data = {};
                data.id = me.selectNode.id;
                data.cateAttrs = [];
                var selData = me._selectList.data.rows;
                for (var i = 0; i < selData.length; i++) {
                    data.cateAttrs.push({attribute: {id: selData[i].attrId},display:true,inheritable:true,nullable:true});
                }
                Dolphin.ajax({
                    url: '/api/c9efc201a27e442a9426a1763a9053f1',
                    type: Dolphin.requestMethod.PUT,
                    data: Dolphin.json2string(data),
                    onSuccess: function (reData) {
                        me._itemList.reload();
                        $('#attr_relation_win').modal('hide');
                    }
                });
            });
        },

        categoryTree: function () {
            var me = this;
            this._categoryTree = new Dolphin.TREE({
                defaultId: -1,
                requestKey: 'parentId',
                panel: '#categoryTree',
                url: '/api/ac423e9cff8948e4a56adffd88e36e37@parentId={parentId}',
                multiple: false,
                onChecked: function (data) {
                    me.selectNode = data;
                    me._itemList.query(null, {
                        cateId: data.id
                    });
                },
                onLoad: function () {
                    if (this.data && this.data.length > 0) {
                        var n = this.data[0];
                        this.expend(n);
                    }
                }
            });
        },

        itemLine: function () {
            var me = this;
            this._itemList = new Dolphin.LIST({
                panel: '#itemList',
                url: '/api/047838ba22ff4568ab4b41ab15d7bfc0@cateId={cateId}',
                ajaxType: 'get',
                mockPathData: ['id'],
                data: {rows: [], total: 0},
                pagination: true,
                multiple: false,
                columns: [{
                    code: 'attrCode',
                    title: '属性编码'
                }, {
                    code: 'attrName',
                    title: '属性名称'
                }, {
                    code: 'attrType',
                    title: '字段类型'
                }, {
                    code: 'category.id',
                    title: '来源',
                    formatter: function (val, dat) {
                        if (val == me.selectNode.id) {
                            return "自身";
                        } else {
                            return "父类";
                        }
                    }
                }],
                onLoadSuccess: function (data) {
                },
                onCheck: function (data, row, thisInput) {
                },
                onChecked: function (data, row, thisCheckbox) {
                    me.curSelect = data;
                },
                onUnchecked: function (data) {
                }
            });
        },

        unSelectList: function (panelId) {
            return new Dolphin.LIST({
                panel: panelId,
                data: {rows: [], total: 0},
                title: '未选择列表',
                panelType: 'panel-info',
                ajaxType: 'post',
                pagination: false,
                rowIndex: false,
                columns: [{
                    code: 'code',
                    title: '属性编码'
                }, {
                    code: 'name',
                    title: '属性名称'
                }, {
                    code: 'remark',
                    title: '备注'
                }]
            });
        },

        selectedList: function (panelId) {
            return new Dolphin.LIST({
                panel: panelId,
                data: {rows: [], total: 0},
                url: '/api/047838ba22ff4568ab4b41ab15d7bfc0@cateId={cateId}',
                title: '已选择列表',
                panelType: 'panel-warning',
                pagination: false,
                idField: 'attrId',
                rowIndex: false,
                columns: [{
                    code: 'attrCode',
                    title: '属性编码'
                }, {
                    code: 'attrName',
                    title: '属性名称'
                }]
            });
        }
    };
});