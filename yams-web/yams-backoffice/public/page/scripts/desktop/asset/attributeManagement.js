$(function () {
    yams.menu.select('attributeManagement');

    yams.page = {
        /**
         * 初始化页面
         */
        init: function () {
            this.initDataList();
            this.initEvent();
        },
        /**
         * 初始化页面事件
         */
        initEvent: function () {
            let me = this;

            $(".newData").click(function () {
                Dolphin.form.empty('.edit-form');
                $('#dataModal').modal('show');
            });

            $(".modelSave").click(function () {
                let ef = $(".edit-form");
                if (Dolphin.form.validate(ef)) {
                    let data = Dolphin.form.getValue(ef, '"');
                    Dolphin.ajax({
                        url: '/api/918d7c1c91bb4f61824f5371804efdaa',
                        type: Dolphin.requestMethod.PUT,
                        data: Dolphin.json2string(data),
                        onSuccess: function (reData) {
                            me._dataList.load();
                            $('#dataModal').modal('hide');
                        }
                    });

                }
            });

            $("#field-Type").change(function () {
                let selVal = $(this).children('option:selected').val();
                let html = "";
                switch (selVal.toLowerCase()) {
                    case 'enum':
                        html = $('<hr/><div class="form-group"><label class="col-md-offset-1 col-md-2 control-label">枚举编码</label>' +
                            '<div class="col-md-6"><input type="text" class="form-control"  name="argumentsJson.enumCode" dol-validate="required"></div></div>');
                        break;
                    case 'integer':
                    case 'number':
                        html = $('<hr/><div class="form-group"><label class="col-md-offset-1 col-md-2 control-label">单位</label>' +
                            '<div class="col-md-6"><input type="text" class="form-control"  name="unitCode"></div>' +
                            '</div>');
                        break;
                    case 'string':
                        html = $('<hr/><div class="form-group"><label class="col-md-offset-1 col-md-2 control-label">文本类型</label>' +
                            '<div class="col-md-6"><select class="form-control" name="argumentsJson.textType" dol-validate="required"><option value="input" selected>单行文本</option><option value="textArea">多行文本</option></select>' +
                            '</div></div>');
                        break;
                    default:
                        break;
                }
                $("#extendInfo").show().empty().append(html);
            });

            $(".btn-query").click(function () {
                me._dataList.load(null, Dolphin.form.getValue('.query-form'))
            });
        },

        initDataList: function () {
            let me = this;
            me._dataList = new Dolphin.LIST({
                panel: "#dataList",
                title: '属性列表',
                idField: 'id',
                columns: [{
                    code: 'code',
                    title: '属性编码'
                }, {
                    code: 'name',
                    title: '属性名称'
                }, {
                    code: 'typeName',
                    title: '字段类型'
                }, {
                    code: 'unitCode',
                    title: '单位'
                }, {
                    code: 'arguments',
                    title: '参数',
                    formatter: function (val) {
                        if(val) {
                            return Dolphin.json2string(val);
                        }
                        return "";
                    }
                }, {
                    code: 'id',
                    title: '&nbsp;',
                    width: '180px',
                    formatter: function (val, row, index) {
                        return yams.buttons.edit({
                                id: row.id
                            }) + yams.buttons.del({id: row.id});
                    }
                }],
                multiple: true,
                rowIndex: true,
                checkbox: true,
                ajaxType: Dolphin.requestMethod.POST,
                url: '/api/918d7c1c91bb4f61824f5371804efdaa',
                pagination: true,
                onLoadSuccess: function () {
                    yams.buttons.editCallback('6bb959c082f44f448b7810913601f257', 'id', function (data) {
                        $('#dataModal').modal('show');
                        $("#field-Type").change();
                        Dolphin.form.setValue(data.value, '.edit-form');
                    });
                    yams.buttons.delCallback('6bb959c082f44f448b7810913601f257', function () {
                        me._dataList.load();
                    })
                }
            });
        },

        /**
         * 一些页面DOM对象和事件的销毁
         */
        destroy: function () {
        }
    };
});