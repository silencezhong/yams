'use strict';
$(function () {
    yams.menu.select('userManagement');

    yams.page = {
        /**
         * 初始化页面
         */
        init: function () {
            Dolphin.form.parse();
            this.initDataList();
            this.initEvent();

        },
        /**
         * 初始化页面事件
         */
        initEvent: function () {
            let me = this;
            $(".js-example-basic-single").select2();
            //上传图片碰到个问题 如果用同一个class每次上传图片顺序不对 所以暂时分开单独写两个class 但里面的代码其实是一样的
            var publicContextPath = $("#publicContextPath").val() == '/' ? '' : $("#publicContextPath").val();
            $('.fimgupload').fileupload({
                url: publicContextPath + '/file/idCard/',
                dataType: 'json',
                done: function (e, data) {
                    var p = $(e.target).closest('.add-image');
                    console.log(data.result.url);
                    var imgUrl = "/uploadFiles"+data.result.url;
                    p.empty();
                    $(".fjs-remove-sku-atom").show();
                    $('<img style="width : 100px ; height : 80px ;" src="'+imgUrl+'" />')
                        .click(function(){
                            $("#showBigImg").attr("src", imgUrl);
                            $('#myModal').modal('show');
                        })
                        .appendTo(p);

                    $("#icFrontImg").val(data.result.url);

                },
                progressall: function (e, data) {
                }
            });


            $('.bimgupload').fileupload({
                url: publicContextPath + '/file/idCard/',
                dataType: 'json',
                done: function (e, data) {
                    var p = $(e.target).closest('.add-image');
                    var imgUrl = "/uploadFiles"+data.result.url;
                    p.empty();
                    $(".bjs-remove-sku-atom").show();
                    $('<img style="width : 100px ; height : 80px ;" src="'+imgUrl+'" />')
                        .click(function(){
                            $("#showBigImg").attr("src", imgUrl);
                            $('#myModal').modal('show');
                        })
                        .appendTo(p);
                    $("#icBackImg").val(data.result.url);

                },
                progressall: function (e, data) {
                }
            });
            $('.aimgupload').fileupload({
                url: publicContextPath + '/file/idCard/',
                dataType: 'json',
                done: function (e, data) {
                    var p = $(e.target).closest('.add-image');
                    var imgUrl = "/uploadFiles"+data.result.url;
                    p.empty();
                    $(".ajs-remove-sku-atom").show();
                    $('<img style="width : 100px ; height : 80px ;" src="'+imgUrl+'" />')
                        .click(function(){
                            $("#showBigImg").attr("src", imgUrl);
                            $('#myModal').modal('show');
                        })
                        .appendTo(p);
                    $("#avatarImg").val(data.result.url);

                },
                progressall: function (e, data) {
                }
            });
            //删除图片
            $(".fjs-remove-sku-atom").click(function(){
                var panel = $(this).next().find('.add-image').empty().html('+ ');
                $("#icFrontImg").val("");
                $('<input style="position:absolute;width:300px;top:0;right:0;margin:0;opacity:0;cursor:pointer;"class="fimgupload"  type="file" name="upfile" multiple>')
                    .appendTo(panel).fileupload({
                    url: publicContextPath + '/file/idCard/',
                    dataType: 'json',
                    done: function (e, data) {
                        var p = $(e.target).closest('.add-image');
                        var imgUrl = "/uploadFiles"+data.result.url;
                        p.empty();
                        $(".fjs-remove-sku-atom").show();
                        $('<img style="width : 100px ; height : 80px ;" src="'+imgUrl+'" />')
                            .click(function(){
                                $("#showBigImg").attr("src", imgUrl);
                                $('#myModal').modal('show');
                            })
                            .appendTo(p);
                        $("#icFrontImg").val(data.result.url);
                    },
                    progressall: function (e, data) {
                    }
                });
                $(".fjs-remove-sku-atom").hide();
            });
            $(".bjs-remove-sku-atom").click(function(){
                var panel = $(this).next().find('.add-image').empty().html('+ ');
                $("#icBackImg").val("");
                $('<input style="position:absolute;width:300px;padding-left:100px;left:0;top:0;right:0;margin:0;opacity:0;cursor:pointer;" class="bimgupload"  type="file" name="upfile" multiple>')
                    .appendTo(panel).fileupload({
                    url: publicContextPath + '/file/idCard/',
                    dataType: 'json',
                    done: function (e, data) {
                        var p = $(e.target).closest('.add-image');
                        var imgUrl = "/uploadFiles"+data.result.url;
                        p.empty();
                        $(".bjs-remove-sku-atom").show();
                        $('<img style="width : 100px ; height : 80px ;" src="'+imgUrl+'" />')
                            .click(function(){
                                $("#showBigImg").attr("src", imgUrl);
                                $('#myModal').modal('show');
                            })
                            .appendTo(p);
                        $("#icBackImg").val(data.result.url);
                    },
                    progressall: function (e, data) {
                    }
                });
                $(".bjs-remove-sku-atom").hide();
            });
            $(".ajs-remove-sku-atom").click(function(){
                var panel = $(this).next().find('.add-image').empty().html('+ ');
                $("#avatarImg").val("");
                $('<input style="position:absolute;width:300px;top:0;right:0;margin:0;opacity:0;cursor:pointer;"class="aimgupload"  type="file" name="upfile" multiple>')
                    .appendTo(panel).fileupload({
                    url: publicContextPath + '/file/idCard/',
                    dataType: 'json',
                    done: function (e, data) {
                        var p = $(e.target).closest('.add-image');
                        var imgUrl = "/uploadFiles"+data.result.url;
                        p.empty();
                        $(".ajs-remove-sku-atom").show();
                        $('<img style="width : 100px ; height : 80px ;" src="'+imgUrl+'" />')
                            .click(function(){
                                $("#showBigImg").attr("src", imgUrl);
                                $('#myModal').modal('show');
                            })
                            .appendTo(p);
                        $("#avatarImg").val(data.result.url);
                    },
                    progressall: function (e, data) {
                    }
                });
                $(".ajs-remove-sku-atom").hide();
            });


            $(".newData").click(function () {
                Dolphin.form.empty('.edit-form');
                $(".fjs-remove-sku-atom").click();
                $(".bjs-remove-sku-atom").click();
                $('#dataModal').modal('show');
                $("#checkPhone").blur(function () {
                    var phones = $.trim($("#checkPhone").val());
                    var isMobile=/^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
                    if(!phones){
                        $('.tel-msg').text('不能为空');
                        return false;
                    }else if (!isMobile.test(phones)) {
                        $('.tel-msg').text('无效号码');
                        return false;
                    }else {
                        $('.tel-msg').text('填写完成');
                       /* Dolphin.ajax({
                            url : '/api/8f7def63cc5845468b2c65a69abb0664',
                            type: Dolphin.requestMethod.PUT,
                            //dataType:"JSON",
                            data:{
                                phone: phones,
                            },
                            onSuccess:function(reData){
                                debugger;
                                console.log(data);
                                $('.tel-msg').text("OK");
                            },
                            error:function(){
                                $('.tel-msg').text('请重新输入');
                            }
                        });*/
                    }

                });
            });

            $(".modelSave").click(function () {
                let ef = $(".edit-form");
                if (Dolphin.form.validate(ef)) {
                    let data = Dolphin.form.getValue(ef, '"');
                    Dolphin.ajax({
                        url: '/api/8f7def63cc5845468b2c65a69abb0664',
                        type: Dolphin.requestMethod.PUT,
                        data: Dolphin.json2string(data),
                        onSuccess: function (reData) {
                            me._dataList.load();
                            $('#dataModal').modal('hide');
                        }
                    });

                }
            });

            $(".modelUpload").click(function () {
                let data = {id:$(this).data('id')};
                data['idCardPhoto'] = yams.page.um.getContent();
                Dolphin.ajax({
                    url: '/api/c51e664ff2e3437bbf26d74b03f8ab60',
                    type: Dolphin.requestMethod.POST,
                    data: Dolphin.json2string(data),
                    onSuccess: function (reData) {
                        $('#idCardDataModal').modal('hide');
                    }
                });
            });
            
            $(".btn-query").click(function () {
                let d = Dolphin.form.getValue('.query-form');
                console.log(d);
                me._dataList.query(d);
            });
        },

        initDataList: function () {
            let me = this;
            me._dataList = new Dolphin.LIST({
                panel: "#dataList",
                title:'会员列表',
                idField: 'id',
                columns: [{
                    code: 'name',
                    title: '会员名称'
                }, {
                    code: 'code',
                    title: '会员编码'
                }, {
                    code: 'phone',
                    title: '联系方式'
                }, {
                    code: 'idCard',
                    title: '身份证号'
                }, {
                    code: 'sex',
                    title: '性别',
                    formatter: function (val) {
                        return val;
                    }
                },{
                    code: 'nickname',
                    title: '微信昵称'

                }, {
                    code: 'id',
                    title: '&nbsp;',
                    width: '90px',
                    formatter: function (val, row, index) {
                        return yams.buttons.edit({
                            id: row.id
                        });
                            //+'<a class="btn btn-outline btn-circle btn-sm '+(row.idCardPhoto?'blue':'red')+' idCartBtn" data-id="'+val+'" href="javascript:;"><i class="fa fa-upload"></i>身份证上传</a>';
                    }
                }],
                multiple: true,
                rowIndex: true,
                checkbox: true,
                ajaxType: Dolphin.requestMethod.POST,
                url: '/api/8f7def63cc5845468b2c65a69abb0664',
                pagination: true,
                onLoadSuccess: function () {

                    console.log('editCallback');

                    yams.buttons.editCallback('0d644ca2f7394092b84a1fb0c7aa0821', 'id', function (reData) {
                        var publicContextPath = $("#publicContextPath").val() == '/' ? '' : $("#publicContextPath").val();
                        if(reData.value.icFrontImg != '' && reData.value.icFrontImg != null){
                            var flink = '/uploadFiles' + reData.value.icFrontImg;
                            var fp = $('.fimgupload').closest('.add-image');
                            fp.empty();
                            $(".fjs-remove-sku-atom").show();
                            $('<img style="width : 100px ; height : 80px ;" src="'+flink+'" />')
                                .click(function(){
                                    $("#showBigImg").attr("src", flink);
                                    $('#myModal').modal('show');
                                })
                                .appendTo(fp);
                        }
                        if(reData.value.icBackImg != '' && reData.value.icBackImg != null){
                            var blink = '/uploadFiles';
                            blink += reData.value.icBackImg;
                            var bp = $('.bimgupload').closest('.add-image');
                            bp.empty();
                            $(".bjs-remove-sku-atom").show();
                            $('<img style="width : 100px ; height : 80px ;" src="'+blink+'" />')
                                .click(function(){
                                    $("#showBigImg").attr("src", blink);
                                    $('#myModal').modal('show');
                                })
                                .appendTo(bp);
                        }
                        if(reData.value.avatarImg != '' && reData.value.avatarImg != null){
                            var alink = '/uploadFiles';
                            alink += reData.value.avatarImg;
                            var ap = $('.aimgupload').closest('.add-image');
                            ap.empty();
                            $(".ajs-remove-sku-atom").show();
                            $('<img style="width : 100px ; height : 80px ;" src="'+alink+'" />')
                                .click(function(){
                                    $("#showBigImg").attr("src", alink);
                                    $('#myModal').modal('show');
                                })
                                .appendTo(ap);
                        }
                        $('#dataModal').modal('show');
                    });

                    yams.buttons.delCallback('0d644ca2f7394092b84a1fb0c7aa0821',function () {
                        me._dataList.reload();
                    });
                    $(".idCartBtn").click(function () {
                        let id = $(this).data('id');
                        Dolphin.ajax({
                            url: '/api/0d644ca2f7394092b84a1fb0c7aa0821@id=' + id,
                            type: Dolphin.requestMethod.GET,
                            onSuccess: function (reData) {

                                if(reData && reData.value && reData.value['idCardPhoto']) {
                                    yams.page.um.setContent(reData.value['idCardPhoto']);
                                } else {
                                    yams.page.um.setContent("");
                                }
                                $(".modelUpload").data('id',id);
                                $('#idCardDataModal').modal('show');
                            }
                        });


                    });
                }
            });
        },

        /**
         * 一些页面DOM对象和事件的销毁
         */
        destroy: function () {
        }
    };

});