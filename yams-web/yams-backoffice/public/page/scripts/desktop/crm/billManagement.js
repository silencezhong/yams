$(function () {
    yams.menu.select('billManagement');

    yams.page = {
        /**
         * 初始化页面
         */
        init: function () {
            Dolphin.form.parse();
            this.initDataList();
            this.initUserList();
            this.initHouseList();
            this.initEvent();

            if($("#fromFlag").val() != null
                && $("#fromFlag").val() != undefined
                && $("#fromFlag").val() == 'todayIncome'){

                const nowStr = Dolphin.date2string(new Date(), "yyyy-MM-dd");
                $("#_startDate").val(nowStr);
                $("#_endDate").val(nowStr);
                $("#status").val("1"); // 已支付

            }

            $(".btn-query").click();
        },
        /**
         * 初始化页面事件
         */
        initEvent: function () {
            var me = this;

            //用户选择器
            $("#selectUser").click(function(){
                $('#userModal').modal('show');
            });
            //用户选择器查询
            $("#queryUser").click(function () {
                let d = Dolphin.form.getValue('.query-user-form');
                me._userList.query(d);
            });
            //用户确定
            $(".confirmUser").click(function(){
                let userArray = me._userList.getChecked();
                $("#userId").val(userArray[0].id);
                $("#contactName").val(userArray[0].name);
                $("#contactMobile").val(userArray[0].phone);
                $('#userModal').modal('hide');
            });

            //房屋选择器
            $("#selectHouse").click(function(){
                $('#houseModal').modal('show');
            });
            //房屋选择器查询
            $("#queryHouse").click(function () {
                let d = Dolphin.form.getValue('.query-house-form');
                me._houseList.query(d);
            });
            //房屋确定
            $(".confirmHouse").click(function(){
                $('#houseModal').modal('hide');
                let houseArray = me._houseList.getChecked();
                $("#houseId").val(houseArray[0].id);
                $("#houseName").val(houseArray[0].name);
            });

            $(".newData").click(function () {
                Dolphin.form.empty('.edit-form-add');
                $('#dataModalAdd').modal('show');
            });

            //创建临时账单
            $(".orderSave").click(function () {
                var ef = $(".edit-form-add");
                if (Dolphin.form.validate(ef)) {
                    var data = Dolphin.form.getValue(ef, '"');
                    console.log(data);
                    Dolphin.ajax({
                        url: '/api/d2b7f740182b4de1b24a2b2644c176b1',
                        type: Dolphin.requestMethod.PUT,
                        data: Dolphin.json2string(data),
                        onSuccess: function (reData) {
                            me._dataList.load('/api/4bd717c1cca5449a8a82c1b1584f87d3');
                            $('#dataModalAdd').modal('hide');
                        }
                    });
                }
            });

            $(".modelSave").click(function () {
                var ef = $(".edit-form");
                if (Dolphin.form.validate(ef)) {
                    var data = Dolphin.form.getValue(ef, '"');
                    Dolphin.ajax({
                        url: '/api/6cf23d5ef2134ed7be73dd2f1aec7365',
                        type: Dolphin.requestMethod.POST,
                        data: Dolphin.json2string(data),
                        onSuccess: function (reData) {
                            me._dataList.load('/api/4bd717c1cca5449a8a82c1b1584f87d3');
                            $('#dataModal').modal('hide');
                        }
                    });

                }
            });

            $(".btn-query").click(function () {
                var d = Dolphin.form.getValue('.query-form');
                // url: '/api/4bd717c1cca5449a8a82c1b1584f87d3',
                me._dataList.load('/api/4bd717c1cca5449a8a82c1b1584f87d3', d);
            });
        },

        initDataList: function () {
            let me = this, STATUS = ['待支付', '已支付','已作废'], TYPES = ['', '合同账单', '水电气账单', '报修账单', '保洁账单','其他账单'];
            me._dataList = new Dolphin.LIST({
                panel: "#dataList",
                title: '账单列表',
                idField: 'id',
                columns: [{
                    code: 'name',
                    title: '账单名称'
                }, {
                    title: '房号',
                    width: '150px',
                    formatter:function (val,row) {
                        return row.properties.house.houseNumber;
                    }
                },{
                    code: 'type',
                    title: '账单类型',
                    width: '150px',
                    formatter: function (val) {
                        return TYPES[val];
                    }
                }, {
                    code: 'createdDate',
                    title: '生成时间',
                }, {
                    code: 'payedDate',
                    title: '支付时间',
                }, {
                    code: 'totalAmount',
                    title: '金额',
                    width: '90px',
                    textAlign: 'right'
                }, {
                    code: 'status',
                    title: '账单状态',
                    width: '160px',
                    textAlign: 'center',
                    formatter: function (val, row) {
                        if (val === 0)
                            return '<span style="color: #ff0000;">' + STATUS[val] + '</span>';
                        else if(val === 1)
                            return '<span style="color: #00a1cb;">' + STATUS[val] + '</span><span>【' + Dolphin.enum.getEnumText('payMethod', row.payMethod) + '】</span>';
                        else
                            return STATUS[val];
                    }
                }, {
                    code: 'id',
                    title: '&nbsp;',
                    width: '180px',
                    formatter: function (val, row, index) {
                        //支付按钮
                        var payBtn = yams.buttons.edit({id: row.id, btnCaption: '支付'});
                        //作废按钮
                        var delBtn = '<a class="btn btn-outline btn-circle btn-sm dark disuseBtn" ' +
                            'data-id="'+val+'" href="javascript:void(0);"><i class="fa fa-trash-o"></i>作废</a>'
                        //查看按钮
                        var viewBtn = yams.buttons.view({id: row.id});

                        if (row.status === 0) {
                            return payBtn + delBtn ;
                        } else if(row.status === 1){
                            return viewBtn;
                        }
                    }
                }],
                multiple: true,
                rowIndex: true,
                checkbox: true,
                ajaxType: Dolphin.requestMethod.POST,

                queryParams: {
                    house_account_obj_ae : yams.context.checkAdmin()
                },
                data: {rows: []},
                pagination: true,
                onLoadSuccess: function () {
                    yams.buttons.editCallback('0ab39b2ab218459c9d48d91246742d68', 'id', function (data) {
                        $('#dataModal').modal('show');
                    });
                    yams.buttons.viewCallback('0ab39b2ab218459c9d48d91246742d68', 'id', function (data) {
                        Dolphin.form.setValue(data.value, '.view-form');
                        $('#viewDataModal').modal('show');
                    });
                    $('.disuseBtn').click(function () {
                        let _this = $(this);
                        Dolphin.confirm('确认作废？', {
                            callback: function (flag) {
                                if (flag) {
                                    Dolphin.ajax({
                                        url: '/api/0ab39b2ab218459c9d48d91246742d68@id=' + _this.data('id'),
                                        type: Dolphin.requestMethod.DELETE,
                                        onSuccess: function (reData) {
                                            me._dataList.reload();
                                        }
                                    });
                                }
                            }
                        })
                    });
                }
            });
        },
        //获得所有用户
        initUserList: function () {
            let me = this;
            me._userList = new Dolphin.LIST({
                panel: "#userList",
                idField: 'id',
                columns: [{
                    code: 'name',
                    title: '会员名称'
                }, {
                    code: 'code',
                    title: '会员编码'
                }, {
                    code: 'phone',
                    title: '联系方式'
                }, {
                    code: 'idCard',
                    title: '身份证号'
                }, {
                    code: 'sex',
                    title: '性别',
                    formatter: function (val) {
                        return val;
                    }
                }],
                multiple: false,
                rowIndex: true,
                checkbox: true,
                pagination: true,
                ajaxType: Dolphin.requestMethod.POST,
                url: '/api/8f7def63cc5845468b2c65a69abb0664',
                onLoadSuccess: function () {
                }
            });
        },
        //获得所有房屋
        initHouseList: function () {
            let me = this;
            this._houseList = new Dolphin.LIST({
                panel: '#houseList',
                idField: 'id',
                queryParams:{
                    status : 1
                },
                columns: [{
                    code: 'name',
                    title: '名称'
                }, {
                    code: 'houseType',
                    title: '户型',
                    formatter: function (val) {
                        return Dolphin.enum.getEnumText("houseType", val);
                    }
                }, {
                    code: 'floor',
                    title: '楼层'
                }, {
                    code: 'area',
                    title: '面积'
                }, {
                    code: 'monthly',
                    title: '月租'
                }, {
                    code: 'recommend',
                    title: '推荐',
                    formatter: function (val) {
                        return val ? "是" : "否";
                    }
                }, {
                    code: 'statusName',
                    title: '状态'
                }],
                multiple: false,
                rowIndex: true,
                checkbox: true,
                pagination: true,

                ajaxType: Dolphin.requestMethod.POST,
                url: '/api/9651431c68a04d54a22f1e2e7faf827b',
                onLoadSuccess: function (data) {
                }
            });
        },

        /**
         * 一些页面DOM对象和事件的销毁
         */
        destroy: function () {
        }
    };


});