$(function () {
    yams.menu.select('inspectionManagement');

    yams.page = {
        /**
         * 初始化页面
         */
        init: function () {

            this.initDataList();
            this.initUserList();
            this.initHouseList();

            this.initEvent();
            Dolphin.form.parse();
        },
        /**
         * 初始化页面事件
         */
        initEvent: function () {
            var me = this;
            me.prices = {};

            //新建订单
            $("#orderAdd").click(function () {
                Dolphin.form.empty('.edit-form');
                $("#orderStatus").val("IOS00");
                $('#dataModal').modal('show');
            });
            //保存订单
            $(".orderSave").click(function () {
                var ef = $(".edit-form");
                if (Dolphin.form.validate(ef)) {
                    var data = Dolphin.form.getValue(ef, '"');
                    data.checkRoomDate = data.checkRoomDate + ":00";
                    Dolphin.ajax({
                        url: '/api/00923c89a610415d8e70b1b4f2380e75',
                        type: Dolphin.requestMethod.PUT,
                        data: Dolphin.json2string(data),
                        onSuccess: function (reData) {
                            me._dataList.load();
                            $('#dataModal').modal('hide');
                        }
                    });
                }
            });

            //用户选择器
            $("#selectUser").click(function(){
                $('#userModal').modal('show');
            });
            //用户选择器查询
            $("#queryUser").click(function () {
                let d = Dolphin.form.getValue('.query-user-form');
                me._userList.query(d);
            });
            //用户确定
            $(".confirmUser").click(function(){
                let userArray = me._userList.getChecked();
                $("#userId").val(userArray[0].id);
                $("#contactName").val(userArray[0].name);
                $("#contactMobile").val(userArray[0].phone);
                $('#userModal').modal('hide');
            });

            //房屋选择器
            $("#selectHouse").click(function(){
                $('#houseModal').modal('show');
            });
            //房屋选择器查询
            $("#queryHouse").click(function () {
                let d = Dolphin.form.getValue('.query-house-form');
                me._houseList.query(d);
            });
            //房屋确定
            $(".confirmHouse").click(function(){

                let houseArray = me._houseList.getChecked();
                $("#houseId").val(houseArray[0].id);
                $("#houseName").val(houseArray[0].name);

                Dolphin.ajax({
                    url: '/api/123bc41c5fe44dfc9f396b5ace5fa123@houseId='+houseArray[0].id,
                    type: Dolphin.requestMethod.GET,
                    onSuccess: function (reData) {
                        $.each(reData.rows,function(i,obj){
                            me.prices[obj.shortRentType] = obj.dayPrice;
                        });

                        //根据订单类型
                        var orderTypeVal = $("#orderType").val();
                        var price;
                        if(orderTypeVal == 'SRM01') {//钟点房
                            var rentTypeVal = $("#shortRentType").val();
                            price = me.prices[rentTypeVal];
                        }else if(orderTypeVal == 'SRM02'){//日租
                            price = Dolphin.accMul(me.prices['SRT00'],$("#days").val());
                        }
                        $("#rentPrice").val(price).change();
                        $('#houseModal').modal('hide');
                    }
                });
            });

            //查询
            $(".btn-query").click(function () {
                me._dataList.load(null,Dolphin.form.getValue('.query-form'))
            });

        },

        initDataList: function () {
            let me = this;
            me._dataList = new Dolphin.LIST({
                panel: "#dataList",
                idField: 'id',
                queryParams: {
                    house_account_obj_ae : yams.context.checkAdmin()
                },
                columns: [{
                    code: 'userId',
                    hidden: true
                },{
                    code: 'contactName',
                    title: '联系姓名',
                    width: '100px',
                    formatter: function(val, row, index){
                        return '<a class="showUserDetail" data-userid="'+row.userId+'" href="javascript:;">'+val+'</a>';
                    }
                }, {
                    code: 'contactMobile',
                    title: '联系手机',
                    width: '120px'
                }, {
                    code: 'house.name',
                    title: '房屋名称',
                    width: '250px'
                }, {
                    code: 'checkRoomDate',
                    title: '看房时间',
                    width: '150px'
                },{
                    code: 'statusName',
                    title: '看房状态',
                    width: '100px'
                },{
                    code: 'id',
                    title: '&nbsp;',
                    width: '220px',
                    formatter: function (val, row, index) {

                        var confirmBtnStr = '';
                        var cancelBtnStr = '<a class="btn btn-outline btn-circle btn-sm dark orderCancel" href="javascript:;">' +
                            '<input type="hidden" value="'+row.id+'"/><i class="fa fa-trash-o"></i>取消</a>';
                        if(row.orderStatus == 'IOS00'){
                            confirmBtnStr = '<a class="btn btn-outline btn-circle btn-sm green orderConfirm" ' +
                                'data-id="'+row.id+'" data-userid="'+row.userId+'" href="javascript:;">' +
                                '<i class="fa fa-check"></i>确认</a>';
                        }
                        return confirmBtnStr
                            + yams.buttons.edit({
                                id: row.id
                            })
                            + cancelBtnStr;
                    }
                }],
                multiple: true,
                rowIndex: true,
                checkbox: true,
                ajaxType: Dolphin.requestMethod.POST,
                url: '/api/00923c89a610415d8e70b1b4f2380e75',
                pagination: true,
                onLoadSuccess: function () {
                    yams.buttons.editCallback('f2f229968e4c42c29fea3b508f047629', 'id', function (data) {
                        let da = data.value;

                        var orderTypeVal = $("#orderType").val();
                        if(orderTypeVal == 'SRM01'){
                            $("#hourContainer").show();
                            $("#dayContainer").hide();
                            $("#checkRoomDate").val(da.arrivedDate.substr(0,16));
                        }else if(orderTypeVal == 'SRM02'){
                            $("#hourContainer").hide();
                            $("#dayContainer").show();
                            $("#arrivedDate").val(da.arrivedDate.substr(0,10));
                            $("#leavedDate").val(da.leavedDate.substr(0,10));
                        }else{
                            $("#hourContainer").hide();
                            $("#dayContainer").hide();
                        }

                        $('#dataModal').modal('show');
                    });

                    //确认订单
                    $('.orderConfirm').click(function () {
                        var id = $(this).data("id");
                        var userId = $(this).data("userid");
                        console.log(id);
                        console.log(userId);
                        Dolphin.confirm('确认订单？', {
                            callback: function (flag) {
                                if (flag) {
                                    Dolphin.ajax({
                                        url: '/api/0e868f2e51a44005950df4b810771393@id='+id,
                                        type: Dolphin.requestMethod.PUT,
                                        onSuccess: function (reData) {
                                            var checkRoomDate = reData.value.checkRoomDate;
                                            var managerName = reData.value.properties.managerName;
                                            var managerPhone = reData.value.properties.managerPhone;
                                            Dolphin.ajax({
                                                url: '/api/0d644ca2f7394092b84a1fb0c7aa0821@id='+userId,
                                                type: Dolphin.requestMethod.GET,
                                                onSuccess: function(reData){
                                                    var openid = reData.value.weChat;
                                                    var userName = reData.value.name;
                                                    var userPhone = reData.value.phone;
                                                    //管理员确认预约看房后微信消息给客户
                                                    var msgData = {
                                                        "url" : "http://"+Dolphin.wechatUrl+"/yams/view/order?openId="+openid,
                                                        "openid" : openid,
                                                        "first" : "尊敬的用户,您好!",
                                                        "keyword1" : checkRoomDate,
                                                        "keyword2" : userPhone,
                                                        "keyword3" : userName,
                                                        "remark" : "您的预约看房请求，已被管理员("+managerName+","+managerPhone+")受理，请按时赴约，谢谢！"
                                                    }
                                                    Dolphin.ajax({
                                                        url: '/wexin/sendWXTemplateMsg',
                                                        type: Dolphin.requestMethod.POST,
                                                        data: Dolphin.json2string(msgData),
                                                        onSuccess: function (reData) {
                                                            me._dataList.load();
                                                        }
                                                    });
                                                }
                                            });

                                        }
                                    });
                                }
                            }
                        })
                    });

                    //取消订单
                    $('.orderCancel').click(function () {
                        var id = $(this).children().val();
                        Dolphin.confirm('取消订单？', {
                            callback: function (flag) {
                                if (flag) {
                                    Dolphin.ajax({
                                        url: '/api/e07cd9d9a7d8428683be64abc42f02c8@id='+id,
                                        type: Dolphin.requestMethod.DELETE,
                                        onSuccess: function (reData) {
                                            me._dataList.load();
                                        }
                                    });
                                }
                            }
                        })
                    });

                    //看客户具体信息
                    $('.showUserDetail').click(function(){
                        var _this = $(this);
                        Dolphin.ajax({
                            url: '/api/0d644ca2f7394092b84a1fb0c7aa0821@id='+_this.data('userid'),
                            type: Dolphin.requestMethod.GET,
                            onSuccess: function (reData) {

                                $("#usercode").html(reData.value.code);
                                $("#useridCard").html(reData.value.idCard);
                                $("#userintroducer").html(reData.value.introducer);
                                $("#username").html(reData.value.name);
                                $("#userphone").html(reData.value.phone);
                                $("#usersex").html(reData.value.sex);
                                $("#userstatus").html(reData.value.status == '1' ? '启用' : '禁用');

                                $("#usericFrontImg").attr('src',reData.value.icFrontImg);
                                $("#usericBackImg").attr('src',reData.value.icBackImg);
                                $("#useravatarImg").attr('src',reData.value.avatarImg);

                                $('#userDetailModal').modal('show');

                            }
                        });

                    });
                }
            });
        },

        initUserList: function () {
            let me = this;
            me._userList = new Dolphin.LIST({
                panel: "#userList",
                idField: 'id',
                columns: [{
                    code: 'name',
                    title: '会员名称'
                }, {
                    code: 'code',
                    title: '会员编码'
                }, {
                    code: 'phone',
                    title: '联系方式'
                }, {
                    code: 'idCard',
                    title: '身份证号'
                }, {
                    code: 'sex',
                    title: '性别',
                    formatter: function (val) {
                        return val;
                    }
                }],
                multiple: false,
                rowIndex: true,
                checkbox: true,
                pagination: true,
                ajaxType: Dolphin.requestMethod.POST,
                url: '/api/8f7def63cc5845468b2c65a69abb0664',
                onLoadSuccess: function () {
                }
            });
        },

        initHouseList: function () {
            let me = this;
            this._houseList = new Dolphin.LIST({
                panel: '#houseList',
                idField: 'id',
                queryParams:{
                    status : 1
                },
                columns: [{
                    code: 'name',
                    title: '名称'
                }, {
                    code: 'houseType',
                    title: '户型',
                    formatter: function (val) {
                        return Dolphin.enum.getEnumText("houseType", val);
                    }
                }, {
                    code: 'floor',
                    title: '楼层'
                }, {
                    code: 'area',
                    title: '面积'
                }, {
                    code: 'monthly',
                    title: '月租'
                }, {
                    code: 'recommend',
                    title: '推荐',
                    formatter: function (val) {
                        return val ? "是" : "否";
                    }
                }, {
                    code: 'statusName',
                    title: '状态'
                }],
                multiple: false,
                rowIndex: true,
                checkbox: true,
                pagination: true,

                ajaxType: Dolphin.requestMethod.POST,
                url: '/api/9651431c68a04d54a22f1e2e7faf827b',
                onLoadSuccess: function (data) {
                }
            });
        },

        /**
         * 一些页面DOM对象和事件的销毁
         */
        destroy: function () {
        }
    };
});