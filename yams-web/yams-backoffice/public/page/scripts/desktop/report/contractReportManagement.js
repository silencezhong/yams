$(function () {
    yams.menu.select('accountManagement');

    yams.page = {
        /**
         * 初始化页面
         */
        init: function () {
            Dolphin.form.parse();
            this.initDataList();
            this.initEvent();
        },
        /**
         * 初始化页面事件
         */
        initEvent: function () {
            var me = this;

            $(".newAccount").click(function () {
                Dolphin.form.empty('.edit-form');
                $('#dataModal').modal('show');
            });

            $(".modelSave").click(function () {
                var ef = $(".edit-form");
                if (Dolphin.form.validate(ef)) {
                    var data = Dolphin.form.getValue(ef, '"');
                    Dolphin.ajax({
                        url: '/api/756af3ae31a64565a54ec0221f84d377',
                        type: Dolphin.requestMethod.PUT,
                        data: Dolphin.json2string(data),
                        onSuccess: function (reData) {
                            me._dataList.load();
                            $('#dataModal').modal('hide');
                        }
                    });
                }
            });

            $(".restPwd").click(function () {
                var items = me._dataList.getChecked();
                if (items.length > 0) {
                    Dolphin.confirm("确认重置所选择账号的密码?", {
                        callback: function (flg) {
                            if (flg) {
                                var pl = [];
                                for (var i = 0; i < items.length; i++) {
                                    pl.push(items[i].id);
                                }
                                Dolphin.ajax({
                                    url: '/api/15f66c54650c443386878f5b818dc48f',
                                    data: Dolphin.json2string({id: pl.join(',')}),
                                    type: Dolphin.requestMethod.POST,
                                    onSuccess: function (reData) {
                                        $(".btn-query").click();
                                    }
                                });
                            }
                        }
                    });
                }
            });

            $(".btn-query").click(function () {
                let d = Dolphin.form.getValue('.query-form');
                me._dataList.query(d);
                // me._dataList.load(null,Dolphin.form.getValue('.query-form'))
            });
            //导出
            $(".btn-export").click(function () {

                var queryCondition = Dolphin.form.getValue('.query-form');
                console.log(queryCondition);
                // console.log(me._deviceList.opts.queryParams);
                var url = (yams.context.contextPath =='/'?'': yams.context.contextPath) +'/api/756af3ae31a64565a54ec02018051502';
                var form = $("<form />");
                form.attr({"style": "display: none", "target": '_blank', "method": "post", "action": url});
                $('body').append(form);
                $.each(queryCondition, function(key, value){
                    var input = $("<input>");
                    input.attr({"name": key, "value": value, "type": "hidden"});
                    console.log('key : '+key+' | value : '+value);
                    form.append(input);
                });
                form.submit();
                form.remove();
            });
        },

        initDataList: function () {
            var me = this;
            me._dataList = new Dolphin.LIST({
                panel: "#dataList",
                title:'合同报表',
                idField: 'id',
                columns: [{
                    code: 'cityName',
                    title: '城市'
                }, {
                    code: 'apartmentName',
                    title: '公寓'
                }, {
                    code: 'manageName',
                    title: '公寓管家'
                },{
                    code: 'contractNo',
                    title: '合同号'
                },{
                    code: 'houseNo',
                    title: '房号'
                },{
                    code: 'houseType',
                    title: '房型'
                },{
                    code: 'customerName',
                    title: '客户名称'
                },{
                    code: 'customerPhone',
                    title: '电话'
                },{
                    code: 'idCard',
                    title: '身份证号'
                },{
                    code: 'payMethodStr',
                    title: '付款方式'
                },{
                    code: 'monthlyRentStr',
                    title: '每月租金'
                },{
                    code: 'pledgeStr',
                    title: '押金'
                },{
                    code: 'rentalEndDate',
                    title: '租金到期日'
                },{
                    code: 'rentStartDateStr',
                    title: '租房开始日期'
                },{
                    code: 'rentEndDateStr',
                    title: '租房结束日期'
                },{
                    code: 'billAmountStr',
                    title: '账单金额'
                },{
                    code: 'isDelayStr',
                    title: '是否快到期'
                },{
                    code: 'delayDayStr',
                    title: '租金剩余天数'
                }],
                multiple: true,
                rowIndex: true,
                checkbox: false,
                ajaxType: Dolphin.requestMethod.POST,
                url: '/api/756af3ae31a64565a54ec02018051501',
                pagination: true,
                onLoadSuccess: function () {
                    yams.buttons.editCallback('a0f1e5cde2084416946ed6c4c2a25da9', 'id', function (data) {
                        $('#dataModal').modal('show');
                    });
                    yams.buttons.delCallback('a0f1e5cde2084416946ed6c4c2a25da9',function (data) {
                        me._dataList.load();
                    });
                }
            });
        },

        /**
         * 一些页面DOM对象和事件的销毁
         */
        destroy: function () {
        }
    };
});