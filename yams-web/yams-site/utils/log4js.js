/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved. 
 */

module.exports = {
    appenders: [{
        type: 'console'
    }],
    levels: {              //日志级别
        default: 'info',
        http: 'info',
        data: 'warn',
        login: 'warn'
    }
};