/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */

module.exports = {
    title: '科寓租房',
    contextPath: '/yams',
    viewPrefix: '/view',
    publicPath: "/static-yams-site",
    mode: "",
    mock: false,
    cdn: '//cdn.bootcss.com',
    // cdn: '',

    // apiServerUrl: 'http://127.0.0.1:9280',
    apiServerUrl: 'http://127.0.0.1:9130',
    apiFilePath: '/../apiConfig.json',
    permission : {
        public:0,
        protect:1,
        private:2
    },
    uploadDir: '/home/yams/app/uploadFiles',
    imagePath: '',
    production: true
};
