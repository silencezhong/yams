'use strict';
const express = require('express');
const router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Hello' });
});

router.get('/refresh/session', function(req, res, next) {
  delete req.session.openId;
  res.send({success:true});
});

router.get('/get/session', function(req, res, next) {
    res.send({success:true,barCode: (req.session && req.session.barCode)||''});
});

module.exports = router;
