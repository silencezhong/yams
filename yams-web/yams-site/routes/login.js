"use strict";

const request = require('request');
const router = require('express').Router();
const extend = require('extend');

/**
 * 登录验证
 */
router.post('/', function (req, res, next) {
    let bodyData = req.body,
        redirect, url;
    if (bodyData.id) {
        req.session.openId = bodyData.id;
        global.tool.getUserInfo(bodyData.id,function (uData) {
            global.weChatUtil.getUserInfo(bodyData.id,function (weData) {
                let userData = uData;
                extend(true,userData,weData);
                req.session.userData = userData;
                res.send({success : true});
                // if(userData.id)
                //     res.send({success : true});
                // else
                //     res.send({success : false});
            });
        });
    } else {
        res.send({success: false, msg: "openId不能为空"});
    }
});

router.get('/logout', function(req, res, next) {
    delete req.session.openId;
    res.send("<html><body ontouchstart><h1>请关闭页面后，重新打开本页面！</h1></body></html>");
});

module.exports = router;
