const router = require('express').Router();
const request = require('request');
const fs = require('fs');

router.post('/groups/update', function (req, res, next) {
    console.log(req.body);
    let type = req.query.type;
    let data = req.body;
    global.weChatUtil.updateGroup(type, {"group": req.body}, function (weData) {
        console.log(weData);
        res.send({success: true, data: weData});
    });
});

router.post('/groups/members', function (req, res, next) {
    console.log(req.body);
    let data = req.body, groups = data['groups'];
    for (let i = 0; i < groups.length; i++) {
        global.weChatUtil.updateGroup("members/update", {
            openid: data.openid,
            to_groupid: groups[i]
        }, function (weData) {
            console.log(weData);
        });
    }
    res.send({success: true});
});

/**
 * 从微信下载上传之后的图片到本地
 */
router.get('/downloadImage', function (req, res, next) {
    console.log('downloadImage', req.query.mediaId + ' | ' +req.query.catalog);
    global.weChatUtil.getToken(function (token) {
        let url = "http://file.api.weixin.qq.com/cgi-bin/media/get?access_token=" + token + "&media_id=" + req.query.mediaId;
        request(url).pipe(fs.createWriteStream(global.config.uploadDir + "/"+req.query.catalog + "/"+req.query.mediaId + ".jpg"));
        res.send({success: true,url:url});
    });
});

/**
 * 发送预看房模板消息
 */
router.post('/sendWXTemplateMsg', function (req, res, next) {

    let data = req.body;
    let message = {
        touser: data.openid,
        template_id: "HjnZ_szN72qwyQ40SS3DrAOLcSAaXQZGxwoDjfDOANw",
        url: data.url,
        topcolor: "#FF0000"
    };
    message.data = {
        first: {value: data.first, color: "#173177"},
        keyword1: {value: data.keyword1},
        keyword2: {value: data.keyword2},
        keyword3: {value: data.keyword3},
        remark: {value: data.remark, color: "#173177"}
    };

    global.weChatUtil.templateMessage(message, () => {
        console.log('sendWXTemplateMsg success');
    });
    res.send({success: true});

});

/**
 * 发送账单消息
 */
router.post('/sendBillMsg', function (req, res, next) {

    let data = req.body;
    let message = {
        touser: data.openid,
        template_id: "8TBpNPfFKp8OEOVQqXHsPEcXHUvQuGjTZa9HKQuePqM",
        url: data.url,
        topcolor: "#FF0000"
    };
    message.data = {
        first: {value: data.first, color: "#173177"},
        keyword4: {value: data.keyword4},
        keyword5: {value: data.keyword5},
        remark: {value: data.remark, color: "#173177"}
    };

    global.weChatUtil.templateMessage(message, () => {
        console.log('sendBillMsg success');
    });
    res.send({success: true});

});

/**
 * 发送短租已确认待付款消息
 */
router.post('/sendPayMsg', function (req, res, next) {

    let data = req.body;
    let message = {
        touser: data.openid,
        template_id: "8TBpNPfFKp8OEOVQqXHsPEcXHUvQuGjTZa9HKQuePqM",
        url: data.url,
        topcolor: "#FF0000"
    };
    message.data = {
        first: {value: data.first, color: "#173177"},
        keyword1: {value: data.keyword1},
        keyword2: {value: data.keyword2},
        keyword3: {value: data.keyword3},
        keyword4: {value: data.keyword4},
        keyword5: {value: data.keyword5},
        remark: {value: data.remark, color: "#173177"}
    };

    global.weChatUtil.templateMessage(message, () => {
        console.log('sendWXTemplateMsg success');
    });
    res.send({success: true});

});


module.exports = router;
