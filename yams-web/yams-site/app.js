/*
 * Copyright (c) 2016 Breezee.org. All Rights Reserved.
 */
'use strict';
let express = require('express');
let path = require('path');
let favicon = require('serve-favicon');
// let logger = require('morgan');
let log4js = require('log4js');
log4js.configure(require('./utils/log4js'));
global.log4js = log4js;

let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
const xmlParser = require('express-xml-bodyparser');
let session = require('express-session');
let FileStore = require('session-file-store')(session);
const i18n = require('i18n');

global.config = require('./utils/config.js');
global.tool = require('./utils/tool');
global.weChatUtil = require('./utils/wechat');
global.cacheData = {};

let app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.set('case sensitive routing', true);
app.set('strict routing', true);
app.set('trust proxy', true);

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
// app.use(logger('dev'));
app.use(log4js.connectLogger(log4js.getLogger("http"), {
    level: 'debug',
    format: ':method | :status | :response-time ms | :url '
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(xmlParser({}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
    secret: 'keyboard cat',
    resave: true,
    name: 'yams.site.sid',
    saveUninitialized: true,
    cookie: {maxAge: 3600000},
    store: new FileStore({
        path: 'sessions/site'
    })
}));

//i18n
i18n.configure({
    // setup some locales - other locales default to en silently
    locales: ['en', 'zh'],
    directory: __dirname + '/views/locales',
    defaultLocale : 'zh',
    autoReload: true,
    updateFiles: false,
    extension: '.json',
    // setting of log level WARN - default to require('debug')('i18n:warn')
    logWarnFn: function (msg) {
        console.log('warn', msg);
    },
    // setting of log level ERROR - default to require('debug')('i18n:error')
    logErrorFn: function (msg) {
        console.log('error', msg);
    }
});

app.use(i18n.init);

app.use('/', require('./routes/index'));
app.use('/login',require('./routes/login'));
app.use('/view', require('./routes/view'));
app.use('/api', require('./routes/data'));
app.use('/mp',require('./routes/mp'));
app.use('/wexin',require('./routes/wexin'));
app.use('/file',require('./routes/file'));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers
app.use(function (err, req, res, next) {
    if (err.status == 404 || err.message.indexOf('Failed to lookup view') > -1) {
        res.render('notFound', {
            message: err.message,
            error: err
        });
    } else if (err.status == 401) {
        res.render(req.session.endType + '/forbid', {
            message: err.message,
            error: err
        });
    } else {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    }
});

module.exports = app;
